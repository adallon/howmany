(* BAC for e-passport

R -> P : getChall
P -> R : nP
R -> P : {nR,nP,kR}ke, mac({nR,nP,kR}ke,km)
P -> R : {nP,nR,kP}ke, mac({nP,nR,kP}ke,km)

Unlinkability.
Phase 0: 2 passports (+ readers) under replication
Phase 1: passport 1 or 2.

*)
channel c1, c2, c3, c4, c5, c6.

public getC:chall.
public cR: tagR.
public cP: tagP.


fun senc(bitstring,atom).
fun hash(bitstring).

reduc sdec(senc(x,y),y) -> x.

let PR1(c,ke,km) = 
out(c, getC);
in(c,xP:nonceP1);
new nR: nonceR1;
new kR: keyR1;
out(c,(senc((cR,nR,xP,kR),ke),hash((senc((cR,nR,xP,kR),ke),km))));
in(c,(senc((cP,xP,nR,yP:keyP1),ke),hash((senc((cP, xP,nR,yP),ke),km)))).


let PP1(c,ke,km) =
in(c, getC);
new nP:nonceP1;
new kP: keyP1;
out(c,nP);
in(c,(senc((cR,xR:nonceR1,nP,yR:keyR1),ke),hash((senc((cR,xR,nP,yR),ke),km))));
out(c,(senc((cP,nP,xR,kP),ke),hash((senc((cP,nP,xR,kP),ke),km)))).


let PR2(c,ke,km) = 
out(c, getC);
in(c,xP:nonceP2);
new nR: nonceR2;
new kR: keyR2;
out(c,(senc((cR,nR,xP,kR),ke),hash((senc((cR,nR,xP,kR),ke),km))));
in(c,(senc((cP,xP,nR,yP:keyP2),ke),hash((senc((cP, xP,nR,yP),ke),km)))).


let PP2(c,ke,km) =
in(c, getC);
new nP:nonceP2;
new kP: keyP2;
out(c,nP);
in(c,(senc((cR,xR:nonceR2,nP,yR:keyR2),ke),hash((senc((cR,xR,nP,yR),ke),km))));
out(c,(senc((cP,nP,xR,kP),ke),hash((senc((cP,nP,xR,kP),ke),km)))).

let P_BAC1 = 
    new ke1: skey1; new km1: mkey1;
    new ke2: skey2; new km2: mkey2;
   (
   ! new_ch cR1; out(c1,cR1); PR1(cR1,ke1,km1) 
   |! new_ch cP1; out(c2,cP1); PP1(cP1,ke1,km1)
   |! new_ch cR2; out(c3,cR2); PR2(cR2,ke2,km2) 
   |! new_ch cP2; out(c4,cP2); PP2(cP2,ke2,km2)
  (* second phase  *)
   | phase 1; PR1(c5,ke1,km1) 
   | phase 1; PP1(c6,ke1,km1) 
   )

let P_BAC2 = 
    new ke1: skey1; new km1: mkey1;
    new ke2: skey2; new km2: mkey2;
   (
   ! new_ch cR1; out(c1,cR1); PR1(cR1,ke1,km1) 
   |! new_ch cP1; out(c2,cP1); PP1(cP1,ke1,km1)
   |! new_ch cR2; out(c3,cR2); PR2(cR2,ke2,km2) 
   |! new_ch cP2; out(c4,cP2); PP2(cP2,ke2,km2)
  (* second phase  *)
   | phase 1; PR2(c5,ke2,km2) 
   | phase 1; PP2(c6,ke2,km2) 
 )

inclusion(P_BAC1,P_BAC2).
