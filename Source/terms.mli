type sort = Atom | Bitstring 

val print_sort_list: sort list -> unit

type atoms
val atoms_to_string_simple: atoms -> string
val print_atoms: atoms -> unit

val new_name : string -> atoms
val new_const: string -> atoms
val new_var  : string -> atoms

type function_symbol

val to_function_symbol: 
  string -> (sort list) -> function_symbol
val function_symbol_def_to_string: function_symbol -> string
val function_name_and_arity: function_symbol -> string * int
val print_function_symbol: function_symbol -> unit

type 'a term

val to_data: 'a -> 'a term
val to_tuple: ('a term) list -> 'a term
val composite_term: 
  (function_symbol list) -> string -> ('a term list) -> 'a term


type atom_type
val to_atom_type: string -> atom_type

type type_term
val type_to_string: type_term -> string
val atom_type_to_type: atom_type term -> type_term
val get_subtypes: type_term -> type_term array

val is_composed: type_term -> bool
val is_tuple: type_term -> bool 

type untyped_term
val untyped_term_to_string: untyped_term -> string
val untyped_term_to_string_alt: untyped_term -> string
val string_to_untyped_term: 
  (string -> (atoms * type_term ) option ) -> 
    string term -> untyped_term

val atomic_position: untyped_term -> atoms -> bool

val direct_subterm: untyped_term -> untyped_term -> bool

type shape
val get_shapes_term: untyped_term list -> shape list
val compatibility: shape list -> bool

type typed_data
val typed_data_to_string: typed_data -> string
val to_typed_data: atoms*type_term -> typed_data
val typed_data_to_atoms: typed_data -> atoms
val string_find: string -> typed_data list -> typed_data option
val is_variable: typed_data -> bool

type typed_term

val typed_term_to_string: typed_term -> string
val typed_term_to_string_alt: typed_data list -> typed_term -> string

val to_typed_term: typed_data term -> typed_term
val is_well_shaped: shape list -> typed_term -> bool
val get_variables: untyped_term -> atoms list


val untype: typed_term -> untyped_term
val get_type: typed_term -> type_term

val define_constant: string -> type_term -> typed_term
val typed_term_to_typed_data: typed_term -> typed_data
val is_constant_name: string -> typed_term -> (atoms*type_term) option
val constant_name: typed_term -> string

val replication_of_data: int -> typed_data -> typed_data

type parsing_data
val parsing_data_to_string: parsing_data -> string
val parsing_data_to_name: parsing_data -> string
val string_and_type_to_nonce: (string*type_term) -> parsing_data
val string_to_parsing_data: string -> parsing_data

val parsing_data_name_to_typed_data: parsing_data -> typed_data

type parsing_term

val parsing_term_to_string: parsing_term -> string
val to_parsing_tuple: parsing_term list -> parsing_term
val composite_parsing_term: 
  (function_symbol list) -> string -> (parsing_term list) -> parsing_term
val parsing_untyped_data: string -> parsing_term
val parsing_typed_data: (string * type_term) -> parsing_term

val string_to_parsing_term: string term -> parsing_term
val parsing_term_to_typed_term:
  typed_data list -> int -> 
    parsing_term -> (typed_term * typed_data list * typed_data list)

val replace_name: 
  (string*parsing_term*(bool option)) list -> parsing_term -> 
    parsing_term * (string*parsing_term*(bool option)) list 

val hidden_channel: parsing_term -> string

val replication_data_term: 
  (typed_data * typed_data) list ->
    (typed_data * typed_data) list ->
      typed_term -> typed_term


val check_compliance_terms:
  typed_term list -> (typed_data * typed_term) list -> bool
val get_dishonest_types: typed_term list -> type_term list
val get_name_types:  typed_term list -> type_term list
val is_public_type:  int -> type_term list -> type_term -> bool

type reduction = string * (untyped_term list) * untyped_term

val is_built_from: typed_term list -> untyped_term -> bool

type rho_prep 

val prepare_rho: reduction list -> rho_prep list

type position = int list
type 'a rho_element = 'a * position * 'a list

val rho: rho_prep list -> type_term -> type_term rho_element list
val rho_term: rho_prep list -> typed_term -> typed_term rho_element list

val replace: (atoms * typed_term) list -> typed_term -> typed_term
val add_to_subst: (atoms * typed_term) list -> typed_data * typed_term -> (atoms* typed_term ) list

val types_of_tests: type_term -> reduction list -> type_term list list

val constP:typed_term
val constQ:typed_term
val deepsec_input_term: typed_data list -> typed_term -> string
