type dependency_graph = {
  list_labels : Label.label list;
  list_types : Label.type_label list;
  rho_list :
    (Label.label * Terms.type_term * Terms.position * Terms.type_term list)
    list;
  labels_seen : Label.label list;
  types_seen : Label.type_label list;
  dishonest : Terms.type_term list;
  name_types : Terms.type_term list;
}
val graph_to_string : dependency_graph -> string
val set_multiplicity : dependency_graph -> Label.label list -> unit
val get_multiplicity : dependency_graph -> Label.label list -> int list
val set_scenario_from_multiplicity :
  dependency_graph -> int list -> Label.label list
val minimal_set :
  dependency_graph -> Label.label list list -> Label.label list list
val overapproximating_scenario :
  dependency_graph -> Label.label list list -> Label.label list

val init_graph :
  int ->
  Terms.reduction list ->
  (Terms.typed_data, Terms.typed_term) Process.generic_process -> 
  (Terms.typed_data, Terms.typed_term) Process.generic_process option -> 
  dependency_graph

val see_label : dependency_graph -> Label.label -> dependency_graph
val see_type : dependency_graph -> Label.type_label -> dependency_graph
val add_list_type : dependency_graph -> Label.type_label -> dependency_graph
val is_dishonest : int -> dependency_graph -> Terms.type_term -> bool
val find_rho :
  dependency_graph ->
  Terms.type_term ->
  (Label.label * Terms.position * Terms.type_term list) list
val find_reach : dependency_graph -> Label.label list 
val search_label :
  int -> dependency_graph -> Terms.type_term -> Label.type_label option
val type_to_label :
  int ->
  dependency_graph -> Terms.type_term -> Label.type_label * dependency_graph
val type_array_to_label :
  int ->
  dependency_graph ->
  Terms.type_term array -> Label.type_label array * dependency_graph
val compute_graph :
  int -> dependency_graph -> Terms.type_term list -> dependency_graph
