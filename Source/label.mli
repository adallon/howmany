type label = {
  lab_phase : int;
  lab_p : Process.process;
  lab_q : Process.process option;
  type_term : Terms.type_term option;
  path : int list;
  marked_positions : Terms.position list;
  is_replicated : bool;
  mutable lab_dep : result option;
  mutable lab_pred : label option;
  mutable following : label list;
  mutable pred_set : bool;
  mutable lab_type : type_label option;
  mutable lab_is_empty : bool option;
  mutable quantity : int;
}
and type_label = {
  mutable type_phase : int;
  type_tau : Terms.type_term;
  type_subtypes: type_label array;
  mutable type_dep : result option;
  mutable tl_rho_list : (label * (type_label list)) list option;
  mutable rho_plus : label list option;
  mutable type_is_empty : bool option;
  mutable dep_init : result option;
}
and result (* = label list list option *)

val label_to_string : label -> string
val label_type_to_string : type_label -> string
val result_to_string : label list list option -> string
val create_label :
  int * bool * Process.process * Process.process option * Terms.type_term option * int list * Terms.position list -> label
val create_type_label : 
  int * Terms.type_term * (type_label array) -> type_label

val label_equals: label -> label -> bool
val compare_label :
  label -> int * bool * Process.process * Terms.type_term option * 'a -> bool

val is_type_label : type_label -> Terms.type_term -> bool

val set_pred : label -> label option -> unit
val get_pred : label -> label option

val empty : result
val singleton_empty : result
val bottom : result

val set_product2 : result -> result -> result
val set_product : result list -> result
val simple_union : 'a list -> 'a list -> 'a list
val set_union2 : result -> result -> result
val set_union : result list -> result

val singleton_label: label -> result
val to_label_list_list_option: result -> label list list option
