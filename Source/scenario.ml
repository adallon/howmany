open Terms
open Process
open Label


(*
 *
 * Once the dependencies have been computed, 
 * we must compute the corresponding scenario as a process.
 * It is done here.
 *
 *)

let rec find_next level lab0 =
  let list_next = lab0.following in
  let rec aux level = function
    | [] -> None
    | lab1::q when lab1.quantity > 0 -> Some(lab1)
    | lab1::q -> aux (level+1) q
  in aux level list_next

let rec find_first level lab =
  (* Goes to the first process p such that lab occurs in p *)
  match lab.lab_pred with
  | None -> lab
  | Some(lab0) -> find_first (level+1) lab0
  
let rec find_last level chain lab =
  (* Goes to the last part of process lab.lab_p
   * which is included in the dependencies 
   * and computes the intermediate elements.
   * Those elements are called a chain. *)
  match find_next (level+1) lab with
  | None -> lab::chain
  | Some(lab1) -> find_last (level+1) (lab::chain) lab1

 
let rec get_last level = function
  (* Finds the last element of a list *)
  | [] -> assert(false)
  | [a] -> a
  | a::t -> get_last (level+1) t

 
let rec mem_remove res a = function
  (* Given a list l0 of labels, this function returns
   * found,l where found is a boolean which is true
   * iff argument a has been found in l0
   * and l is l0 without a when a belongs to l0,
   * or [] when a does not belong to l0.
   * In this last case, the resulting list is unimportant
   * as it will not be used when mem_remove is called.
   *)
  | [] -> false,[]
  | b::t when label_equals a b -> true, List.rev_append res t
  | b::t -> mem_remove (b::res) a t



let rec replace_process level (p0,p1,path) p = 
  if (p = p0 && p != Reach) then p1
  else match p with
  | New(n,p) -> New(n,replace_process (level+1) (p0,p1,path) p)
  | Create_Channel (c1,c2,p) ->
      let rep = replace_process (level+1)  (p0,p1,path) p
      in Create_Channel(c1,c2,rep)
  | Phase (ph,p) -> 
      let rep = replace_process (level+1)  (p0,p1,path) p
      in (Phase(ph,rep))
  | In (c,bounded,t,p) -> 
      let rep = replace_process (level+1)  (p0,p1,path) p in
      (In(c,bounded,t,rep))
  | Out(c,t,p) -> 
      let rep = replace_process (level+1)  (p0,p1,path) p in
      (Out(c,t,rep))
  | Bang(p) -> 
      let rep = replace_process (level+1)  (p0,p1,path) p in
      (Bang(rep))
  | Parallel(pl) ->
      replace_parallel (level+1) (p0,p1,path) pl
  | Match (var,caselist) ->
      Match(var, replace_cases (level+1) (p0,p1,path) caselist)
  | Nul -> Nul
  | Reach -> Reach

and replace_parallel level (p0,p1,path) pl =
  let rec aux level pa i pl =
    match i,pl with
    | _,[] -> assert(false) (* should never happen *)
    | 0,p::t -> replace_process (level+1) (p0,p1,pa) p
    | j,p::t -> aux (level+1) pa (j-1) t
  in match path with
  | [] -> assert(false) (* should never happen *)
  | i::pa -> aux (level+1) pa i pl

and replace_cases level (p0,p1,path) cases =
  let rec aux level pa i cases =
    match i,cases with
    | _,[] -> assert(false) (* should never happen *)
    | 0,(bounded,pattern,p)::t -> 
        [bounded,pattern,replace_process (level+1) (p0,p1,pa) p]
    | j,(bounded,pattern,p)::t -> 
        (bounded,pattern,Nul)::(aux (level+1) pa (j-1) t) 
  in match path with
  | [] -> assert(false) (* should never happen *)
  | i::pa -> aux (level+1) pa i cases

let rec replace_chain_aux level process (subp0,subp1,path0) = function
  | [] -> [replace_process (level+1) (subp0,subp1,path0) process]
  (* process is the root process. 
   * It is an argument of the higher level function *)
  | lab::chain ->
      let p = lab.lab_p in
      let path1 = lab.path in
      let new_p = replace_process (level+1) (subp0,subp1,path0) p 
      in new_p::(replace_chain_aux (level+1) process (p,new_p,path1) chain)

let replace_chain level process = function
  | [] -> assert(false) (* Should never happen *)
  | lab::q ->
      let chain = lab::q in
      replace_chain_aux (level+1) process (next lab.lab_p,Nul,[]) chain
 

let generate_scenario verbose process (scg:label list) =
  (* this function computes the scenario corresponding 
   * to a given scenario_graph (ie multiset of labels).*)

  let rec remove_chain_from_graph chain =
    match chain with
    | [] -> ()
    | lab::r ->
        let _ =
          assert(lab.quantity > 0);
          lab.quantity <- lab.quantity - 1;
        in remove_chain_from_graph r

  in let rec on_one_simple res = function
    | [] -> res
    | lab::l when lab.quantity = 0 -> on_one_simple res l
    | lab::l  ->
        let lab0 = find_first 0 lab in
        let chain = find_last 0 [] lab0 in
        let chain_replaced = replace_chain 0 process chain 
        in let p = get_last 0 chain_replaced in
        let _ = 
          remove_chain_from_graph chain
        in on_one_simple (p::res) l

  in let rec count_labels seen tot = function
    | [] -> tot
    | lab::t -> 
        if List.mem lab seen 
        then count_labels seen tot t 
        else count_labels (lab::seen) (lab.quantity + tot) t

  in let nb = count_labels [] 0 scg

  in (on_one_simple [] scg , nb)

let generate_scenario_Q verbose processQ counted =
  (* This function computes the scenario for the process Q (for equivalence)
   * from the counting elements given in count.ml
   * This only works for simple processes.
   *)
  let rec find_channel = function
    | New(n,p) -> find_channel p
    | Create_Channel(c1,_,_) -> c1
    | Phase(n,p) -> find_channel p
    | In(c,_,_,_) -> c
    | Out(c,_,_) -> c
    | Bang(p) -> find_channel p
    | Nul   -> failwith("Useless null process in protocol Q")
    | Reach -> failwith("Reach process in protocol Q is useless for equivalence")
    | Match(_) -> failwith("There should be no match inside the Q process as the typing result does not work with match constructs for equivalence. Please use pattern matching in inputs instead.")
    | Parallel(_) -> failwith("Process Q is not simple due to some deep parallel construct.")
  in
  let rec get_processes names = function
    | New(n,p)  -> get_processes (n::names) p
    | Parallel(pl) -> get_processes_par names pl 
    | Match(_)  -> failwith("Match is not allowed with equivalence.")
    | p -> names, [(find_channel p), p]
  and get_processes_par names = function
    | [] -> names,[]
    | p::l -> 
        let names1,res_p = get_processes [] p in
        let names2,res_l = get_processes_par [] l in
        let names0 = List.rev_append (List.rev_append names2 names1) names 
        in names0, List.rev_append res_p res_l
  in let names,processes_q = get_processes [] processQ
  in let from_count processes_q counted =
    let rec search_process (ch,n) = function
      | [] -> []
      | (Ch(c,k),p)::pl when c = ch -> [p,n]
      | _::pl -> search_process (ch,n) pl
    in let processes_counted =
      List.flatten (Util.map (fun x -> search_process x processes_q) counted)
    in let rec cut k = function
      | _ when k = 0 -> Nul
      | Nul         -> Nul
      | Reach       -> failwith("Reach process in protocol Q is useless for equivalence")
      | Match(_)    -> failwith("There should be no match inside the Q process as the typing result does not work with match constructs for equivalence. Please use pattern matching in inputs instead.")
      | Parallel(_) -> failwith("Process Q is not simple due to some deep parallel construct.")
      | Bang(p)     -> Bang(cut k p)
      | New(n,p)    -> New(n,cut k p)
      | Create_Channel(c1,c2,p) -> Create_Channel(c1,c2,cut k p)
      | Phase(n,p)  -> Phase(n,cut k p)
      | In(c,bound,t,p) -> In(c,bound,t,cut (k-1) p)
      | Out(c,t,p)  -> Out(c,t,cut (k-1) p)
    in Util.map (fun (p,k) -> cut k p) processes_counted 

  in names,from_count processes_q counted
  
