val max_phase: int
val name: string
val fancy_name: string
val concat_string_array: string -> string array -> string
val stripchars: string -> string -> string
val todo: unit -> unit
val simplify_option_list: 'a option list -> 'a list
val map: ('a -> 'b) -> 'a list -> 'b list
val split: ('a * 'b) list -> 'a list * 'b list
(*
val split3: ('a * 'b * 'c) list -> 'a list * 'b list * 'c list
*)
val map2: ('a -> 'b -> 'c) -> 'a list -> 'b list -> 'c list
val fold_left_i: (int -> 'a -> 'b -> 'a) -> 'a -> 'b array -> 'a
val fold_map : (('a * 'b) -> ('a * 'c)) -> 'a -> 'b array -> ('a * 'c array)
val product_list2: 'a list list -> 'a list list -> 'a list list
val product_list_array: 'a list list array -> 'a list list
val infinite_message: string -> string
val file_name_parser: string -> string

val print_level : int -> unit
val print_v_level : 'a -> 'a -> int -> unit
val print_v_string : 'a -> 'a -> string -> unit
val print_v_newline : 'a -> 'a -> unit -> unit

