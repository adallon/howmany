val generate_scenario: 
  int -> Process.process -> Label.label list -> (Process.process list * int)

val generate_scenario_Q:
  int -> Process.process -> (string * int) list -> Terms.typed_data list * Process.process list
