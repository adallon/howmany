type channel = Ch of string * int

val channel_to_string : channel -> string
val channel_to_string_simple : channel -> string
val change_channel : channel -> int -> channel

type ('a, 'b) generic_process =
    New of 'a * ('a, 'b) generic_process
  | Create_Channel of channel * channel * ('a, 'b) generic_process
  | Phase of int * ('a, 'b) generic_process
  | In of channel * 'a list * 'b * ('a, 'b) generic_process
  | Out of channel * 'b * ('a, 'b) generic_process
  | Bang of ('a, 'b) generic_process
  | Parallel of ('a, 'b) generic_process list
  | Match of 'a * ('a list * 'b * ('a, 'b) generic_process) list
  | Nul
  | Reach

val generic_process_to_string:
    ('a -> string) ->
     ('a list -> 'b -> string) -> ('b -> string) ->
       ('a,'b) generic_process ->  string

type process = (Terms.typed_data, Terms.typed_term) generic_process

val next: process -> process

val process_to_string : process -> string

val unfold : process -> process

val get_terms : ('a, 'b) generic_process -> 'b list

val counting_instructions: ('a,'b) generic_process -> channel option * int


val get_process_subtypes : process -> Terms.type_term list

val check_compliance : process -> unit

val no_replication: int -> process -> process * (Terms.typed_data list) * int

val well_shaped_process: Terms.shape list -> process -> bool

type scenarios = ((process list * int) list * (process list * int )) option

