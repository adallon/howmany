open Terms
open Process
open Label
open Scenario
open Dep_graph
open Compute_dep
open Exceptions

let prepare_graph verbose reductions process type_op =
  let gph = init_graph verbose reductions process None in
  let _ = check_compliance process
  in let gph = compute_graph verbose gph type_op in
  set_empty verbose gph

let handle_res_list verbose process gph res_list =
  let generate_scenario_if_possible = function
    | None -> None
    | Some(r) -> 
        let r_min = minimal_set gph r in
        let sc_opt = overapproximating_scenario gph r_min in
        let p_opt  = generate_scenario verbose process sc_opt in
        let on_each_scenario sc =
          let _ =
            set_multiplicity gph sc
          in
          generate_scenario verbose process sc
        in 
        let result = 
          Util.map on_each_scenario r_min
        in
        Some(result,p_opt)
  in Util.map generate_scenario_if_possible res_list

  (*
let handle_res_list_equiv verbose processP processQ gph res_list =
  let generate_scenario_if_possible = function
    | None -> None
    | Some(r) -> 
        let r_min = minimal_set gph r in
        let sc_opt = overapproximating_scenario gph r_min in
        let p_opt  = generate_scenario verbose process sc_opt in
        let on_each_scenario sc =
          let _ =
            set_multiplicity gph sc
          in
          generate_scenario verbose process sc
        in 
        let result = 
          Util.map on_each_scenario r_min
        in
        Some(result,p_opt)
  in Util.map generate_scenario_if_possible res_list
  *)




let reachability verbose reductions process =
  let gph  = prepare_graph verbose reductions process [] in
  let deps,_,_,_,_ = dependencies verbose gph in
  let reach = find_reach gph in
  let res_list = 
    Util.map to_label_list_list_option
    (Util.map deps reach)
  in handle_res_list verbose process gph res_list

type request = DEP | DEPPLUS | SOUT | SPLUS

let request verbose req reductions (process,tau) =
  let gph = prepare_graph verbose reductions process [tau] 
  in let deps =
    let (_,dep_dep,dep_sout,dep_splus,dep_plus) = 
                            dependencies verbose gph 
    in
    match req with
    | DEP     -> dep_dep
    | DEPPLUS -> dep_plus
    | SOUT   -> dep_sout
    | SPLUS  -> dep_splus
  in
  match search_label Util.max_phase gph tau with
   | None -> assert(false) (* should have been set at this point *)
   | Some(tau_lab) -> 
       let res = to_label_list_list_option (deps tau_lab)
  in 
  begin
    match (handle_res_list verbose process gph [res])
    with
    | [r] -> r
    | _ -> assert(false) (* should be of size 1 *) 
  end


let sout_all verbose reductions process =
  let types = get_process_subtypes process in
  let rec types_to_string res types =
    match res,types with
    | None,[] -> ""
    | Some(res),[] -> res
    | Some(res),tau::l -> 
        let new_res = String.concat ", " [res;type_to_string tau]
        in types_to_string (Some new_res) l
    | None,tau::l ->
        let res = type_to_string tau in
        types_to_string (Some res) l
        
  in let types_string = types_to_string None types in
  let gph = prepare_graph verbose reductions process types in 
  let (_,_,dep_sout,_,_) = dependencies verbose gph in
  let get_result tau =
    match search_label Util.max_phase gph tau with
   | None -> assert(false) (* should have been set at this point *)
   | Some(tau_lab) -> dep_sout tau_lab
  in let results = Util.map get_result types 
  in let final_res = set_union results
  in let f_res = to_label_list_list_option final_res
  in match (handle_res_list verbose process gph [f_res])
  with
  | [r] -> (types_string,r)
  | _ -> assert(false) (* should be of size 1 *) 


let dep_all_point verbose reductions process =
  let types = get_process_subtypes process in
  let rec types_to_string res types =
    match res,types with
    | None,[] -> ""
    | Some(res),[] -> res
    | Some(res),tau::l -> 
        let new_res = String.concat ", " [res;type_to_string tau]
        in types_to_string (Some new_res) l
    | None,tau::l ->
        let res = type_to_string tau in
        types_to_string (Some res) l
 
  in let types_string = types_to_string None types in
  let gph = prepare_graph verbose reductions process types in 
  let (deps,_,_,_,_) = dependencies verbose gph in
  let results = Util.map deps gph.list_labels in
  let final_res = set_union results
  in let f_res = to_label_list_list_option final_res
  in match (handle_res_list verbose process gph [f_res])
  with
  | [r] -> (types_string,r)
  | _ -> assert(false) (* should be of size 1 *) 

let semi_equivalence verbose (dest,test) process =
  let types = get_process_subtypes process in
  let reductions = List.rev_append dest test in
  let other_types_init = 
    let types0 = Util.map (fun tau -> Terms.types_of_tests tau reductions) types in
    List.flatten (List.flatten types0)
  in
  let gph = prepare_graph verbose dest process (List.rev_append other_types_init types) in 
  let (dep_proc,dep_type,dep_sout,dep_splus,_)=dependencies verbose gph 
  in let get_label tau =
    match search_label Util.max_phase gph tau with
      | None -> assert(false) (* should have been set at this point *) 
      | Some(tau_lab) -> tau_lab
    
  in let result_reach = 
    set_union (Util.map dep_proc gph.list_labels)

  in let result_equalities =
    let to_map tau =
      let tau_lab = get_label tau
      in
      set_product2 (dep_type tau_lab) (dep_splus tau_lab) 
    in set_union (Util.map to_map types)
  in let result_reductions =
    let to_map tau =
      let other_types = Terms.types_of_tests tau reductions in
      let tau_lab =  get_label tau 
      in
      let to_map_2 tau_i = dep_type (get_label tau_i)
      in let other_types_deps =
        Util.map (Util.map to_map_2) other_types
      in let to_map3 tau_list =
        let prod_others = set_product tau_list in
        set_product2 (dep_sout tau_lab) prod_others
      in set_union (Util.map to_map3 other_types_deps)
    in set_union (Util.map to_map types)
  in let complete_result =
    set_union [result_reach;result_equalities;result_reductions]
  in let f_res = to_label_list_list_option complete_result
  in match handle_res_list verbose process gph [f_res] with
  | [r] -> r
  | _ -> assert(false) (* should be of size 1 *)
  

