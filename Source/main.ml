open Lexing
open Process
open Parsing_helper
open Conclude_dep
open Rule_checker
open Output
open Equivalence
open Strategy
open Exceptions

let version = "0"
let verbosity_default = 1
let strategy_default = Small
let satequiv_default = false
let deepsec_default = false

let parse filename ic =
  let lexbuf = Lexing.from_channel ic 
  in let initial_position =
    { 
      pos_fname = filename ;
      pos_lnum  = 1;
      pos_bol   = 1;
      pos_cnum  = 1
    }
  in
  let continue = ref true in
  let _ =
    (* we must initialize the position 
     * if we want the lexer to take account of them  
     * *)
    lexbuf.lex_start_p <- initial_position ;
    lexbuf.lex_curr_p  <- initial_position ;
  in
  let p_res0 = init_parsing_result () in
  let p_res = ref p_res0 in
  let _ =
    while (!(continue)) do
      p_res := Grammar.main Lexer.process lexbuf;
      continue := next_step !p_res ;
    done;
  in !p_res

let option_reader opt =
  let rec aux deepsec satequiv std verbosity run count strat = function
    |[] -> (deepsec,satequiv,std,verbosity,run,count,strat)
    | "--satequiv"::l -> aux deepsec  true     std  verbosity run  count strat l
    | "--deepsec"::l  -> aux true     satequiv std  verbosity run  count strat l
    | "--standard"::l -> aux deepsec  satequiv true verbosity run  count strat l
    | "--run"::l      -> aux deepsec  true     std  verbosity true count strat l
    | "--verbose"::l  -> aux deepsec  satequiv std  2 run count strat l
    | "--quiet"::l    -> aux deepsec  satequiv std  0 run count strat l
    | "--normal"::l   -> aux deepsec  satequiv std  1 run count strat l
    | "--count"::l    -> aux deepsec  satequiv std  verbosity run true strat l
    | "--small"::l    -> aux deepsec  satequiv std  verbosity run count Small l
    | "--overapp"::l  -> aux deepsec  satequiv std  verbosity run count Overapp l
    | "--all"::l      -> aux deepsec  satequiv std  verbosity run count All l
    | "--help"::l     -> raise Help
    | _ -> raise OptionError
  in let (deepsec,sateq,std,verb,run,count,strat) = 
    aux deepsec_default satequiv_default false verbosity_default false false strategy_default opt 
  in strat,to_output_options (deepsec,sateq,std,verb,run,count)

let help_message =
  let usage = 
    String.concat "" ["Usage: ./";Util.name;" options filename options \n"]
  in
  let m0 = "\nAvailable options are:\n" in
  let m1 = "  --satequiv Create SAT-Equiv encodings in new files.\n" in
  let m2 = "  --run      Run SAT-Equiv on the encodings. Requires satequiv to be in the path.\n" in
  let m3 = 
    String.concat " " 
    ["  --standard The output corresponds to the";Util.fancy_name;"syntax. Default when --satequiv is not set.\n"] 
  in
  let m4 = "  --verbose  Verbose output. Give the computation steps of the dependencies.\n" in
  let m5 = "  --quiet    Quiet output. Give no information excepted the results. Default when --satequiv is set.\n" in
  let m6 = "  --normal   Normal verbosity. This is the default when --satequiv is not set.\n" in
  let m7 = "  --help     Print this list of options.\n" in
  String.concat "" [usage;m0;m1;m2;m3;m4;m5;m6;m7]


let main () =
  let args = Sys.argv in
  let len = Array.length args in
  let rec sort (opt,files) = function
    | 0 -> (opt,files)
    | i when i < 0 -> raise OptionError
    | i ->
        let arg_i = args.(i) in 
        if arg_i.[0] = '-' 
        then sort (arg_i::opt,files) (i-1)
        else sort (opt,arg_i::files) (i-1)
  in let opt,files  = sort ([],[]) (len-1)
  in let strat,output_opt = option_reader opt
  in let verbosity  = verbosity output_opt
  in
  let filename =
    match files with
    | [file] -> file
    | _  -> raise OptionError
  in let filename_output = String.concat "" ["_howmany_encoding_";Util.file_name_parser filename]
  in let ic = open_in filename
  in  
  let p_res = parse filename ic
  in let _ =
    if verbosity > 0
    then 
      begin
      print_string (parsing_result_to_string p_res);
      print_newline ();
      end
    else ()
  in
  let (queries,data) =  get_query p_res in
  let (reach,dependencies,depplus,sout,splus,soutall,depall,semiequiv,equivalence) = queries in
  let (_,constants,_,reductions) = data in
  let (dest,test,shapes) = reduction_or_test constants reductions in
  (* (dest,test,shapes):
   * dest: destruction rules, like sdec(senc(x,y),y) -> x
   * test: test rules, like checksign(sign(x,y),vk(y)) -> ok
       Those rules are only useful for the static equivalence.
   * shapes: a list representing the shape*)
  if (List.for_all (well_shaped_process shapes) reach)
  then 
  let reach_scenarios = 
     Util.map (reachability verbosity dest) reach
  in let dep_scenarios =
     Util.map (request verbosity DEP     dest) dependencies  
  in let dep_plus_scenarios =
     Util.map (request verbosity DEPPLUS dest) depplus
  in let sout_scenarios = 
     Util.map (request verbosity SOUT   dest) sout
  in let splus_scenarios = 
     Util.map (request verbosity SPLUS  dest) splus
  in let sout_all_scenarios =
     Util.map (sout_all verbosity dest) soutall
  in let dep_all_scenarios =
     Util.map (dep_all_point verbosity dest) depall
  in let semiequiv_scenarios =
     Util.map (semi_equivalence verbosity (dest,test)) semiequiv
  in let seven_scenarios = 
    (dep_scenarios,dep_plus_scenarios,sout_scenarios,splus_scenarios,sout_all_scenarios,dep_all_scenarios,semiequiv_scenarios)
  in let _ =
    final_output strat output_opt data filename_output reach_scenarios seven_scenarios ;
    equivalence_handler verbosity strat output_opt data filename_output (dest,test) equivalence
  in ()
  else print_newline ()

let _ =
  try
    main ()
  with
  | Help ->
      print_string help_message
  | OptionError -> 
      begin
        print_string "Option or file error.\n";
        print_string "There is a problem with a file or an option.\n";
        print_string help_message
      end
  | Term_error m -> 
      begin
        print_string "Syntax error [Term error]: ";
        print_string m;
        print_newline ()
      end
  | Process_error m -> 
      begin
        print_string "Syntax error [Process error]: ";
        print_string m;
        print_newline ()
      end
  | Parsing_error m -> 
      begin
        print_string "Syntax error [Parsing error]: ";
        print_string m;
        print_newline ()
      end
  | Type_compliance_error(t1,t2) ->
      begin
        print_string "Processes are not type compliant due to the following terms:\n ";
        print_string t1;
        print_string "\n ";
        print_string t2;
        print_string "\n Either they are unifiable; or they are matched one against another; and anyway their types differ.\n"
      end
  | SAT_Equiv_error m ->
      begin
        print_string "\nSAT-Equiv encoding error: ";
        print_string m;
        print_newline ()
      end
