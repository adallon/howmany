open Terms
open Process

let scenario_to_string s0 =
  let (s,nb_act) = s0 in
  let rec process_list_to_string = function
    | [] -> ""
    | [p] -> 
        let str = process_to_string p in
        String.concat "" [str;"\n  )\n"]
    | p::q ->
        let str = process_to_string p in
        String.concat "\n  |" [str; process_list_to_string q]

  in let insert_in_list l a =
    if List.mem a l then l else a::l

  in let merge_lists l1 l2 =
    List.fold_left insert_in_list l1 l2

  in let s_no_rep,names = 
    let rec applied_no_rep res i = function
      | [] -> List.rev res
      | x::t -> let a,b,j = no_replication i x in applied_no_rep ((a,b)::res) j t
    in List.split (applied_no_rep [] 0 s)

  in let name_list = 
    List.fold_left merge_lists [] names

  in let name_to_string d =
    let data = typed_data_to_string d
    in String.concat "" ["new ";data;"; "]

  in let name_str = 
    String.concat "" (List.map name_to_string name_list)
  in 
  match s_no_rep with
  | [] -> Some(0),nb_act,"0.\n" (* Empty scenario *)
  | [p] ->
    Some(1),nb_act,String.concat "" [name_str;process_to_string p]
  | pl ->
    Some(List.length pl),nb_act,
    String.concat "" [name_str;"\n  (";process_list_to_string pl]

let scenario_list_to_string = function
    | None    -> [None,0,"bottom"],(None,0,"")
    | Some(s,opt) -> 
        (Util.map scenario_to_string (s:(process list * int)  list)),
        scenario_to_string (opt:process list * int)
        (*simplify_scenarios [] (s:process list list)*)

let scenario_list_to_string_list scenarios =
  let str_l = Util.map scenario_list_to_string scenarios
  in let max m value =
    match m,value with
    | None,_|_,None -> None
    | Some(m),Some(v) -> if m < v then Some(v) else Some(m) 

  in let optimal_scenario opt =
    let separator = "\n --- Overapproximating scenario ---\n"
    in match opt with
    | None,_,_ -> None
    | Some(n),nb_inst,s -> Some(n,nb_inst,String.concat "" [separator;s])

  in let rec next_scenario max_num max_inst res i (scLeft,opt) =
    match scLeft with
      | [] -> max_num,max_inst,List.rev res,optimal_scenario opt
      | (int_op,inst,sc)::sc_l -> 
          let istr = string_of_int i
          in let sc_with_number =
            String.concat "" ["\n --- Scenario ";istr;" ---\n";sc]
          in let new_max = max max_num int_op
          in let new_max_inst = max max_inst (Some inst)
          in next_scenario new_max new_max_inst (sc_with_number::res) (i+1) (sc_l,opt)
  in let str_l1 = Util.map (next_scenario (Some 0) (Some 0) [] 1) str_l
  in Util.map (fun (m,max_i,l,opt) -> m,max_i,String.concat "" l,opt) str_l1 

(*
let print_scenarios_reach scenarios_list =
  let str_list_list =
    let to_map (x,x1,y,z) =
      match z with
      | None -> y
      | Some(n,nb_inst,s) -> String.concat "" [y;s;"\nParallel processes in the overapproximating scenario: ";string_of_int n;"\nNumber of instructions (labels) in the overapproximating scenario: ";  string_of_int nb_inst; "\n\n"]
    in
    Util.map (Util.map to_map) (Util.map scenario_list_to_string_list scenarios_list)
  in
  let str_list =
    Util.map (String.concat "\n\n-- next reaching point -- \n\n") str_list_list
  in let rec print_with_new_line i = function
    | [] -> ()
    | str::l ->
      begin
        print_newline();
        print_string "-- Reachability request ";
        print_int i ;
        print_string " --";
        print_newline();
        print_string str;
        print_newline();
        print_with_new_line (i+1) l
      end
  in 
  begin 
    print_newline ();
    print_newline ();
    print_with_new_line 1 str_list;
    print_newline();
  end

  *)


let print_scenarios_with_string str0 scenarios_and_types_list =
  let types,scenarios_list = Util.split scenarios_and_types_list in
  let str_list = scenario_list_to_string_list scenarios_list
  in 
  let rec print_with_new_line i = function
    | [],[] -> ()
    | taulist::types,(max_num,max_inst,str,opt)::l ->
      begin
        print_newline();
        print_string "--  ";
        print_string str0;
        print_string " request ";
        print_int i ;
        print_string " --";
        print_newline();
        let _ =
          match taulist with
          | None -> ()
          | Some(taulist) ->
             begin 
              print_string "subtypes: ";
              print_string taulist;
              print_newline();
             end
        in
        print_string str;
        let _ =
          match opt with
          | None -> ()
          | Some(n,nb_inst,s) ->
              let _ =
              print_newline ();
              print_string s;
              print_string "\nParallel processes in the overapproximating scenario: ";
              print_int n;
              print_string "\nInstructions (labels) in the overapproximating scenario: ";
              print_int nb_inst;
              print_newline ()
              in ()
        in
        let _ =
          match max_num with
          | None -> ()
          | Some(m) ->
              let _ =
                print_newline();
                print_string "Maximal number of parallel processes in the simple scenarios: ";
                print_int m;
              in ()
        in
        let _ =
          match max_inst with
          | None -> ()
          | Some(m) ->
              let _ =
                print_newline();
                print_string "Maximal number of instructions (labels) in the simple scenarios: ";
                print_int m;
              in ()
        in

        print_newline ();
        print_with_new_line (i+1) (types,l)
      end
    | _ -> assert(false) (* The lists have been split so they have the same length *)
  in 
  begin 
    match str_list with
    | [] -> ()
    | l  ->
        let _ =
          print_newline ();
          print_newline ();
          print_with_new_line 1 (types,str_list);
          print_newline();

        in ()
  end

let aux_none x = (None,x)
let aux_some (tau,scen) = (Some(tau),scen)

let print_scenarios_reach (scen:scenarios list list) =
  let scen_op = Util.map (Util.map aux_none) scen in
  let rec aux i = function
    | [] -> ()
    | sc::sc_list ->
        let _ =
          print_string "\n -- Reachability request ";print_int i; print_string " -- \n";
          print_scenarios_with_string "Reach point " sc ;
        in aux (i+1) sc_list
  in aux 1 scen_op

(*  List.iter (print_scenarios_with_string "Reach point ") scen_op *)

let print_scenarios_dep scen =
  let scen_op = Util.map aux_none scen in
  print_scenarios_with_string "Dependency" scen_op
let print_scenarios_depplus scen = 
  let scen_op = Util.map aux_none scen in
  print_scenarios_with_string "Dependency Plus" scen_op
let print_scenarios_sout scen =
  let scen_op = Util.map aux_none scen in
  print_scenarios_with_string "S_out" scen_op
let print_scenarios_splus scen =
  let scen_op = Util.map aux_none scen in
  print_scenarios_with_string "S+" scen_op
let print_scenarios_soutall soutall_scen =
  let scens = Util.map aux_some soutall_scen in
  print_scenarios_with_string "S_out_all" scens
let print_scenarios_depall depall_scen =
  let scens = Util.map aux_some depall_scen in
  print_scenarios_with_string "Dep_all" scens
let print_scenarios_semiequiv scen =
  let scen_op = Util.map aux_none scen in
  print_scenarios_with_string "Semi-equiv" scen_op



