
let max_phase = 1024 (* used in dependencies in process.ml *)
let name = "howmany" 
let fancy_name = "HowMany"

let concat_string_array sep string_array =
  let len = Array.length string_array in
  let rec aux = function
    | n when n=len  -> ""
    | n when n = (len-1) -> string_array.(n)
    | n -> 
        let str = aux (n+1) in
        String.concat sep [string_array.(n);str]
  in aux 0

let stripchars s cs =
  let len = String.length s in
  let res = Bytes.create len in
  let rec aux i j =
    if i >= len
    then Bytes.to_string (Bytes.sub res 0 j)
    else if String.contains cs s.[i] then
      aux (succ i) (j)
    else 
      begin
        Bytes.set res j s.[i];
        aux (succ i) (succ j)
      end

  in aux 0 0


let todo () =
 begin
   print_string "A function has not been defined";
   assert(false)
 end 

let simplify_option_list l =
  let rec aux res = function
    |[] -> res
    |None::t -> aux res t
    |Some(a)::t -> aux (a::res) t
  in List.rev  (aux [] l)

let map f l =
  let rec rev_map res = function
    | [] -> res
    | a::t -> rev_map ((f a)::res) t
  in rev_map [] (List.rev l)

let split l =
  let to_fold (resa,resb) (a,b) = (a::resa,b::resb)
  in let (resa,resb) = List.fold_left to_fold ([],[]) l
  in List.rev resa, List.rev resb

  (*
let split3 l =
  let rec aux (resa,resb,resc) = function
    | [] -> List.rev resa, List.rev resb, List.rev resc
    | (a,b,c)::t -> aux (a::resa,b::resb,c::resc) t
  in aux ([],[],[]) l
  *)


let map2 f la lb =
  let rec rev_map2 res = function
    | [],[] -> res
    | a::ta,b::tb -> rev_map2 ((f a b)::res) (ta,tb)
    | _ -> raise (Invalid_argument "Util.map2 called on lists of different length." )
  in rev_map2 [] (List.rev la,List.rev lb)

let fold_left_i (f: int -> 'a -> 'b -> 'a) (first:'a) (arr:'b array) =
  let f0 (i,a) b  = (i+1,f i a b)
  in let _,r = Array.fold_left f0 (0,first) arr
  in r


let fold_map (f:('a * 'b) -> ('a * 'c)) (first:'a) (arr:'b array) =
  let new_arr =
    let len = Array.length arr in
    Array.init len (fun i -> None)
  in
  let to_fold i a0 b0 =
    let (a1,b1) = f (a0,b0) in
    let _ =
      new_arr.(i) <- Some(b1)
    in a1
  in let remove_some = function
    | Some(a) -> a
    | None -> assert(false)
    (* Should never happen as each value is set. *)
  in let map_remove t =
    let len = Array.length t in
    let t0 = remove_some t.(0) 
    in
    let new_arr = Array.init len (fun _ -> t0) in
    let rec aux i =
      if len = i then ()
      else
        let ti = t.(i) in 
        let new_ti = remove_some ti
        in 
        begin
        new_arr.(i) <- new_ti;
        aux (i+1)
        end
    in let _ = aux 0 in new_arr
  in let a = fold_left_i to_fold first arr
  in (a,map_remove new_arr)


let product_list2 l1 l2 =
  let aux a =
    map (List.rev_append (List.rev a)) l2
  in let all_res = map aux l1
  in List.fold_left List.rev_append [] all_res

let product_list_array arr = 
  Array.fold_left product_list2 ([[]]) arr

(*
let concat_string_list sep string_list =
  let rec aux = function
  | [] -> ""
  | [a] -> a
  | a::t -> 
        String.concat sep [sort_to_string a ; sort_list_to_string t ]
  in aux string_list
*)

let infinite_message toolname =
  let first_part = "One of the scenarios is infinite.\nIt is impossible to compute a "
  in let last_part = " encoding in this case.\nPlease look at the standard output and remove the scenario with result bottom."
  in String.concat "" [first_part;toolname;last_part]

let file_name_parser filename =
  let clean_name =
    let slash = Str.regexp "/" in
    let list_words = Str.split slash filename in
    let rec take_last = function
      | []   -> assert(false)
      | [a]  -> a
      | a::t -> take_last t
    in take_last list_words
  in
  if Str.last_chars clean_name 3 = ".hm"
  then 
    let dot = Str.regexp "\\." in
    let list_elements = Str.split dot clean_name in
    let rec all_but_last res = function
      | [] -> assert(false)
      | [a]  -> List.rev res
      | a::l -> all_but_last (a::res) l
    in String.concat "." (all_but_last [] list_elements)
  else clean_name

let rec print_level = function
  | 0 -> ()
  | n -> 
      print_string "  "; 
      print_level (n-1)

let print_v_level vmin v lev =
    if v > vmin
    then print_level lev
    else ()

let print_v_string vmin v str =
    if v > vmin
    then print_string str
    else ()

let print_v_newline vmin v () =
    if v > vmin
    then print_newline ()
    else ()


