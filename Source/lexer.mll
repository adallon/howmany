{
open Grammar;;
open Lexing;;
open Parsing_helper;;

exception ForbiddenChar of char * int * int * int;;
exception ForbiddenString of string * int * int * int;;


let forbidden_char ch lexbuf =
  let first_p = lexbuf.lex_start_p in
  let last_p  = lexbuf.lex_curr_p  in
  let (line,first,last) = 
    Parsing_helper.pos_diff (first_p:position) (last_p:position)
  in
  match last - first with
  | -1 -> raise (ForbiddenChar (ch,line,first,last))
  |  0 -> raise (ForbiddenChar (ch,line,first,last))
  | n -> assert(false)

let forbidden_string str lexbuf =
  let first_p = lexbuf.lex_start_p in
  let last_p  = lexbuf.lex_curr_p  in
  let (line,first,last) = 
    Parsing_helper.pos_diff (first_p:position) (last_p:position)
  in raise (ForbiddenString (str,line,first,last))


let keyword_table = Hashtbl.create 15 ;;


let _ =
  let keywordlist = 
    ["let",LET ; "new",NEW ; "new_ch", NEWCHANNEL ; "phase",PHASE ; "reduc",REDUC ; "match",MATCH ; "with",WITH ;
      "in",IN  ; "out",OUT ; "channel",CHANNEL ; "inclusion", EQUIVALENCE ; "semiequiv", SEMIEQUIV ;  
      "public",PUBLIC ; "free" , PUBLIC ; "fun",FUN ; "bitstring",BITSTRING ; "atom",ATOM ; "begin" , BEGIN ; "end",END ; "Reach", REACH ; "reachability", REACHABILITY ; "dep", DEP ; "depplus", DEPPLUS ; "sout", SOUT ; "splus", SPLUS ; "sout_all", SOUTALL ; "dep_all", DEPALL  ]
    in List.iter (fun (key,tok) -> Hashtbl.add keyword_table key tok ) keywordlist
}

let ident = (['a'-'z' 'A'-'Z']['a'-'z' 'A'-'Z' '0'-'9' '_']*("'"?))

rule process =
  parse 
    ','  { COMMA }
  | '.'  { DOT }
  | "|"  { PAR }
  | "!"  { BANG }
  | "(*" { parenthesisComment 1 lexbuf }
  | "*)" { forbidden_string "*)" lexbuf }
  | "/*" { slashComment lexbuf }
  | "*/" { forbidden_string "*/" lexbuf }
  | "//" { linecomment lexbuf }
  | '('  { LEFTPAR }
  | ')'  { RIGHTPAR }
  | '['  { LEFTBRACKET }
  | ']'  { RIGHTBRACKET }
  | "->" { ARROW }
  | '='  { EQ }
  | ';'  { SEMICOLON }
  | ':'  { COLON }
  | ' '  { process lexbuf }
  | '\n' { Lexing.new_line lexbuf ; process lexbuf }
  | ident as id  { try Hashtbl.find keyword_table id with Not_found -> IDENT(id) }
  | ['0'-'9']+ as number 
  { 
    try INT(int_of_string (number)) 
    with Failure _ -> raise (forbidden_string number lexbuf )
  }
  | eof { EOF }
  | _ as ch  { forbidden_char ch lexbuf }

and parenthesisComment n =
  parse
  "*)" 
  { 
    if n = 1 
    then process lexbuf 
    else parenthesisComment (n-1) lexbuf
  }
  | "(*" {parenthesisComment (n+1) lexbuf}
  | '\n' {Lexing.new_line lexbuf ; parenthesisComment n lexbuf}
  | _ {parenthesisComment n lexbuf}

and slashComment =
  parse
  "*/" { process lexbuf }
  | '\n' {Lexing.new_line lexbuf ; slashComment lexbuf}
  | _ {slashComment lexbuf}


and linecomment =
  parse
    '\n' {Lexing.new_line lexbuf ; process lexbuf}
  |  _ {linecomment lexbuf}

{
  (* TRAILER : instructions à la fin du lexing *)
}
