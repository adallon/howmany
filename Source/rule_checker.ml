open Terms
open Exceptions


let hypothesis = String.concat "\n  " 
  ["Recall that the hypothesis on reduction rules are:";
  "* each rule is under the form g(t1,...,tn) -> r";
  "* each g appears exactly in one rule";
  "* each ti is linear";
  "* each ti which is not a variable is the shape of its head function symbol";
  "* there is at most one variable x with several occurences in t1,...,tn";
  "* there is at least one rule with such a variable (one non linear rule)";
  "* if there is such a x, it occurs (exactly once) at an atomic position in t1";
  "* either r is a term built from constructors and constants (no variable)";
  "  or r is a direct subterm of t1";
  "* the only variable of t2,...,tn is x"]

let complete_message_hypothesis =
  String.concat "\n\n" [hypothesis ; "For more information, see the papper. Examples are provided with the tool. They include classical primitives."]


let get_shapes rules =
  let to_fold res (des,args,right) =
      List.rev_append (get_shapes_term args) res
  in let shapes = List.fold_left to_fold [] rules
  in 
    try
      if compatibility shapes
      then shapes
      else assert(false)
      (* should never occur as check_compatibility
       * returns either true or an exception *)
    with
    | Term_error _ ->
        let message = "You tried to define a reduction rule where a tuple has a composite subterm.\n This implies that the shape of the built-in tuples are non-trivial, and thus it is not allowed.\n Using tuples in reduction rules is allowed only when all of their subterms are variables." 
        in raise (Term_error message)
    | Compatibility_error name ->
        let message = String.concat "" 
        [" The shape of function symbol ";name;
        " can not be defined.\n It would violate the compatibility property.\n";
        " Recall that shapes are computed automatically from the reduction rules.\n\n";
        complete_message_hypothesis]
        in raise (Term_error message)

let check_hypothesis rules =
  let rec included l2 = function
    | [] -> true
    | a::t -> (List.mem a l2)&&(included l2 t)
  in
  let rec non_linear_list res = function
    | [] -> res
    | a::t ->
        if (List.mem a t)
        then 
          begin
          if List.mem a res
          then non_linear_list res t
          else non_linear_list (a::res) t
          end
        else non_linear_list res t
  in
  let all_equal l =
    let rec aux a = function
      | [] -> true
      | b::t -> (a=b)&&(aux a t)
    in match l with
    | [] -> true
    | a::t -> aux a t
  in
  let check_one ((des,args,right):reduction) =
    (* Each ti is linear *)
    let vars = Util.map get_variables args in 
    let non_lin = Util.map (non_linear_list []) vars in
    let f x = (x == []) in
    let _ = 
      if (List.for_all f non_lin)
      then None
      else 
        let message = String.concat "" 
        ["The reduction rule with symbol ";des;
        " has a non-linear strict subterm. This is forbidden.\n\n";
        complete_message_hypothesis]
        in raise (Term_error message)
    in

    (* The rule has at most one non linear variable *)
    let to_fold_var res t =
      List.rev_append (get_variables t) res
    in let vars_left  = List.fold_left to_fold_var [] args 
    in let vars_right = get_variables right 
    in
    let _ =
      (* The variables at right are included in the variable at left *)
      if (included vars_left vars_right)
      then None
      else 
        let message = String.concat "" ["The reduction rule defining destructor ";
        des;" is not subterm-convergent,\n"; 
        " as the variables in the RHS are not included in the variables";
        " of the LHS.\n This is forbidden.\n\n";complete_message_hypothesis]
        in raise (Term_error message)
    in 
    let non_lin_list = non_linear_list [] vars_left in
    let _ =
      if all_equal non_lin_list
      then true
      else 
        let message = String.concat "" 
        ["The reduction rule with symbol ";des;
        " has several non linear variables.\n It is forbidden.\n\n";
        complete_message_hypothesis]
        in raise (Term_error message)
    in let _ = 
    (* This non linear variable occurs exaclty once at atomic position in t1 *)
      match non_lin_list with
      | [] -> None
      | x::t -> 
          let t1 = List.hd args in
          let _ = 
    (* This non linear variable occurs exactly once in t1.
     * As t1 is linear, it is sufficient to check that
     * this non linear variable occurs at least once in t1 *)
            if List.mem x (get_variables t1)
            then
              None
            else
              let message = String.concat ""
 ["The reduction rule with symbol ";des;
      "has a non linear variable,\n";
      "but this non linear variable does not occur in its first subterm.\n"; 
      "This is forbidden.\n\n";complete_message_hypothesis]
              in raise (Term_error message)
          in
          let _ = 
            (* This non linear variable occurs only at atomic positions in t1 *)
            if atomic_position t1 x
            then None
            else
              let message = String.concat ""
              ["The reduction rule with symbol ";des;
              " has its non linear variable\n";
              "occuring at a non-atomic position of the first subterm.\n";
              "This is forbidden.\n\n"; complete_message_hypothesis]
              in raise (Term_error message)
          in None
    in let _ =
      (* x is the only variable of t2,...,tn *)
      match non_lin_list with
      | [] -> None
      | x::t ->
          let _ =
            match args with
            | [] -> assert(false)
            |_::q ->
                let vars = Util.map get_variables q
                in
                let is_x y = (x = y) in
                if (List.for_all (List.for_all is_x) vars)
                then None
                else
                  let message = String.concat "" 
                  ["The reduction rule with symbol ";des;
            " has a non linear variable,\n";
            "but this non linear variable is not the only variable of the direct subterms (t2,...,tn) with index i > 1.";
            "This is forbidden.\n\n";
            complete_message_hypothesis]
                  in raise (Term_error message)
          in None
    (* Says if the rule is linear or not *)
    in (non_lin_list != []) 
    (* There is at least one non linear rule *)
  in let _ =
    if List.exists check_one rules
    then None
    else
      let message = String.concat "" 
      ["All defined rules are linear.\nThis is forbidden.\n\n";
    complete_message_hypothesis]
      in raise (Term_error message)
  in true

(*
 * Hypothesis:
   * Either test or destructor 
   * Shapes  (Compatible)
   * g(t1,...,tn) where each ti is linear 
   * there is at most one non linear variable x
     with several occurences 
   * x occurs (exactly once) at an atomic position in t1.
   * the only variable of t2,...,tn is x 
   * when g is a destructor (not a test)
     g(t1,...,tn) -> r with r a DIRECT subterm of t1. 
   * at least one non-linear rule 
 *
 *
 *)


let check_destructor rules =
  let check_one_rule (des,args,right) =
    let t1 = List.hd args 
    in
    let _ =
      if direct_subterm t1 right
      then None
      else
        let message = 
          String.concat "" ["The reduction rule with destructor ";des;
      " [";des;"(t1,...,tn) -> r] does not follow the following hypothesis:\n";
  "  * r is a direct subterm of t1\n";
  complete_message_hypothesis]
        in raise (Term_error message)
    in true
  in List.for_all check_one_rule rules



let reduction_or_test constants rules =
  let is_test (des,args,right) =
    is_built_from constants right 
  in let rec aux reds tests = function
    | [] -> reds,tests
    | r::t ->
        if is_test r
        then aux reds (r::tests) t
        else aux (r::reds) tests t 
  in let shapes = get_shapes rules 
  in let (reds,tests) = aux [] [] rules 
  in if (check_hypothesis rules) && (check_destructor reds)
  then (reds,tests,shapes)
  else assert(false) 
  (* check_hypothesis returns either true or an exception. *)
