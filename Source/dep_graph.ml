open Terms
open Process
open Label
open Exceptions


type aux_label = int * bool * process * (type_term option) * (type_term rho_element list option)
(* int: phase 
 * bool: is replicated?
 * process: process corresponding to the label  
 * type_term option: If the process is not Reach, there is a corresponding type term
 * last element: rho result on outputs
 * *)

type aux_res_element = aux_label option * aux_label * position list * (int list)
 (* aux_label option: previous label if there is any
  * aux_label: current label
  * position list: list of marked positions
  * int list: path toward parallel composition and matches *)

type aux_res = aux_res_element list


let print_marked_positions verbose t plist =
  if verbose > 0 then
    let _ =
      let print_pos p =
        let _ =
          print_string " ";
          List.iter (fun x -> print_int x ; print_string "." ) (List.rev p);
        in ()
      in if plist = [] 
         then ()
         else let _ =
           print_string "\nmarked positions in term ";
           print_string (untyped_term_to_string (untype t));
           print_string "\n [";
           print_string (typed_term_to_string t);
           print_string "]:\n";
           List.iter (fun x -> print_pos x) plist;
           print_newline ()
         in ()
    in ()
  else ()


let sequential_dependencies verbose rho_prep process q_option =
  let included s0 s1 =
    (* checks if s0 is a sub-multiset of s1 *)

    let rec is_in res a = function
      (* finds a and removes it if it is in the list *)
      | [] -> false,[]
      | b::l when a = b -> true,List.rev_append res l
      | b::l -> is_in (b::res) a l
    in let to_fold (b,containing_list) included_elmt =
      if b
      then is_in [] included_elmt containing_list
      else (false,[])
    in let b,_ =  List.fold_left to_fold (true,s1) s0
    in b

  in let rec should_mark (t,p,s) = function
    (* checks if (t,s) has been seen in the list of seen labels *)
    | [] -> false
    | (t0,_,s0)::l -> (t0 = t && included s0 s)||(should_mark (t,p,s) l)

  in let mark sigma seen_terms t =
    (* finds all marked positions *)
    let actual_terms = Util.map (replace sigma) seen_terms in

    let seen_rho =
      let rec cumulate_rho res = function
        | [] -> res
        | t0::l -> 
            let rho_t0 = rho_term rho_prep t0 in
            cumulate_rho (List.rev_append rho_t0 res) l
      in cumulate_rho [] actual_terms
    in 
    let rho_els = rho_term rho_prep (replace sigma t) in

    let to_fold res rho_el =
      if should_mark rho_el seen_rho
      then let (_,p,_) = rho_el in p::res
      else res
    in List.fold_left to_fold [] rho_els

  in let rec aux ph is_rep (res:aux_res) path sigma_match (seen_terms:(typed_term list)) (prev:aux_label option) = function
    | New(n,p) -> aux ph is_rep res path sigma_match seen_terms prev p
    | Create_Channel(c1,c2,p) -> aux ph is_rep res path sigma_match seen_terms prev p
    | Phase(ph1,p) ->
        if ph1 > ph 
        then aux ph1 is_rep res path sigma_match seen_terms prev p
        else
          let message = 
            String.concat "" 
            ["Phases should be non-decreasing.";
            " This is not the case as ";
            process_to_string (Phase(ph1,p));
            " is under the scope of phase "; string_of_int ph; "\n"]
          in raise (Process_error message)
    | In(c,bound,t,p) ->
        let new_label = (ph,is_rep,In(c,bound,t,p), Some(get_type t),None) 
        in let marked_positions = mark sigma_match seen_terms t
        in let new_seen = t::seen_terms
        in let new_res = (prev,new_label,marked_positions,path)::res
        in let _ =
          print_marked_positions verbose t marked_positions
        in aux ph is_rep new_res [] sigma_match new_seen (Some(new_label)) p
    | Out(c,t,p) ->
        let type_t = get_type t in
        let new_label = 
          (ph,is_rep,Out(c,t,p), Some(type_t),Some(rho rho_prep type_t)) 
        in let marked_positions = mark sigma_match seen_terms t
        in let new_seen = t::seen_terms
        in let new_res = (prev,new_label,marked_positions,path)::res
        in let _ =
          print_marked_positions verbose t marked_positions
        in aux ph is_rep new_res [] sigma_match new_seen (Some(new_label)) p
    | Bang(p) -> aux ph true res path sigma_match seen_terms prev p
    | Parallel(pl) ->
        let to_fold (i,res) p =
          (* (int*res -> p  -> int*new_res) 
           * ph and prev are constant for each call, but res changes
           *)
          (i+1,aux ph is_rep res (i::path) sigma_match seen_terms prev p)
        in let (_,res_final) = 
          List.fold_left to_fold (0,res) pl
        in res_final
    | Match(var,cases) ->
        let to_fold (i,res) (_,t,p) =
          (* int*res -> case -> int*new_res *)
          let sigma_match1 = add_to_subst sigma_match (var,t) in 
          (i+1, aux ph is_rep res (i::path) sigma_match1 seen_terms prev p)
        in let (_,res_final) = 
          List.fold_left to_fold (0,res) cases
        in res_final
    | Nul   -> res
    | Reach -> 
        let last_label = (ph,is_rep,Reach,None,None) in 
        (prev,last_label,[],path)::res
  in aux 0 false [] [] [] [] None process



let compute_basic_graph verbose rho_prep process q_opt =
  (* creates the basic graph structure of sequential dependencies *)
  
  let rec find_prev (alpha:label) = function
  (* finds the previous label inside sequential dependencies *)
    | [] -> assert(false) (* should never occur: label must exist *)
    | (prev,lab,_,_)::t when compare_label alpha lab -> prev
    | _::t -> find_prev alpha t

  in let rec find_label lab = function
  (* transforms the label to have the label type *)
    | [] -> assert(false) (* should never occur: label must exist *)
    | alpha::t when compare_label alpha lab -> alpha
    | _::t -> find_label lab t

  in let set_one_pred seqs label_list alpha =
    (* uses above functions to set one predecessor *)
    match find_prev alpha seqs with
    | None -> set_pred alpha None
    | Some(ph,is_rep,p,tau,rho_op) -> 
        let beta = find_label (ph,is_rep,p,tau,rho_op) label_list in
        set_pred alpha (Some(beta))

  in let set_all_preds seqs label_list =
    (* uses above functions to set all the predecessors *)
    List.iter (set_one_pred seqs label_list) label_list

  in let sequential = sequential_dependencies verbose rho_prep process q_opt
  in let label_and_rho_list =
    let aux = function
      |(_,(ph,is_rep,p,tau_op,rho_op),marked,path) -> 
          let lab = create_label (ph,is_rep,p,None,tau_op,path,marked) in
          lab,rho_op
    in Util.map aux sequential
  
  in let label_list =
    Util.map (fun (x,y) -> x) label_and_rho_list
  
  in let rec get_rho_list res label_and_rho_list =
    let rec add_to_rho_list res (lab0,tau0,pos0,s0) = function
      | [] -> (lab0,tau0,pos0,s0)::res
      | (lab,tau,pos,s)::t ->
          if (lab.lab_p = lab0.lab_p) && (tau0 = tau) && (pos0 = pos) && (s0 = s)
          then List.rev_append res ((lab,tau,pos,s)::t)
          else add_to_rho_list ((lab,tau,pos,s)::res) (lab0,tau0,pos0,s0) t
    in let rec append l1 l2 = 
      List.fold_right (add_to_rho_list []) l2 l1

    in match label_and_rho_list with
    | [] -> res
    | (lab,None)::l -> get_rho_list res l
    | (lab,Some(rho_elements))::l ->
        let new_res =
          Util.map (fun (tau,pos,s) -> (lab,tau,pos,s)) rho_elements
       (* in  get_rho_list (List.rev_append new_res res) l *)
        in get_rho_list (append new_res res) l 

  in let rho_list = get_rho_list [] label_and_rho_list

  in set_all_preds sequential label_list; label_list,rho_list

type dependency_graph =
  { 
    list_labels : label list;
    list_types  : type_label list;
    rho_list    : 
      (label * type_term * position * (type_term list)) list;
    labels_seen : label list;
    types_seen  : type_label list;
    dishonest   : type_term list;
    name_types  : type_term list;
  }

let graph_to_string gph =
  (* simple version *)
  let str_lab = 
    Util.map label_to_string gph.list_labels 
  in
  String.concat "\n" str_lab


let set_multiplicity gph scenario =

  let rec zero = function
    | [] -> ()
    | lab::q ->
       let _ =
        lab.quantity <- 0
       in zero q 
  in
  let rec count = function
    | [] -> ()
    | lab::q ->
        let new_quantity =
          if lab.is_replicated 
          then 1 + lab.quantity
          else 1  
          (* When the label is not replicated, its quantity is 1.
           * Here, as it has been seen by count, it is at least 1.
           * Thus it is exactly 1. *)
        in
        let _ =
          lab.quantity <- new_quantity
        in count q
  in
  let _ =
    zero gph.list_labels;
    count scenario
  in ()



let get_multiplicity gph scenario =
  let _ =
    set_multiplicity gph scenario
  in
  let labs = gph.list_labels in
  Util.map (fun lab -> lab.quantity) labs

let set_scenario_from_multiplicity gph mult =
  let labs = gph.list_labels in
  let rec multiplie res lab = function
    | 0 -> res
    | n -> multiplie (lab::res) lab (n-1)
  in
  let rec aux res = function
    |[],[] -> res
    | lab::q,m::l ->
        let _ =
          lab.quantity <- m
        in aux (multiplie res lab m) (q,l)
    | _ -> assert(false) (* both lists should have the same size *)
  in aux [] (labs,mult)


let minimal_set gph scenarios =
  
  let add_multiplicity sc =
    let mult = get_multiplicity gph sc
    in (sc,mult)
  
  in let scenarios_with_mult =
    Util.map add_multiplicity scenarios
  in 

  let rec inclusion = function
    |[],[] -> true
    |n1::l1,n2::l2 -> (n1 <= n2)&&(inclusion (l1,l2)) 
    |_,_ -> assert(false) (* the list of labels should be the same *)
  in
  let rec add_to_list res sc_list (sc,mult) =
    match sc_list with
    | [] -> (sc,mult)::res
    | (sc1,mult1)::q when inclusion (mult,mult1) ->
        List.rev_append res ((sc1,mult1)::q)
    | (sc1,mult1)::q when inclusion (mult1,mult) ->
        List.rev_append res ((sc,mult)::q)
    | (sc1,mult1)::q ->
        add_to_list ((sc1,mult1)::res) q (sc,mult)

  in let only_max =
    List.fold_left (add_to_list []) [] scenarios_with_mult

  in Util.map (fun (x,mult) -> x) only_max 

let overapproximating_scenario gph scenarios =
 
  let multiplicity sc =
    let mult = get_multiplicity gph sc
    in mult
  in let all_mult =
    Util.map multiplicity scenarios
  in 

  let rec maximal_multiplicity res sc1 sc2 =
    match sc1,sc2 with
    |[],[] -> List.rev res
    |n1::l1,n2::l2 -> maximal_multiplicity ((max n1 n2)::res) l1 l2
    |_,_ -> assert(false) (* the list of labels should be the same *)
  in let optimal = 
    match all_mult with
    | [] -> []
    | m::l -> List.fold_left (maximal_multiplicity []) m l
  in set_scenario_from_multiplicity gph optimal
 


let init_graph verbose reductions process q_opt =
  let rho_prep = prepare_rho reductions in
  let (list_label, rho_list) = 
    compute_basic_graph verbose rho_prep process q_opt
  in
  let t = get_terms process in
  let dishonest = get_dishonest_types t in
  let name_types = get_name_types  t in
  {
    list_types  = [];
    list_labels = list_label;
    rho_list    = rho_list;
    labels_seen = [];
    types_seen  = [];
    dishonest   = dishonest;
    name_types  = name_types ;
  }

let see_label gph alpha =
  {
    list_types  = gph.list_types;
    list_labels = gph.list_labels;
    rho_list    = gph.rho_list;
    labels_seen = alpha::gph.labels_seen;
    types_seen  = gph.types_seen;
    dishonest   = gph.dishonest;
    name_types  = gph.name_types;
  }

let see_type gph tau_lab =
  {
    list_types  = gph.list_types;
    list_labels = gph.list_labels;
    rho_list    = gph.rho_list;
    labels_seen = gph.labels_seen;
    types_seen  = tau_lab::gph.types_seen;
    dishonest   = gph.dishonest;
    name_types  = gph.name_types;
  }

let add_list_type gph tau_lab =
  {
    list_types  = tau_lab::gph.list_types;
    list_labels = gph.list_labels;
    rho_list    = gph.rho_list;
    labels_seen = gph.labels_seen;
    types_seen  = gph.types_seen;
    dishonest   = gph.dishonest;
    name_types  = gph.name_types;
  }

let is_dishonest verbose gph tau = 
  if (List.mem tau (gph.dishonest))
  then true
  else
    let _ =
      if verbose > 0 then
        begin
        print_string "\n  honest_type: ";
        print_string (type_to_string tau);
        if verbose > 1 then print_newline ()
        end
      else ()
    in false

let find_rho gph tau =
  let rec aux res = function
    |[] -> res
    |(lab,tau0,pos,s)::l ->
        if tau = tau0
        then aux ((lab,pos,s)::res) l
        else aux res l
  in aux [] gph.rho_list

let find_reach gph =
  let rec aux res = function
    | [] -> res
    | lab::l ->
        if lab.lab_p = Reach 
        then aux (lab::res) l
        else aux res l
  in aux [] gph.list_labels

let search_label ph0 gph tau = 
  let rec aux = function
    |[] -> None
    |lab::t ->
      if (is_type_label lab tau)
      then
        begin
        if ph0 < lab.type_phase 
        then 
          begin
          lab.type_phase <- ph0;
          Some(lab)
          end
        else 
          Some(lab)
        end
      else aux t
  in aux gph.list_types

let rec type_to_label ph0 gph tau =
  match search_label ph0 gph tau with
  | None ->
    let subt = get_subtypes tau in
    let subt_labs,gph1 = 
      type_array_to_label ph0 gph subt 
    in
    let tau_lab = create_type_label (ph0,tau,subt_labs)
    in tau_lab,add_list_type gph1 tau_lab
  | Some(tau_lab) -> tau_lab,gph

and type_array_to_label ph0 gph subt =
  let n = Array.length subt in
  let res = Array.make n None in
  let rec aux ph0 gph0 = function
    | i when i = n -> res,gph0  
    | i -> 
        let new_lab,gph1 = type_to_label ph0 gph0 (subt.(i)) in
        begin
          res.(i) <- Some(new_lab) ;
          aux ph0 gph1 (i+1)
        end 
  in let remove_some = function
    | Some(x) -> x
    | None -> assert(false) (* should never happen *)
  in let arr,gph1 = aux ph0 gph 0
  in (Array.map remove_some arr), gph1

let print_level = Util.print_level
let print_v_level = Util.print_v_level
let print_v_string = Util.print_v_string
let print_v_newline = Util.print_v_newline



let compute_graph verbose gph type_list =

  let print_verb_level = print_v_level 1 verbose in
  let print_verb_string = print_v_string 1 verbose in
  let print_verb_newline = print_v_newline 1 verbose in
  let _ = 
    print_verb_level 0;
    print_verb_string "compute_graph";
    print_verb_newline ()
  in

  let gph =
    let to_fold gph tau =
      let _,gph = type_to_label Util.max_phase gph tau
      in gph
    in List.fold_left to_fold gph type_list
  in
  let on_input level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "on_input with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    let tau =
      match alpha.type_term with
      |Some(tau0) -> tau0
      | None -> assert(false) (* Inputs should be typed *)
    in let tau_lab,gph1 =
      type_to_label Util.max_phase gph tau
    (* The phase max_phase is used to be decided later.
     * As it can only decrease, it is given an arbitrary maximal value,
     * set as 1024.
     *)
    in let _ =
      alpha.lab_type <- Some(tau_lab)
    in gph1
  in

  let on_label level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "on_label with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match alpha.lab_p with
    | Reach|Out(_,_,_) -> gph
    | In(c,bound,t,p)  -> on_input (level+1) gph alpha
    | _ -> assert(false) (* should not happen *)
  in

  let set_rho level gph tau both_rho public =
  
    let _ =
      print_verb_level level;
      print_verb_string  "set_rho on type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ()
    in
    let rho0 = find_rho gph tau.type_tau in
    let rho1,gph1 =
      let aux_rho (final_res,gph) (lab,pos,s)=

        let to_fold (res,gph) tau_elmt =
          let label,gph1 = type_to_label lab.lab_phase gph tau_elmt
          in (label::res,gph1)
        in let r,gph1 = List.fold_left to_fold ([],gph) s
        in (lab,pos,r)::final_res,gph1

      in List.fold_left aux_rho ([],gph) rho0

    in let rho_element level gph (res,resPlus) (alpha,pos,tau_l) =
      let _ =
        print_verb_level level;
        print_verb_string  "rho_element in set_rho";
        print_verb_newline ()
      in
      let is_marked = List.mem pos alpha.marked_positions
      in
      if (is_marked || public)
      then 
        let _ =
          print_verb_level level;
          print_verb_string  "marked (or public) element";
          print_verb_newline ()
        in
        match pos with
        | [] -> 
            let _ =
              assert(tau_l = []); 
              print_verb_level level;
              print_verb_string "top position: dep_plus";
              print_verb_newline ()
            in (res,alpha::resPlus)
        | _  -> (res,resPlus)
      else 
        let _ =
          print_verb_level level;
          print_verb_string  "element not marked and not public";
          print_verb_newline ()
        in ((alpha,tau_l)::res,resPlus)

    in let rho_list,rho_plus =
      List.fold_left (rho_element (level+1) gph1) ([],[]) rho1
    in let _ =
      tau.rho_plus <- Some(rho_plus);
      if both_rho
      then tau.tl_rho_list <- Some(rho_list)
      else ()
    in gph1

  in
  let on_type level gph tau =
    let _ = 
      print_verb_level level;
      print_verb_string  "on_type with type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ()
    in
    let public_tau = is_public_type verbose gph.name_types tau.type_tau
    in
    if (is_tuple tau.type_tau  || public_tau )
    then
      let _ = 
        print_verb_level level;
        print_verb_string "public type or tuple";
        print_verb_newline ();
        tau.tl_rho_list <- Some([]) 
      in set_rho (level+1) gph tau false public_tau
    else 
      if tau.tl_rho_list = None
      then set_rho (level+1) gph tau true false
      else gph

  in let on_all_labels level gph =
    let _ =  
      print_verb_level level;
      print_verb_string  "on_all_labels ";
      print_verb_newline ()
    in List.fold_left (on_label (level+1)) gph gph.list_labels

  in let on_all_types level gph =
    let _ =  
      print_verb_level level;
      print_verb_string  "on_all_types ";
      print_verb_newline ()
    in List.fold_left (on_type (level+1)) gph gph.list_types

  in let one_following level gph alpha =
    let _ =  
      print_verb_level level;
      print_verb_string  "one_following with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match alpha.lab_pred with
    | None -> gph
    | Some(beta) ->
        let _ =
          beta.following <- alpha::beta.following
        in gph

  in let set_following level gph =
    let _ =  
      print_verb_level level;
      print_verb_string  "set_following ";
      print_verb_newline ()
    in List.fold_left (one_following (level+1)) gph gph.list_labels

  in
  let rec iterate n gph =
    let gph = on_all_types 0 gph
    in let m = List.length gph.list_types
    in match n<m with
    |true -> iterate m gph
    |false -> gph
  in
  let gph = set_following 0 (on_all_labels 0 gph)
  in let n =  List.length gph.list_types 
  in iterate n gph


