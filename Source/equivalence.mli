val equivalence_handler : 
  int -> Strategy.strategy -> Output.output_options -> Tool_encoder.process_data -> string -> 
    (Terms.reduction list*Terms.reduction list) -> (Process.process * Process.process) list -> unit
