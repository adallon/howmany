open Terms
open Process
open Exceptions
open Strategy

let toolname = "SAT-Equiv"
let toolext  = ".pi"
let command file = String.concat "" ["satequiv -p ";file]
let begin_comment = ""
let end_comment = "\n"

let satequiv_params = (toolname,toolext,command,begin_comment,end_comment)

(* Printing the SAT-Equiv output corresponding to a process *)

let to_remove = "_-"

(* Second field: public data *)

let string_public const other_constants =
  let field_sep = "\n___ Public constants" in
  (* Note that SAT-Equiv removes useless constants
   * so we can add those anyway *)
  let to_map t = Util.stripchars (typed_term_to_string t) to_remove  in
  let const_str = 
    Util.map to_map (List.rev_append other_constants const) 
  in String.concat "\n" (field_sep::const_str)


(* Third field: private data *)

let string_private priv =
  let field_sep = "\n___ Private constants (aka fresh names in HowMany)"
  in
  let to_map t = Util.stripchars (typed_term_to_string t) to_remove
  in let priv_str = 
    Util.map to_map priv in
  String.concat "\n" (field_sep::priv_str)

(* Next fields: process P and process Q *)


  (* Forth field: process P *)

let field_sep_P = 
  "\n___ Process P\n"

  (* Fifth field: process Q *)
let field_sep_Q =
  "\n___ Process Q\n"

  (* process computing *)


let no_replication_sat_equiv const_p_or_q i j p =
  let rec replace_channel c0 = function
    | [] -> c0
    | (ch0,ch1)::l -> if ch0 = c0 then ch1 else replace_channel c0 l
  in
  (* This function removes nonces that are not replicated,
   * and channel creation for SAT-Equiv.
   * In particular, it only allows simple process.
   * It also removes all replication symbol *)
  let rec aux on_channel i j res chans rep = function
  | New(d,p) -> 
        let new_d = replication_of_data i d in
        aux on_channel (i+1) j (new_d::res) chans ((d,new_d)::rep) p
  | Create_Channel(ch1,ch2,p) -> 
      begin
      match on_channel with
      | Some(c) -> 
          if c = replace_channel ch1 chans 
          then aux on_channel i j res ((ch2,c)::chans) rep p
          else
            let message =
              String.concat "" [toolname;" only supports simple processes.\n There is some channel replication on channel ";channel_to_string ch1; " which is impossible to interprete as a simple process.\n"]
            in raise (SAT_Equiv_error message)
      | None -> 
          assert(chans = []);
          let ch3 = change_channel ch2 j in
          aux (Some ch3) (i+1) (j+1) res [ch2,ch3] rep p
      end
  | Phase(ph,p) -> 
      let q,r,i1,j1,oc = aux on_channel i j res chans rep p in
      Phase(ph,q),r,i1,j1,oc
  | In(ch,bound,t,p) -> 
      begin
      match on_channel with
      | Some(c) ->
          if c = replace_channel ch chans
          then 
            let q,r,i1,j1,oc = aux (Some c) i j res chans rep p in
            let t1 = replication_data_term rep [] t in
            In(c,bound,t1,q),r,i1,j1,oc
          else
            let message =
              String.concat "" [toolname;" only supports simple processes.\n There is some input on channel ";channel_to_string ch; " which is not simple.\n"]
            in raise (SAT_Equiv_error message)
      | None -> assert(chans = []);
          let q,r,i1,j1,oc = aux (Some ch) i j res [] rep p in
          let t1 = replication_data_term rep [] t in
          In(ch,bound,t1,q),r,i1,j1,oc
      end

  | Out(ch,t,p) -> 
      begin
      match on_channel with
      | Some(c) ->
          if c = replace_channel ch chans
          then 
            let q,r,i1,j1,oc = aux (Some c) i j res chans rep p in
            let t1 = replication_data_term rep [] t in
            Out(c,t1,q),r,i1,j1,oc
          else
            let message =
              String.concat "" [toolname;" only supports simple processes.\n There is some output on channel ";channel_to_string ch; " which is not simple.\n"]
            in raise (SAT_Equiv_error message)
      | None -> assert(chans = []);
          let q,r,i1,j1,oc = aux (Some ch) i j res [] rep p in
          let t1 = replication_data_term rep [] t in
          Out(ch,t1,q),r,i1,j1,oc
      end
  | Bang(p) -> 
      begin
        match on_channel with
        | Some(c) ->
            let message = String.concat ""
            [toolname;" only supports simple processes.\n There is an in-depth replication on channel ";channel_to_string c;". It is not possible in a simple process.\n"]
            in raise (SAT_Equiv_error message)
        | None -> assert(chans = []); 
        aux None i j res [] rep p 
      end
  (* we remove the replication symbol *)
  | Parallel pl -> assert(false) 
  (* in the reasulting scenario, every process is linear *)
  | Match(v,case_list) ->
      let message = 
        String.concat "" ["\n";toolname;" does not support pattern matching.\n"]
      in raise (SAT_Equiv_error message)
  | Nul -> Nul,res,i,j,on_channel
  | Reach ->
      begin match const_p_or_q with
      | None -> assert(false) (* It should have been checked before that there is no Reach for the equivalence case *)
      | Some(constPQ) ->
        begin
          match on_channel with
          | None ->
              let message = 
                String.concat "" ["There is an immediately-reachable reach instruction.\nNo need for a ";toolname;" encoding: Reach is reachable.\n"]
              in raise (SAT_Equiv_error message)
          | Some(c) -> Out(c,constPQ,Nul),res,i,j,on_channel
        end
      end
      
  in aux None i j [] [] [] p


let string_linear_process int_chan p =
  let rec aux res = function
  | New(_,_) | Create_Channel(_,_,_)|Bang(_)
  | Parallel(_)|Match(_,_) |Reach-> 
      assert(false) (* should have been removed before *)
  | Phase(ph,p) -> 
      let ph_str = string_of_int ph in
      let ph_str = String.concat "" [ph_str;":"] in
      aux (ph_str::res) p
  | In(c,bound,t,p) ->
      let t_str = typed_term_to_string_alt bound t in
      let in_str = String.concat "" ["in(";int_chan;",";t_str;")."] in
      aux (in_str::res) p
  | Out(c,t,p) ->
      let t_str = untyped_term_to_string (untype t) in
      let out_str = 
        String.concat "" ["out(";int_chan;",";t_str;")."] in
      aux (out_str::res) p
  | Nul -> "0"::res
  in let r = aux [] p
  in String.concat "" (List.rev r)

let compute_process_list constP constQ pl = 
  
  let insert_in_list l a =
    if List.mem a l then l else a::l

  in let merge_lists l1 l2 =
    List.fold_left insert_in_list l1 l2

  in
  let pl_no_rep,names = 
    let rec applied_no_rep res i = function
      | [] -> List.rev res
      | x::t -> let a,b,j = no_replication i x in applied_no_rep ((a,b)::res) j t
    in List.split (applied_no_rep [] 0 pl)
  in
  let priv = List.fold_left merge_lists [] names in

  let to_fold constPQ (res,chan_list,priv,i,j) p =
    let p1,priv1,i1,j1,oc1 = no_replication_sat_equiv (Some constPQ) i j p 
    in match oc1 with
    | None -> (res,chan_list,priv,i,j) (* no input/output: useless *)
    | Some(c) -> 
        let new_priv = List.rev_append priv1 priv in
        (p1::res,c::chan_list,new_priv,i1,j1)
  in
  let res_p,c_p,priv_p,_,_ = 
    List.fold_left (to_fold constP) ([],[],[],0,0) pl_no_rep
  in
  let res_q,c_q,priv_q,_,_ = 
    List.fold_left (to_fold constQ) ([],[],[],0,0) pl_no_rep

  in let all_priv = 
    let l1 =  List.rev_append priv priv_p 
    in merge_lists priv_q l1

  in let rec compare = function
    | [],[] -> true
    | c1::l1,c2::l2 -> (c1 = c2)&&(compare (l1,l2))
    | _,_ -> false

  in let rec all_diff = function
    | [] -> ()
    | a::l ->
        if List.mem a l 
        then
          let message = String.concat ""
          [toolname;" only supports simple processes.\n";
    "Channel "; channel_to_string a;" occurs in several parallel process.\n"; "It is forbidden.\n"]
          in raise (SAT_Equiv_error message)
        else all_diff l

  in let _ =
    assert(compare (c_p,c_q));
    all_diff c_p

  in all_priv,res_p,res_q

let sat_equiv_string const pl =
  let priv,pl1,ql1 = compute_process_list constP constQ pl in
  let public = string_public const [constP;constQ] in
  let priv_str = 
    let priv0 = Util.map (fun t -> to_typed_term (to_data t)) priv in
    string_private priv0
  in

  let to_fold (i,res) p = 
    let i_str = string_of_int i in
    let str = Util.stripchars (string_linear_process i_str p) to_remove
    in (i+1,str::res)

  in let pl_str = 
    let _,str = List.fold_left to_fold (0,[]) pl1 in
    String.concat "\n" str
  in let ql_str = 
    let _,str = List.fold_left to_fold (0,[]) ql1 in
    String.concat "\n" str
  in
  String.concat "" [public;priv_str;field_sep_P;pl_str;field_sep_Q;ql_str;"\n____ Comments\n\n"] 


let sat_equiv_encodings strat const pl_list =
  let rec remove_options res = function
    | [] -> List.rev res
    | (None::_) -> 
        raise (SAT_Equiv_error (Util.infinite_message toolname))
    | (Some(pl,opt)::l) ->
        begin
          match strat with
          | Small   -> remove_options ((Util.map (fun (a,_) -> a) pl)::res) l
          | Overapp -> remove_options ((Util.map (fun (a,_) -> a) [opt])::res) l
          | All     -> remove_options ((Util.map (fun (a,_) -> a) (opt::pl))::res) l
        end
  in let pll = remove_options [] pl_list in
  Util.map (Util.map (sat_equiv_string const)) pll

let sat_equiv_equivalence const process_lists =
    
  let insert_in_list l a =
    if List.mem a l then l else a::l

  in let merge_lists l1 l2 =
    List.fold_left insert_in_list l1 l2

  in let compute_process_list_simple i_init pl =
    let (pl_no_rep,names_P),i0 = 
      let rec applied_no_rep res i = function
        | [] -> List.rev res,i
        | x::t -> let a,b,j = no_replication i x in applied_no_rep ((a,b)::res) j t
      in let to_split,i = applied_no_rep [] i_init pl in List.split to_split, i
      
    in let priv0 = List.fold_left merge_lists [] names_P in

    let to_fold (res,chan_list,priv,i,j) p =
      let p1,priv1,i1,j1,oc1 = no_replication_sat_equiv None i j p 
      in match oc1 with
      | None -> (res,chan_list,priv,i,j) (* no input/output: useless *)
      | Some(c) -> 
          let new_priv = List.rev_append priv1 priv in
          (p1::res,c::chan_list,new_priv,i1,j1)
    in let res_p,c_p,priv_p,i_f,_ = 
      List.fold_left to_fold ([],[],[],i0,0) pl_no_rep
    in (merge_lists priv0 priv_p),res_p,c_p,i_f

  in
  let satequiv_string_equiv const pl namesQ ql =

    let rec compare = function
      | [],[] -> true
      | c1::l1,c2::l2 -> (c1 = c2)&&(compare (l1,l2))
      | _,_ -> false

    in let rec all_diff = function
      | [] -> ()
      | a::l ->
        if List.mem a l 
        then
          let message = String.concat ""
          [toolname;" only supports simple processes.\n";
    "Channel "; channel_to_string a;" occurs in several parallel process.\n"; "It is forbidden.\n"]
          in raise (SAT_Equiv_error message)
        else all_diff l

    in
    let privP,pl1,c_p,i_f =  compute_process_list_simple 0   pl in
    let privQ,ql1,c_q, _  =  compute_process_list_simple i_f ql in
    let _ =
      if compare (c_p,c_q)
      then all_diff c_p
      else 
        let _ =
          print_newline ();
          List.iter (fun x -> print_string (channel_to_string x)) c_p ;
          print_newline ();
          List.iter (fun x -> print_string (channel_to_string x)) c_q ;
          print_newline ();
        in assert(false)
    in
    let all_priv = merge_lists (merge_lists privP namesQ) privQ in
    let public = string_public const [] in
    let priv_str =
      let priv0 = Util.map (fun t -> to_typed_term (to_data t)) all_priv 
      in string_private priv0
    in let to_fold (i,res) p = 
      let i_str = string_of_int i in
      let str = Util.stripchars (string_linear_process i_str p) to_remove
      in (i+1,str::res)
    in let pl_str = 
      let _,str = List.fold_left to_fold (0,[]) pl1 in
      String.concat "\n" str
    in let ql_str = 
      let _,str = List.fold_left to_fold (0,[]) ql1 in
      String.concat "\n" str
    in
    String.concat "" [public;priv_str;field_sep_P;pl_str;field_sep_Q;ql_str;"\n____ Comments\n\n"]
  in let (pl,(namesQ,ql),_) = process_lists
  in satequiv_string_equiv const pl namesQ ql 


