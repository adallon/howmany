open Terms
open Exceptions

type channel = Ch of string * int

let channel_to_string = function
  | Ch(str,i) -> String.concat "_" [str;string_of_int i]

let channel_to_string_simple = function
  | Ch(str,_) -> str

let change_channel ch i =
 match ch with Ch(ch,_) -> Ch(ch,i) 

type ('a,'b) generic_process =
  | New of 'a * ('a,'b) generic_process
  | Create_Channel of channel * channel * ('a,'b) generic_process
  | Phase of int * ('a,'b) generic_process
  (* The list of bound variables might be useful in inputs *)
  | In  of channel * ('a list) * 'b * ('a,'b) generic_process
  | Out of channel * 'b * ('a,'b) generic_process
  | Bang of ('a,'b) generic_process
  | Parallel of ('a,'b) generic_process list
  (* The list of bound variables might be useful in patterns *)
  | Match of 
  'a * (('a list * 'b * ('a,'b) generic_process) list)
  | Nul
  | Reach

(* typed process *)
type process = (typed_data , typed_term) generic_process


(* 
 * a_to_str : typed_data_to_string 
 * b_to_str : typed_term_to_string_alt
 * out_to_str t: untyped_term_to_string (untype t)
 *)
let generic_process_to_string a_to_str b_to_str out_to_str p =
  let rec process_to_string = function
  | New (d,p) ->
      let str = process_to_string p in
      let d_str = a_to_str d in
      String.concat "" 
      ["new ";d_str ; " ;" ; str ]
  | Create_Channel (ch1,ch2,p) ->
      let str = process_to_string p in
      let ch_str1 = channel_to_string ch1 in
      let ch_str2 = channel_to_string ch2 in
      String.concat "" 
      ["new_ch "; ch_str2 ;
      " ; out(" ; ch_str1 ; "," ; ch_str2 ;") ;"; str ]
  | Phase (ph,p) ->
      let str = process_to_string p in
      String.concat "" ["phase ";string_of_int ph; " ;" ; str]
  | In(ch,bound,t,p) ->
      let str = process_to_string p in
      let ch_str = channel_to_string ch in
      let t_str = b_to_str bound t in
  String.concat "" ["in(";ch_str ; "," ; t_str ; ") ;";str]
  | Out(ch,t,p) ->
      let str = process_to_string p in
      let ch_str = channel_to_string ch in
      let t_str = out_to_str t in
      String.concat "" ["out(";ch_str ; ","; t_str ; ") ;";str]
  | Bang(p) ->
      let str = process_to_string p in
      String.concat "" ["!";str]
  | Parallel pl ->
      begin
      match pl with
      | [] -> assert(false)
      | l  ->
          let pl_str = Util.map process_to_string l in
          let pl_merged = String.concat " | " pl_str in
          String.concat "" ["(";pl_merged;")"]
      end
  | Match (v,case_list) ->
      let str = a_to_str v in
      let one_case (bound,pattern,p) =
        let pattern_str = b_to_str bound pattern
        in let p_str = process_to_string p in
        String.concat " -> " [pattern_str ; p_str]
      in 
      begin
        match case_list with
      | [] -> assert(false)
      | l  -> 
          let cases_str = Util.map one_case l in
          let cases_merged = String.concat " | " cases_str in
          let all_cases =
            String.concat "" [ " begin ";cases_merged;" end "]
          in String.concat "" ["match " ; str ;" with "; all_cases ]
      end
  | Nul   -> "0."
  | Reach -> "Reach"
  in process_to_string p

let process_to_string =
  let out t = untyped_term_to_string (untype t) in
  let a = typed_data_to_string in
  let b = typed_term_to_string_alt in
  generic_process_to_string a b out

let next = function
  | In(ch,bound,t,p) -> p
  | Out(ch,t,p)      -> p
  | Nul -> Nul
  | Reach -> Reach
  | _ -> assert(false) 

let unfold p =
  (* Computes the unfolding of a process *)
  let rec search_element c = function
    | [] -> c
    | (c1,c2)::t -> if c = c1 then c2 else search_element c t
  in let search_channel (c:channel) = search_element c
  in let replication_of_channel c index =
    match c with
    | Ch(name,i) -> assert(i=0);Ch(name,index)

  in let rec aux chans names vars index = function
  | New(n,p) ->  
      let new_n = replication_of_data index n in
      aux chans ((n,new_n)::names) vars index p 
  |Create_Channel(c1,c2,p) ->
  (*  let new_c1 = search_channel c1 chans in  *)
      let new_c2 = replication_of_channel c2 index in
      aux ((c2,new_c2)::chans) names vars index p
     (* Create_Channel(new_c1,new_c2,p1) *)
  |Phase(ph,p) -> 
      let p1,ind = aux chans names vars index p in
      Phase(ph,p1),ind
  |In(c,bound,t,p) ->
      let new_c = search_channel c chans in
      let new_bound = 
        Util.map (fun x -> (replication_of_data index x)) bound 
      in let bound_to_new_bound =
        Util.map (fun x -> (x,replication_of_data index x)) bound
      in let vars2 = 
        List.rev_append (List.rev bound_to_new_bound) vars
      in
      let t1 = replication_data_term names vars2 t in
      let p1,ind = aux chans names vars2 index p in
      In(new_c,new_bound,t1,p1),ind
  |Out(c,t,p) ->
      let new_c = search_channel c chans in
      let t1 = replication_data_term names vars t in
      let p1,ind = aux chans names vars index p in
      Out(new_c,t1,p1),ind
  |Bang(p) ->
      let p1,ind = aux chans names vars (index+1) p in
      let p2,ind2 = aux chans names vars (ind+1) p in
      Parallel([p1;p2]),ind2
  |Parallel(pl) ->
      let pl1,ind = aux_par chans names vars index pl in
      Parallel(pl1),ind
  |Match(var,cases) -> 
      let var1 = search_element var vars in
      let cases2,ind = aux_cases chans names vars index cases in
      Match(var1,cases2),ind
  | Nul -> Nul,index
  | Reach -> Reach,index

  and aux_par chans names vars index = function 
    | [] -> [],index
    | p::t ->
        let p1,ind1 = aux chans names vars (index+1) p in
        let t1,ind2 = aux_par chans names vars (ind1+1) t in
        p1::t1,ind2

  and aux_cases chans names vars index cases =
    let on_one_case ind (bound,pattern,p) =
      let new_bound = 
        Util.map (fun x -> (replication_of_data ind x)) bound 
      in let bound_to_new_bound =
        Util.map (fun x -> (x,replication_of_data ind x)) bound
      in let vars2 = 
        List.rev_append (List.rev bound_to_new_bound) vars
      in
      let pattern1 = replication_data_term names vars2 pattern in
      let p1,ind1 = aux chans names vars2 ind p in
      (new_bound,pattern1,p1),ind1 
    in let rec on_cases ind = function
      | [] -> [],ind
      | case::l -> 
          let case1,ind1 = on_one_case (ind+1) case in
          let cases1,ind2 = on_cases (ind1+1) l in
          case1::cases1,ind2 
    in on_cases index cases
  
  in let p1,_ =  aux [] [] [] 0 p in p1


let get_terms_for_type_compliance p =
  let rec aux (res,resMatch) = function
  | New(_,p) -> aux (res,resMatch) p
  | Create_Channel(_,_,p) -> aux (res,resMatch) p
  | Phase(_,p) -> aux (res,resMatch) p
  | In(_,_,t,p) -> aux ((t::res),resMatch) p
  | Out(_,t,p) -> aux ((t::res),resMatch) p
  | Bang(p) -> aux (res,resMatch) p
  | Parallel(pl) -> List.fold_left aux (res,resMatch) pl
  | Match(var,cases) -> 
      let couples =
        Util.map (fun (_,pattern,p) -> (var,pattern)) cases
      in let processes =
        Util.map (fun (_,pattern,p) -> p) cases
      in let new_resMatch = 
        List.rev_append (List.rev couples) resMatch
      in List.fold_left aux (res,new_resMatch) processes
  (* I do not yet know what to do in this case *)
  | Nul -> res,resMatch
  | Reach -> res,resMatch
  in aux ([],[]) p

let get_terms p =
  let rec aux res = function
  | New(_,p) -> aux res p
  | Create_Channel(_,_,p) -> aux res p
  | Phase(_,p) -> aux res p
  | In(_,_,t,p) -> aux (t::res) p
  | Out(_,t,p) -> aux (t::res) p
  | Bang(p) -> aux res p
  | Parallel(pl) -> List.fold_left aux res pl
  | Match(var,cases) -> 
      let patterns =
        Util.map (fun (_,pattern,_) -> pattern) cases
      in let processes =
        Util.map (fun (_,_,p) -> p) cases
      in let new_res = 
        List.rev_append (List.rev patterns) res
      in List.fold_left aux new_res processes
  (* I do not yet know what to do in this case *)
  | Nul -> res
  | Reach -> res
  in aux [] p

let counting_instructions p =
  let rec aux res chan = function
  |New(n,p) -> aux res chan p
  |Create_Channel(ch1,_,p) -> 
      begin match chan with
      | Some(_) -> aux res chan p
      | None    -> aux res (Some ch1) p
      end
  |Phase(n,p)  -> aux res chan p
  |In(c,_,_,p) -> 
      begin match chan with
      | Some(_) -> aux (1+res) chan p
      | None    -> aux (1+res) (Some c) p
      end
  |Out(c,_,p)  -> 
      begin match chan with
      | Some(_) -> aux (1+res) chan p
      | None    -> aux (1+res) (Some c) p
      end
  | Bang(p) -> aux res chan p
  | Parallel(_) -> (failwith "Processes must be simple")
  | Match (_) -> (failwith "Does not work with match")
  | Nul -> chan,res
  | Reach -> chan,1+res
  in aux 0 None p



let get_process_subtypes p =
  let rec all_subtypes res t =
    let args = get_subtypes t in
    Array.fold_left all_subtypes (t::res) args
  in
  let terms = get_terms p in
  let types = Util.map get_type terms in
  let all_subtype_list = List.fold_left all_subtypes [] types 
  in let rec simplify res = function
    | [] -> res
    | a::t -> 
        if List.mem a res 
        then simplify res t 
        else simplify (a::res) t
  in simplify [] all_subtype_list

let check_compliance p =
  let unfolded = unfold p in
  let (terms,match_terms) = get_terms_for_type_compliance unfolded in
  assert(check_compliance_terms terms match_terms)

let no_replication i p =
  (* This function removes nonces that are not replicated.
   * It also removes all replication symbol *)
  let rec aux i inside res rep = function
  | New(d,p) -> 
      if inside 
      then 
        let q,r,j = aux i true res rep p 
        in New(d,q),r,j
      else
        let new_d = replication_of_data i d in
        aux i false (new_d::res) ((d,new_d)::rep) p
  | Create_Channel(ch1,ch2,p) -> 
      let q,r,j = aux i inside res rep p
      in Create_Channel(ch1,ch2,q),r,j
  | Phase(ph,p) -> 
      let q,r,j = aux i inside res rep p in
      Phase(ph,q),r,j
  | In(ch,bound,t,p) -> 
      let q,r,j = aux i inside res rep p in
      let t1 = replication_data_term rep [] t in
      In(ch,bound,t1,q),r,j
  | Out(ch,t,p) -> 
      let q,r,j = aux i inside res rep p in
      let t1 = replication_data_term rep [] t in
      Out(ch,t1,q),r,j
  | Bang(p) -> aux i true res rep p 
  (* we remove the replication symbol *)
  | Parallel pl -> assert(false) 
  (* in the reasulting scenario, every process is linear *)
  | Match(v,case_list) ->
      let on_one_case res i (bound,t,p) =
        let (t1:typed_term) = replication_data_term rep [] t in
        let q,r,j = aux i inside res rep p
        in (bound,t1,q),r,j

      in let rec cases res_cases res i = function
        | [] -> (List.rev res_cases,res,i)
        | c::l -> 
            let c0,r0,j0 = on_one_case res i c in
            cases (c0::res_cases) r0 (j0+1) l
      
      in let (new_case_list,r,j) = cases [] res i case_list
      in (Match(v,new_case_list),r,j)

  | Nul -> Nul,res,i
  | Reach -> Reach,res,i
  in let (q,r,j) =  aux i false [] [] p in (q,r,j)

let well_shaped_process shapes p =
  let rec aux res p0 = 
  if res then
    begin
    match p0 with
  | New(d,p) -> aux true p
  | Create_Channel(ch1,ch2,p) -> aux true p
  | Phase(ph,p) -> aux true p
  | In(ch,bound,t,p) -> (is_well_shaped shapes t) && (aux true p)
  | Out(ch,t,p)      -> (is_well_shaped shapes t) && (aux true p)
  | Bang(p) -> aux true p
  | Parallel pl -> List.fold_left aux true pl 
  | Match(v,case_list) ->
      let one_case res (bound,t,p) =
        if res 
        then (is_well_shaped shapes t) && (aux true p)
        else false
      in List.fold_left one_case true case_list
  | Nul -> true
  | Reach -> true
    end
  else false
  in aux true p

type scenarios = ((process list * int)  list * (process list * int)) option
