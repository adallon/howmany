val reachability : int -> Terms.reduction list ->  Process.process -> Process.scenarios list

type request = DEP | DEPPLUS | SOUT | SPLUS

val request : int -> request -> Terms.reduction list -> (Process.process * Terms.type_term) -> Process.scenarios

val sout_all : int -> Terms.reduction list -> Process.process -> 
  string * Process.scenarios

val dep_all_point : int -> Terms.reduction list -> Process.process -> 
  string * Process.scenarios

val semi_equivalence: int -> (Terms.reduction list * Terms.reduction list ) -> Process.process -> Process.scenarios
