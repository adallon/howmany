open Terms
open Process
open Label
open Dep_graph
open Exceptions

let print_level = Util.print_level
let print_v_level = Util.print_v_level
let print_v_string = Util.print_v_string
let print_v_newline = Util.print_v_newline

let set_empty verbose gph =

  let print_verb_level = print_v_level 1 verbose in
  let print_verb_string = print_v_string 1 verbose in
  let print_verb_newline = print_v_newline 1 verbose in
  let _ = 
    print_verb_newline ();
    print_verb_level 0;
    print_verb_string "set_empty";
    print_verb_newline ()
  in

  let initialize_types level gph tau =
    let _ =
      print_verb_level level;
      print_verb_string  "initialize_type with type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ()
    in

    if is_dishonest verbose gph (tau.type_tau)
    then 
      let _ =
        print_verb_level level;
        print_verb_string  "This type is dishonest. ";
        print_verb_newline ();
        print_verb_level level;
        print_verb_string  "It cannot be empty. ";
        print_verb_newline ();
        tau.type_is_empty <- Some(false);
        tau.dep_init <- Some(singleton_empty);
      in gph
    else 
      let _ =
        print_verb_level level;
        print_verb_string  "This type is honest. ";
        print_verb_newline ();
        print_verb_level level;
        print_verb_string  "It could be empty. ";
        print_verb_newline ();
        tau.dep_init <- Some(empty);
      in gph

  in 
  let rec is_empty_type level gph tau = 
    let _ =
      print_verb_level level;
      print_verb_string  "is_empty_type with type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ()
    in
    match tau.type_is_empty with
    | Some(b) ->     
      let _ =
        print_verb_level level;
        print_verb_string  "A result has been set.";
        print_verb_newline ();
        print_verb_level level;
        print_verb_string  "We return this result.";
        print_verb_newline ()
      in b
    | None ->
        let _ =
          print_verb_level level;
          print_verb_string  "No result has been set.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string  "We compute the result.";
          print_verb_newline ()
        in
        let gph1 = see_type gph tau in 
        let empty_subtypes =
          let _ =
            print_verb_level level;
            print_verb_string  "computing subtypes";
            print_verb_newline ()
          in
          let b =
            (Array.length tau.type_subtypes = 0)||(Array.exists (is_empty_type (level+1) gph1) tau.type_subtypes)
          in if b
          then
            let _ =
              print_verb_level level;
              print_verb_string  "Subtypes have empty result.";
              print_verb_newline ()
            in true
          else
            let _ =
              print_verb_level level;
              print_verb_string  "Subtypes have non empty result.";
              print_verb_newline ()
            in false


        in let empty_rho =
          let _ =
            print_verb_level level;
            print_verb_string  "computing rho";
            print_verb_newline ()
          in
          match tau.tl_rho_list with
          | None -> assert(false) (* should be defined at this point *)
          | Some(rho) ->
              let f (alpha,tau_l) =
                (is_empty_label (level+1) gph1 alpha 
          || List.exists (is_empty_type (level+1) gph1) tau_l)
              in
              let b = List.for_all f rho
              in
              let _ =
                assert(rho != [] || b)
              in
              let _ =
                if b
                then 
                  let _ =
                    print_verb_level level;
                    print_verb_string  "Return true.";
                    print_verb_newline ()
                  in None
                else 
                  let _ =
                    print_verb_level level;
                    print_verb_string  "Return false.";
                    print_verb_newline ()
                  in None
              in b
        in 
        if (empty_subtypes && empty_rho)
        then true
        else
          let _ =
            print_verb_level level;
            print_verb_string  "The result is not empty. ";
            print_verb_newline ();
            print_verb_level level;
            print_verb_string  "We set it as non-empty. ";
            print_verb_newline ();
            tau.type_is_empty <- Some(false);
          in false

  and has_empty_pred level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "has_empty_pred with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match get_pred alpha with
    | None -> false
    | Some(beta) -> is_empty_label (level+1) gph beta
  
  and has_empty_type level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "has_empty_type with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match alpha.lab_type with
    | None -> false
    | Some(tau) -> is_empty_type (level+1) gph tau

  and is_empty_label level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "is_empty_label with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match alpha.lab_is_empty with
    | Some(b) -> b
    | None ->
        let gph1 = see_label gph alpha
        in 
        if   ((has_empty_pred (level+1) gph1 alpha) 
          ||  (has_empty_type (level+1) gph1 alpha))
        then true
        else
          let _ =
            alpha.lab_is_empty <- Some(false)
          in false

  in let set_empty_type level gph tau =
    let _ =
      print_verb_level level;
      print_verb_string  "set_empty_type with type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ()
    in
    let b = is_empty_type (level+1) gph tau
    in
    let _ =
      if b then
        begin
        print_verb_level level;
        print_verb_string  "set as empty ";
        print_verb_newline ()
        end
      else
        begin
        print_verb_level level;
        print_verb_string  "set as not empty ";
        print_verb_newline ()
        end
    in let _ =
      tau.type_is_empty <- Some(b)
    in gph
  
  in let set_empty_label level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "set_empty_label with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    let b = is_empty_label (level+1) gph alpha
    in let _ =
      alpha.lab_is_empty <- Some(b)
    in gph


  in let gph = 
    List.fold_left (initialize_types 0) gph gph.list_types
  in let gph =
    List.fold_left (set_empty_type 0) gph gph.list_types
  in let gph =
    List.fold_left (set_empty_label 0) gph gph.list_labels
  in gph


let dependencies verbose gph =

  let print_verb_level = print_v_level 1 verbose in
  let print_verb_string = print_v_string 1 verbose in
  let print_verb_newline = print_v_newline 1 verbose in
 
  let rec dep_pred level gph alpha =
    let _ =
      print_verb_level level;
      print_verb_string  "dep_pred with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match get_pred alpha with
    | None -> singleton_empty
    | Some(beta) -> dep_label (level+1) (see_label gph alpha) beta

  and dep_lab_type level gph alpha =
    (* a generic function to compute the dependencies
     * of the type of a label if necessary.
     * If the label is an input, it computes the dependencies
     * of the type of the input term;
     * If the label is an output, it returns singleton_empty 
     *)
    let _ =
      print_verb_level level;
      print_verb_string  "dep_lab_type with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in
    match alpha.lab_type with
    | None -> singleton_empty
    | Some(tau) -> dep_type (level+1) (see_label gph alpha) tau

  and dep_label level gph alpha =
    (* Finds the function to apply *)

    let _ =
      print_verb_level level;
      print_verb_string  "dep_label with label ";
      print_verb_string (label_to_string alpha);
      print_verb_newline ()
    in

    match alpha.lab_is_empty,alpha.lab_dep with
    | None,_ -> assert(false) (* should be defined at this point *)
    | Some(true),Some(r) ->
        let _ =
          assert(r = empty);
          print_verb_level level;
          print_verb_string "This has been precomputed as empty.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string "We return it directly.";
          print_verb_newline ()
        in empty
    | Some(true),None ->
        let _ =
          print_verb_level level;
          print_verb_string "This has been precomputed as empty.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string "We return it directly.";
          print_verb_newline ();
          alpha.lab_dep <- Some(empty)
        in empty
    | Some(false),Some(r) -> 
        let _ =
          assert(r != empty);
          print_verb_level level;
          print_verb_string "Some non-empty result has been reached before.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string "We return it directly.";
          print_verb_newline ()
        in r
    | Some(false),None ->
        let _ =
          print_verb_level level;
          print_verb_string "No result has been reached before.";
          print_verb_newline ();
        in

        if List.mem alpha gph.labels_seen 
        then 
          let _ = 
            print_verb_level level;
            print_verb_string "Loop: return bottom.";
            print_verb_newline ()
          in bottom (* We do not set the result *)
        else 
          let r_pred = dep_pred (level+1) gph alpha in
          let r_type = dep_lab_type (level+1) gph alpha in
          let r = set_product [singleton_label alpha ; r_pred ; r_type ]
          in if r = bottom
          then bottom
          else
            let _ =
              alpha.lab_dep <- Some(r)
            in r

  and dep_rho level gph rho_op =
    (* Auxiliary function for dep_type *)
    let _ =
      print_verb_level level;
      print_verb_string  "dep_rho ";
      print_verb_newline ()
    in
    let dep_rho_element level gph res (alpha,tau_l) =
      let _ =
        print_verb_level level;
        print_verb_string  "dep_rho_element ";
        print_verb_newline ()
      in
      let alpha_dep = dep_label (level+1) gph alpha in
      let tau_l_dep =
        Util.map (dep_type (level+1) gph) tau_l 
      in (set_product2 alpha_dep (set_product tau_l_dep))::res

    in let r_list =
      match rho_op with
      | None -> assert(false) (* Should be defined at this point *)
      | Some(rho) ->
          List.fold_left (dep_rho_element (level+1) gph) [] rho
    in set_union r_list

  and dep_s_out  level gph tau = dep_rho level gph tau.tl_rho_list
  and dep_s_plus level gph tau = 
    
    let _ = 
      print_verb_newline ();
      print_verb_level level;
      print_verb_string "dep_s_plus on type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ();
    in
    let rho_plus tau = 
      let rho_list = match tau.tl_rho_list with
      | None -> assert(false) (* should not happen at this point *)
      | Some(r) ->
          if r == []
          then
            let _ =
              print_verb_level (level+1);
              print_verb_string "rho_plus on empty rho";
              print_verb_newline ();
            in []
          else r
      in let rho_suppl = match tau.rho_plus with
      | None -> assert(false)
      | Some(r) -> 
          if r == []
          then
            let _ =
              print_verb_level (level+1);
              print_verb_string "rho_plus on empty rho_plus";
              print_verb_newline ();
            in []
          else  
            let _ =
              print_verb_level (level+1);
              print_verb_string "rho_plus on non-empty rho_plus";
              print_verb_newline ();
            in Util.map (fun alpha -> alpha,[]) r
      in Some(List.rev_append rho_suppl rho_list)
    in
    dep_rho (level+1) gph (rho_plus tau)
 
  and dep_type level gph tau = dep_type_generic level gph false tau
  and dep_type_generic level gph with_plus tau =
    let _ =
      print_verb_level level;
      print_verb_string  "dep_type with type ";
      print_verb_string (label_type_to_string tau);
      print_verb_newline ()
    in
    (* The dependency function on types: cases  *)
    match tau.type_is_empty,tau.type_dep with
    | None,_ -> 
        begin
        print_string (label_type_to_string tau);
        assert(false) 
        (* should be defined at this point *)
        end
      (* A result has already been reached *)
    | Some(true),Some(r) ->
        let _ =
          assert(r = empty);
          print_verb_level level;
          print_verb_string "This has been precomputed as empty.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string "We return it directly.";
          print_verb_newline ()
        in empty
    | Some(true),None ->
        let _ =
          print_verb_level level;
          print_verb_string "This has been precomputed as empty.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string "We return it directly.";
          print_verb_newline ();
          tau.type_dep <- Some(empty)
        in empty

    | Some(false),Some(r) ->    
        let _ =
          print_verb_level level;
          print_verb_string "This has been precomputed, but not as empty.";
          print_verb_newline ();
          print_verb_level level;
          print_verb_string "We return the previous result directly.";
          print_verb_newline ();
        in r 
    | Some(false),None ->
      let _ =
        print_verb_level level;
        print_verb_string "This has not been precomputed, and is not empty.";
        print_verb_newline ();
        print_verb_level level;
        print_verb_string "We compute the result.";
        print_verb_newline ();
      in 
      (* No result was reached yet *)
      if List.mem tau gph.types_seen 
      then 
        let _ = 
          print_verb_level level;
          print_verb_string "Loop: return bottom.";
          print_verb_newline ()
        in
        bottom
        (* The node has been explored: we get a cycle. *)
      else 
        (* The node has not been explored *)
        let dep_init =
          match tau.dep_init with
          | None -> assert(false) (* should be defined at this point *)
          | Some(di) -> di
        in let gph1 = see_type gph tau
        in let res_subtypes =
          Array.map (dep_type (level+1) gph1) (tau.type_subtypes)
        in let dep_subtypes =
          if Array.length res_subtypes = 0
          then empty
          else Array.fold_left set_product2 singleton_empty res_subtypes
        in 
        let r = 
          if is_tuple tau.type_tau
          then
            let _ =
              assert(tau.tl_rho_list = Some([]));
            in let r0 = set_union [dep_init;dep_subtypes]
            in r0
          else
            let dep_of_rho = 
              if with_plus
              then dep_s_plus (level+1) gph1 tau
              else dep_s_out  (level+1) gph1 tau 
            in
            set_union [dep_init;dep_subtypes;dep_of_rho] 
        in
        if (r = bottom)
        then
          let _ = 
            (* We do not save the result as it can be improved. *)
            print_verb_level level;
            print_verb_string "The result is bottom.";
            print_verb_newline ()
          in bottom
        else
          let _ =
            tau.type_dep <- Some(r);
            print_verb_level level;
            print_verb_string "The result is not bottom.";
            print_verb_newline ()
          in r
  in let dep_plus = dep_type_generic 0 gph true
  in (dep_label  0 gph, dep_type    0 gph,
      dep_s_out 0 gph, dep_s_plus 0 gph,
      dep_plus )


