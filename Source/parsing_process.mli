val new_channel: string -> Process.channel

type parsing_process =
    (Terms.parsing_data, Terms.parsing_term) Process.generic_process

val parsing_process_to_string : parsing_process -> string

val reach : parsing_process
val nul_process : parsing_process
val bang: parsing_process -> parsing_process
val parallel: parsing_process list -> parsing_process
val phase_nul: int -> parsing_process
val create_channel: string -> string -> string -> parsing_process
val new_nonce:  string -> Terms.type_term    -> parsing_process
val new_input:  string -> Terms.parsing_term -> parsing_process
val new_output: string -> Terms.parsing_term -> parsing_process
val new_match : 
  string -> (Terms.parsing_term * parsing_process) list -> parsing_process
val append_to_sequential: 
  parsing_process -> parsing_process -> parsing_process
val replace: 
  string list -> Terms.parsing_term list -> 
    parsing_process -> parsing_process

val verify_channels: (Process.channel list) -> parsing_process -> bool

val parsing_to_process:
  Terms.typed_data list -> parsing_process -> Process.process


