
val pos_diff: Lexing.position -> Lexing.position -> int*int*int
val error_number: int -> Lexing.position -> Lexing.position -> 'a
val syntax_error : Lexing.position -> Lexing.position -> 'a

type parsing_result

val parsing_result_to_string: parsing_result -> string
val init_parsing_result: unit -> parsing_result
val add_channels  : parsing_result -> Process.channel list -> unit
val add_fun_symbol: parsing_result -> Terms.function_symbol -> unit
val get_signature : parsing_result -> Terms.function_symbol list
val add_constants : parsing_result -> Terms.typed_term list -> unit
val add_reduction : parsing_result ->
  string * string Terms.term list * string Terms.term -> unit
val add_process_def : 
  parsing_result -> (string * string list * Parsing_process.parsing_process) -> unit

val call : 
  parsing_result -> 
    (string * Terms.parsing_term list) -> Parsing_process.parsing_process

val add_reachability :
  parsing_result -> Parsing_process.parsing_process -> unit

val add_dep :
  parsing_result -> Parsing_process.parsing_process * Terms.type_term -> unit

val add_sout :
  parsing_result -> Parsing_process.parsing_process * Terms.type_term -> unit

val add_depplus :
  parsing_result -> Parsing_process.parsing_process * Terms.type_term -> unit

val add_splus :
  parsing_result -> Parsing_process.parsing_process * Terms.type_term -> unit

val add_soutall :
  parsing_result -> Parsing_process.parsing_process -> unit

val add_depall :
  parsing_result -> Parsing_process.parsing_process -> unit

val add_semiequiv :
  parsing_result -> Parsing_process.parsing_process -> unit

val add_equivalence :
  parsing_result 
  -> Parsing_process.parsing_process 
  -> Parsing_process.parsing_process 
  -> unit

val next_step: parsing_result -> bool
val stop: parsing_result -> unit

type query_type = 
  (Process.process list                   (* reachability queries *)
  * ( Process.process *  Terms.type_term) list  
  (* dependency   queries *)
  * ( Process.process *  Terms.type_term) list  
  (* dep+         queries *)
  *  (Process.process *  Terms.type_term) list  
  (* sout         queries *)
  *  (Process.process *  Terms.type_term) list  
  (* s+           queries *)
  *  Process.process list                 (* soutall      queries *)
  *  Process.process list                 (* depall       queries *)
  *  Process.process list                 (* semiequiv    queries *)
  *  (Process.process * Process.process) list (* equiv    queries *)
  )

val get_query: parsing_result -> (query_type * Tool_encoder.process_data)

