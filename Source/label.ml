open Process
open Terms


type label =
  { 
    lab_phase:int;
    lab_p:process;
    lab_q:process option;
    type_term: type_term option; (* Reach is untyped *)
    path: int list;
    (* path to follow to find the process from parent process *)
    marked_positions: position list;
    is_replicated:bool;
    mutable lab_dep: result option ;
    mutable lab_pred: label option;
    mutable following: label list;
    mutable pred_set: bool;
    mutable lab_type: type_label option;
    (* inputs depend on some type_label *)
    mutable lab_is_empty: bool option;
    mutable quantity: int;
  }
and type_label =
  { 
    mutable type_phase:int;
    type_tau:type_term;
    type_subtypes:type_label array;
    mutable type_dep: result option;
    mutable tl_rho_list : (label * (type_label list)) list option;
    mutable rho_plus : (label) list option;
    mutable type_is_empty: bool option;
    mutable dep_init : result option;
  }
and result = label list list option

let rec label_to_string lab =
  let p = lab.lab_p in
  let p_str = process_to_string p in p_str
(*  match lab.lab_type_dep_list with
  | None    -> String.concat "" ["Label: ";p_str ; " None !"]
  | Some(r) -> 
      let r_str  = Util.map (Util.map label_type_to_string) r in
      let r_flat = Util.map (String.concat ";") r_str in
      let r0     = String.concat "\n]\n[\n" r_flat in
      String.concat "" ["Label: ";p_str; " Borders: [\n"; r0 ; "\n]" ]
*)

and label_type_to_string tau_lab =
  let tau = tau_lab.type_tau in
  let tau_str = type_to_string tau in
  (*
  let tau_res = tau_lab.type_dep in
  let tau_res_str =
    match tau_res with
    | None -> String.concat "" 
    [ "-[-" ; tau_str ; ": NoResultReached!-]-"]
    | Some(r) -> 
        String.concat ""
        [ "-[-" ; tau_str ; " : " ; result_to_string r ; "-]-" ]
  in tau_res_str
  *)
  tau_str

and result_to_string = function
  | None -> "bottom"
  | Some([]) -> "emptyset"
  | Some([[]]) -> "singleton emptyset"
  | Some(l) ->
      let l_str = Util.map (Util.map label_to_string) l
      in let remove_first_list l =
        let l_str = String.concat " ; " l
        in String.concat "" ["  {";l_str;"}"]
      in let l_str2 = 
        String.concat "" (Util.map remove_first_list l_str)
      in String.concat "" ["["; l_str2  ;"]"]


let create_label (ph,is_rep,p,q_op,tau_op,path,marked) =
  {
    lab_phase = ph;
    lab_p = p;
    lab_q = q_op;
    type_term = tau_op;
    path = List.rev path;
    is_replicated = is_rep; 
    lab_dep = None;
    lab_pred = None;
    following = [];
    pred_set = false;
    lab_type = None;
    marked_positions = marked;
    lab_is_empty = None;
    quantity = 0;
  }

let create_type_label (ph,tau,subtypes) =
  {
    type_phase = ph;
    type_tau = tau;
    type_subtypes = subtypes;
    type_dep = None;
    tl_rho_list = None;
    rho_plus = None;
    type_is_empty = None;
    dep_init = None;
  }

let label_equals lab1 lab2 = (lab1 == lab2)

let compare_label alpha (ph,is_rep,p,tau_op,rho_op) =
  alpha.lab_phase = ph && is_rep = alpha.is_replicated && alpha.lab_p = p && alpha.type_term = tau_op

let is_type_label tau_lab tau =  (tau_lab.type_tau = tau)

let set_pred (alpha:label) (lab:label option) =
  begin
  assert(not alpha.pred_set);
  alpha.pred_set <- true;
  alpha.lab_pred <- lab;
  end

let get_pred alpha =
  assert(alpha.pred_set);
  alpha.lab_pred

let (empty:result) = Some([])
(* neutral element for the union and 0 for the product *)
let (singleton_empty:result) = Some([[]])
(* neutral element for the product *)
let (bottom:result) = None
(* represents infinite sets *)

let clean result =
  (* This function removes singleton_empty if it is not the only element
   * of the list, as we are only interested by maximal sets *)
  let rec remove_empty = function
    | (res,[]) -> List.rev res
    | ([],[[]]) -> [[]]
    | (res,[]::t) -> remove_empty (res,t)
    | (res,a::t) -> remove_empty (a::res,t)
  in match result with
  | None -> None
  | Some(r) -> Some(remove_empty ([],r)) 

let set_product2 (res1:result) (res2:result) =
  match res1,res2 with
  | Some([]),_ | _,Some([]) -> Some([])
  | None,_ | _,None -> None
  | Some(l1),Some(l2) -> Some(Util.product_list2 l1 l2)

let set_product (res_list:result list) =
  if List.mem empty res_list
  then empty
  else
    if List.mem None res_list
    then None
    else List.fold_left set_product2 singleton_empty res_list

let simple_union l1 l2 = List.rev_append l1 l2

let set_union2 (res1:result) (res2:result) =
  match res1,res2 with
  | None,_|_,None -> None
  | Some(l1),Some(l2) -> clean (Some(List.rev_append l1 l2))

let set_union (res_list:result list) =
  if List.mem bottom res_list 
  then bottom
  else List.fold_left set_union2 empty res_list

let singleton_label alpha = Some([[alpha]])
let to_label_list_list_option r = r
