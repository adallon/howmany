open Process

let count = function
  | None -> (failwith "Some scenario is bottom. It cannot be converted to a list. ")
  | Some(sc_l,(sc_opt,n)) ->
    Util.map (fun (a,_) -> Util.map Process.counting_instructions a) sc_l , 
    Util.map Process.counting_instructions sc_opt

let counted_to_list0 sc_list  = 
  let remove_option = function
    | Some ch,n -> let chan = channel_to_string_simple ch in (chan,n)
    | None,_ -> (failwith "There is some process without a channel")
  in Util.map remove_option sc_list

let get_count sc =
  let counted = count sc in
  let rec aux (lg,ld) = (Util.map counted_to_list0 lg, counted_to_list0 ld)
  in aux counted

 
let rec counted_to_list = function
  | [] -> []
  | (ch_op,n)::l ->
      begin match ch_op with
      | Some ch ->
          let chan = String.concat "" ["\"";channel_to_string_simple ch;"\""]
          in (chan,n)::counted_to_list l
      | None -> (failwith "There is some process without a channel")
      end

let get_count_internal sc_list =
  let l0 = Util.map count sc_list in
  let rec aux res = function
    | [] -> List.rev res
    | (lg,ld)::l ->
    aux ((counted_to_list ld :: (Util.map counted_to_list lg))::res) l
  in aux [] l0

let ext = ".json"

let count_in_file filename sc_list =
  let l1   = get_count_internal sc_list
  in let file = String.concat "" [filename; ext]  
  in let oc   = open_out file
  in let write_all l =
    let to_string_list a =
      let element_to_string (ch,n) = String.concat "" ["[";ch;",";string_of_int n;"]"] in
      let str b = 
        let str = String.concat "," (Util.map element_to_string b) 
        in String.concat "" ["[";str;"]"]
      in String.concat "" ["[";String.concat "," (Util.map str a);"]"] 

    in let l_str some_list = 
      let str = String.concat "," ((Util.map to_string_list) some_list)
      in String.concat "" ["["; str ;"]"]
    in let str0 = l_str l 
     (* String.concat "" ["["; String.concat "," (Util.map l_str l); "]\n"] *)
    in let _ =
      output_string oc str0
    in ()
  in let _ =
    write_all l1;
    close_out oc
  in ()

