open Process
open Terms
open Exceptions

let new_channel ch = Ch(ch,0)

type parsing_process = (parsing_data, parsing_term) generic_process

let parsing_process_to_string (p:parsing_process) =
  let out = parsing_term_to_string  in
  let a = parsing_data_to_string in
  let b bound = parsing_term_to_string in
  generic_process_to_string a b out p


let reach = Reach
let nul_process = Nul
let bang p = Bang(p)
let parallel pl = Parallel pl
let phase_nul i = 
  if i < Util.max_phase
  then Phase(i,Nul)
  else 
    let i_str = string_of_int i in
    let message = 
      String.concat "" 
      ["You specified phase ";i_str;" but phases should be less than ";
      string_of_int Util.max_phase]
    in raise (Process_error message) 
let create_channel c1 c2 c3 =
  if c2 = c3 
  then Create_Channel(Ch(c1,0),Ch(c2,0),Nul)
  else 
    let message = 
      String.concat "" ["You can only output a channel you have just created. You wrote new_ch ";c2;" ; out(";c1;",";c3;") with ";c2;"=/=";c3]
    in raise (Process_error message)
let new_nonce n tau =
  let name = string_and_type_to_nonce (n,tau) in
  New (name,Nul)
let new_input c t = In(Ch(c,0),[],t,Nul)
let new_output c t = Out(Ch(c,0),t,Nul)
let new_match str cases =
  let case_list = Util.map (fun (pattern,p) -> ([],pattern,p)) cases 
  in Match (string_to_parsing_data str,case_list) 

let append_to_sequential seq p =
  match seq with
  | New(a,Nul) -> New(a,p)
  | Create_Channel(c1,c2,Nul) -> Create_Channel(c1,c2,p)
  | Phase(n,Nul) -> Phase(n,p)
  | In(c,bound,t,Nul) -> In(c,bound,t,p)
  | Out(c,t,Nul) -> Out(c,t,p)
  | _ -> assert(false) 


let replace_channel merged0 c =
  let rec search ch = function
    | [] -> None,[]
    | (n,t,Some(false))::merged ->
        let res,merged2 = search ch merged
        in  res,((n,t,Some(false))::merged2)
    | (n,t,o)::merged when n = ch ->
        let ch1 = hidden_channel t in
        Some(ch1),(n,t,Some(true))::merged
    | (n,t,o)::merged -> 
        let res,merged2 = search ch merged
        in res,(n,t,o)::merged2
  in match c with
  | Ch(n,i) -> assert(i=0); 
      begin 
        match (search n merged0) with
        | None,merged2      -> c,merged2
        | Some(ch1),merged2 -> Ch(ch1,0),merged2
      end

let replace a args p =
  if List.length a = List.length args
  then 
    let merged = Util.map2 (fun x -> (fun y -> (x,y,None))) a args
    (* the bool option is used to mean:
      * None: undefined yet
      * Some(true): defined as a channel
      * Some(false): defined as a term
      * *)
    in let rec aux merged = function
       | New(a,p) -> 
           let new_p,merged2 = aux merged p in 
           New(a,new_p),merged2
       | Create_Channel(c1,c2,p) -> 
           let c1_2,merged2  = replace_channel merged c1 in
           let new_p,merged3 = aux merged2 p in
           Create_Channel(c1_2,c2,new_p),merged3
       | Phase(n,p) ->
           let new_p,merged2 = aux merged p in
           Phase(n,new_p),merged2
       | In(c,bound,t,p) -> 
           let c2,merged2 = replace_channel merged c in
           let t2,merged3 = replace_name merged2 t in
           let new_p,merged4 = aux merged3 p in
           In(c2,bound,t2,new_p),merged4
       | Out(c,t,p) -> 
           let c2,merged2 = replace_channel merged c in
           let t2,merged3 = replace_name merged2 t in
           let new_p,merged4 = aux merged3 p in
           Out(c2,t2,new_p),merged4
       | Bang(p) ->
           let p2,merged2 = aux merged p in
           Bang(p2),merged2
       | Parallel(pl) ->
           let pl2,merged2 = aux_parallel merged pl in
           Parallel(pl2),merged2
       | Match(var,cases) ->
           let cases2,merged2 = aux_cases merged cases in
           Match(var,cases2),merged2
       | Nul -> Nul,merged
       | Reach -> Reach,merged
    and aux_parallel merged = function
       | [] -> [],merged
       | p::l -> 
           let p2,merged2 = aux merged p in
           let pl,merged3 = aux_parallel merged2 l in
           p2::pl,merged3
    and aux_cases merged_cases = function
       | [] -> [],merged_cases
       | (bound,t,p)::l ->
           let t2,merged2 = replace_name merged_cases t in
           let p2,merged3 = aux merged2 p in
           let l2,merged4 = aux_cases merged3 l in
           (bound,t2,p2)::l2,merged4
    in let res,_ = aux merged p in res
  else assert(false) (* Should have been verified earlier *)


let rec verify_channels defined = function
  | New(n,p) -> verify_channels defined p
  | Create_Channel (c1,c2,p) ->
      if (List.mem c1 defined) 
      then verify_channels (c2::defined) p
      else 
        let p_str = parsing_process_to_string (Create_Channel(c1,c2,p)) in
        let message = 
          String.concat "" 
          ["undefined channel ";channel_to_string_simple c1;
          " in process ";p_str]
        in raise (Process_error message)
  | Phase (ph,p) -> verify_channels defined p
  | In (c,bound,t,p) ->
      if (List.mem c defined) 
      then verify_channels defined p
      else 
        let p_str = parsing_process_to_string (In(c,bound,t,p)) in
        let message = 
          String.concat "" 
          ["undefined channel ";channel_to_string_simple c;
          " in process ";p_str]
        in raise (Process_error message)
  | Out (c,t,p) ->
      if (List.mem c defined) 
      then verify_channels defined p
      else 
        let p_str = parsing_process_to_string (Out(c,t,p)) in
        let message = 
          String.concat "" 
          ["undefined channel ";channel_to_string_simple c;
          " in process ";p_str]
        in raise (Process_error message)
  | Bang(p) -> verify_channels defined p
  | Parallel(pl) -> List.for_all (verify_channels defined) pl
  | Match(var,cases) ->
      let on_case (bound,pattern,p) = verify_channels defined p
      in List.for_all on_case cases
  | Nul -> true
  | Reach -> true

let parsing_to_process defs p0 =
  (* The index is used only for variables *)
  let rec aux defs index = function
  | New(n,p) ->
      let name = parsing_data_to_name n in
      let defined = string_find name defs in
      begin
        match defined with
        | None -> 
            let typed_name = parsing_data_name_to_typed_data n in
            let p1,ind = aux (typed_name::defs) index p in
            New(typed_name,p1),ind
        | Some(t) ->
            let message=
              String.concat "" ["Name ";name;" is defined inside a new but has been defined before."]
            in raise (Process_error message)
      end
  | Create_Channel (ch1,ch2,p) ->
      let p1,ind = aux defs index p 
      in Create_Channel (ch1,ch2,p1),ind
  | Phase(ph,p) ->
      let p1,ind = aux defs index p 
      in Phase (ph,p1),ind
  | In(c,[],t,p) -> 
       let new_t,def2,bound2 = 
        parsing_term_to_typed_term defs index t 
       in let p2,ind = aux def2 index p 
       in In(c,bound2,new_t,p2),ind
  | In(c,bound,t,p) -> assert(false)
  (* This case should not occur 
   * as no variable was defined as binding 
   * while parsing  
   *)
  | Out(c,t,p) ->
      let new_t,def2,bound = 
        parsing_term_to_typed_term defs index t 
      in
      begin
      match bound with
      | [] -> let p2,ind = aux def2 index p in Out(c,new_t,p2),ind
      | _ -> 
          let message = 
            "Some variable is bound by an output. This is forbidden."
          in raise (Process_error message)
      end
  | Bang(p) -> let p1,ind = aux defs index p in Bang(p1),ind
  | Parallel(pl) ->
      let pl1,ind = aux_par defs index pl in Parallel(pl1),ind
  | Match(var,cases) -> 
      let var_name = parsing_data_to_name var
      in let defined = string_find var_name defs 
      in let actual_var =
        begin
          match defined with
          | Some(t) when is_variable t -> t
          | Some(_) ->
              let message = 
                String.concat "" ["Identifier ";var_name;" is matched but is not a variable."]
              in raise (Process_error message)
          | None -> 
              let message = 
                String.concat "" ["Variable ";var_name;" occurs in a match without beeing defined before."]
              in raise (Process_error message)
        end 
      in let on_case index = function
        | ([],pattern,p) ->
            let new_pattern,def2,bound2 =
              parsing_term_to_typed_term defs index pattern
            in let p2,ind = aux def2 index p
            in (bound2,new_pattern,p2),ind
        | (_,_,_) -> assert(false)
      (* This case should not occur 
       * as no variable was defined as binding 
       * while parsing  
       *)
      in let rec on_cases index = function
        | [] -> [],index
        | a::t -> 
            let a1,ind = on_case index a
            in let t1,ind2 = on_cases (ind+1) t
            in a1::t1,ind2
      in let new_cases,ind = on_cases index cases in
       Match(actual_var,new_cases),ind
  | Nul -> Nul,index
  | Reach -> Reach,index
  and aux_par defs index = function
    | [] -> [],index
    | p::t ->
        let p1,ind = aux defs index p in
        let t1,ind2 = aux_par defs (ind+1) t in
        p1::t1,ind2
  in let p1,_ = aux defs 0 p0 in p1


