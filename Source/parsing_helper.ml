open Lexing
open Terms
open Parsing_process
open Exceptions

let pos_diff first last =
  let line = first.pos_lnum in
  let first_num = first.pos_cnum - first.pos_bol + 1 in
  let last_num  = last.pos_cnum  - last.pos_bol in
  (line,first_num,last_num)

let error_number k first last =
  let (line,first_num,last_num) = pos_diff first last 
  in let message =
    String.concat "" 
    ["Invalid number ";string_of_int k;" at line ";string_of_int line;
    " characters "; string_of_int first_num ; 
    "-" ; string_of_int last_num ]
  in raise (Parsing_error message )

let syntax_error first last =
  let (line,first_num,last_num) = pos_diff first last 
  in let message =
    String.concat "" 
    [" at line ";string_of_int line;
    " characters "; string_of_int first_num ; 
    "-" ; string_of_int last_num ]
  in raise (Parsing_error message)

type query_type = 
  (Process.process list                   (* reachability queries *)
  * ( Process.process *  type_term) list  (* dependency   queries *)
  * ( Process.process *  type_term) list  (* dep+         queries *)
  *  (Process.process *  type_term) list  (* sout         queries *)
  *  (Process.process *  type_term) list  (* s+           queries *)
  *  Process.process list                 (* soutall      queries *)
  *  Process.process list                 (* depall       queries *)
  *  Process.process list                 (* semiequiv    queries *)
  *  (Process.process * Process.process) list (* equiv    queries *)
  )

type parsing_result = {
  channels: (Process.channel list) ref; 
  constants: (typed_term list ) ref; 
  signature: (function_symbol list) ref ;
  reductions: (string* (untyped_term list) * untyped_term) list ref ;
  process_defs: (string* string list * parsing_process) list ref ;
  reachability: parsing_process list ref;
  dependencies: (parsing_process *  type_term) list ref;
  sout: (parsing_process *  type_term) list ref;
  splus: (parsing_process *  type_term) list ref;
  soutall: parsing_process  list ref;
  depall: parsing_process  list ref;
  semiequiv: parsing_process  list ref;
  depplus: (parsing_process *  type_term) list ref;
  equivalence: (parsing_process * parsing_process) list ref;
  next: bool ref ;
  query_final: query_type ref
  }

let parsing_result_to_string p_res =
  let channels_str = Util.map Process.channel_to_string !(p_res.channels) in
  let chan = String.concat ", " channels_str in
  let chan_complete = 
    match chan with
    | "" -> ""
    | s  -> String.concat "" ["channel ";s;"."]
  in
  let const_str = Util.map typed_term_to_string !(p_res.constants) in
  let const     =
    match const_str with
    | [] -> ""
    | l ->  
        let res = String.concat ","  l in 
        String.concat "" ["public ";res;"."]
  in let fun_symbs = 
    Util.map function_symbol_def_to_string !(p_res.signature)
  in let fun_symbs_defs =
    Util.map (fun x -> String.concat "" ["fun ";x;"."]) fun_symbs
  in let reduction_to_string (des,args,right) =
    let args_str = Util.map untyped_term_to_string args in
    let args_concat = String.concat "," args_str in
    let r_str = untyped_term_to_string right in
    String.concat "" ["reduc ";des;"(" ; args_concat ;") -> "; r_str ; "." ]
  in
  let reds = Util.map reduction_to_string !(p_res.reductions) in
  let process_defs =
    let to_string (name,args,p) =
      let p_str = parsing_process_to_string p in
      let args_str = String.concat ", " args  in
      match args_str with
      | ""  -> String.concat "" ["let ";name;" = ";p_str]
      | str -> String.concat "" ["let ";name;"("; str;") = ";p_str]
    in let defs_list = Util.map to_string !(p_res.process_defs)
    in String.concat "\n" defs_list
  in let str0 = String.concat "\n" (const::(fun_symbs_defs))
  in let str1 = String.concat "\n" (chan_complete::(str0::reds))
  in let reach_str =
    let to_string p =
      let p_str = parsing_process_to_string p in
      String.concat "" ["reachability(";p_str;")."]
    in let reach0 = Util.map to_string !(p_res.reachability)
    in String.concat "\n" reach0
  in let equiv_str =
    let to_string (p,q) =
      let p_str = parsing_process_to_string p in
      let q_str = parsing_process_to_string q in
      String.concat "" ["equivalence(";p_str;",";q_str;")."]
    in let equiv0 = Util.map to_string !(p_res.equivalence)
    in String.concat "\n" equiv0

  in String.concat "\n" [str1;process_defs;reach_str;equiv_str]

let init_parsing_result () =
  let channels = ref [] in
  let sign   = ref [] in
  let const  = ref [] in 
  let reds   = ref [] in
  let proc   = ref [] in
  let reach  = ref [] in
  let dep    = ref [] in
  let sout   = ref [] in
  let depplus = ref [] in
  let splus  = ref [] in
  let soutall  = ref [] in
  let depall   = ref [] in
  let semiequiv = ref [] in
  let equiv  = ref [] in
  let next   = ref true in
  let final  = ref ([],[],[],[],[],[],[],[],[]) in
  {
    channels     = channels ;
    constants    = const ; 
    signature    = sign ; 
    reductions   = reds ;
    process_defs = proc ;
    reachability = reach ;
    dependencies = dep ;
    sout         = sout ;
    depplus      = depplus ;
    splus        = splus ;
    soutall      = soutall ;
    depall       = depall ;
    semiequiv    = semiequiv ;
    equivalence  = equiv ;
    next         = next ;
    query_final  = final ;
  }

let add_channels p_res chs =
  let new_chs = List.rev_append (List.rev chs) !(p_res.channels) in
  p_res.channels := new_chs

let add_fun_symbol p_res fs =
  let sign = !(p_res.signature) in
  p_res.signature := (fs::sign)

let get_signature p_res = !(p_res.signature) 
 
let is_constant p_res name =
  let const = Util.map (is_constant_name name) !(p_res.constants) in
  let rec aux = function
    | [] -> None
    | None::t -> aux t
    | Some(a)::t -> Some(a)
  in aux const
 
let add_constants p_res c_list =
  let const = p_res.constants in
  let rec aux = function
    | [] -> () 
    | a::t ->
        begin
          let name = constant_name a 
          in match is_constant p_res name
          with
          | None    -> const := a::(!const) ; aux t
          | Some(b) -> 
              let message = String.concat "" 
              ["Several constants are named ";name]
                
              in raise (Parsing_error message)  
        end
  in aux c_list

let add_reduction p_res (name,term_list,right) =
  let to_untyped = 
    string_to_untyped_term (is_constant p_res) 
  in let untyped_term_list =
   Util.map to_untyped term_list 
  in let right_untyped = to_untyped right
  in let red = (name,untyped_term_list,right_untyped) 
  in let rec add_iter = function
    | [] -> [red]
    | (des,tl,r)::q when des = name -> 
        let message = 
          String.concat "" 
          ["destructor ";des;
          " is defined several times. This is forbidden."]
        in raise (Parsing_error message)
    | (des,tl,r)::q -> (des,tl,r)::(add_iter q)
  in p_res.reductions := (add_iter !(p_res.reductions))

let add_process_def p_res (name,args,p) =
  p_res.process_defs := (name,args,p)::(!(p_res.process_defs))

let call p_res (name,args) =
  let rec search (name,args) = function
    | [] -> 
        let message = 
          String.concat "" 
          ["Process ";name;" is called but is not defined"]
        in raise (Parsing_error message)
    | (n,a,p)::l ->
        if n = name
        then 
          let len1 = List.length args in
          let len2 = List.length a    in
          if  len1 = len2 
          then replace a args p
          else 
            let message = 
              String.concat "" 
              ["Process P is defined with ";string_of_int len2;" arguments but is called with ";string_of_int len1 ; " arguments."]
            in raise (Parsing_error message)
        else search (name,args) l
  in search (name,args) !(p_res.process_defs)

let add_reachability p_res p =
  p_res.reachability := p::(!(p_res.reachability))

let add_dep p_res (p,tau) =
  p_res.dependencies := (p,tau)::(!(p_res.dependencies))

let add_sout p_res (p,tau) =
  p_res.sout := (p,tau)::(!(p_res.sout))

let add_splus p_res (p,tau) =
  p_res.splus := (p,tau)::(!(p_res.splus))

let add_depplus p_res (p,tau) =
  p_res.depplus := (p,tau)::(!(p_res.depplus))

let add_soutall p_res p =
  p_res.soutall := p::(!(p_res.soutall))

let add_depall p_res p =
  p_res.depall := p::(!(p_res.depall))

let add_semiequiv p_res p =
  p_res.semiequiv := p::(!(p_res.semiequiv))

let add_equivalence p_res p q =
  p_res.equivalence := (p,q)::(!(p_res.equivalence))

let verify_channels_pres p_res =
  let pre_defined = !(p_res.channels) in
  let defs = !(p_res.process_defs) in
  let on_one (a,b,c) = 
    let b0 = Util.map new_channel b in
    try
    verify_channels (List.rev_append b0 pre_defined) c
    with
    | Process_error(m) -> 
        let p_str = parsing_process_to_string c in
        let message = String.concat "" [m;",\n inside process:\n";p_str]
        in raise (Process_error message)
  in 
  begin
    assert(List.for_all on_one defs);
  (* verify_channels returns true or raises an exception *)
    let reach = !(p_res.reachability) in
    assert(List.for_all (verify_channels pre_defined) reach);
    let on_one (p,q) =
      (verify_channels pre_defined p) && (verify_channels pre_defined q)
    in let equiv = !(p_res.equivalence) in
    assert(List.for_all on_one equiv)
  end

let next_step p_res = !(p_res.next)

let stop p_res = 
  begin
  verify_channels_pres p_res ;
  p_res.next := false ;
  p_res.constants    := List.rev !(p_res.constants) ;
  p_res.signature    := List.rev !(p_res.signature) ;
  p_res.reductions   := List.rev !(p_res.reductions) ;
  p_res.process_defs := List.rev !(p_res.process_defs) ;
  p_res.reachability := List.rev !(p_res.reachability);
  p_res.dependencies := List.rev !(p_res.dependencies);
  p_res.sout         := List.rev !(p_res.sout);
  p_res.splus        := List.rev !(p_res.splus);
  p_res.soutall      := List.rev !(p_res.soutall);
  p_res.depall       := List.rev !(p_res.depall);
  p_res.semiequiv    := List.rev !(p_res.semiequiv);
  p_res.equivalence  := List.rev !(p_res.equivalence);
  p_res.query_final  := 
    let defs = Util.map typed_term_to_typed_data !(p_res.constants)
    in let reach_parsed = 
      Util.map (parsing_to_process defs) !(p_res.reachability) 
    in let tomap (x,y) = (parsing_to_process defs x, y)
    in let dep =
      Util.map tomap !(p_res.dependencies)
    in let sout =
      Util.map tomap !(p_res.sout)
    in let splus =
      Util.map tomap !(p_res.splus)
    in let depplus =
      Util.map tomap !(p_res.depplus)
    in let soutall = 
      Util.map (parsing_to_process defs) !(p_res.soutall)
    in let depall = 
      Util.map (parsing_to_process defs) !(p_res.depall)
    in let semiequiv = 
      Util.map (parsing_to_process defs) !(p_res.semiequiv)
    in let equiv = 
      Util.map (fun (p,q) -> (parsing_to_process defs p, parsing_to_process defs q)) !(p_res.equivalence)
    in (reach_parsed,dep,depplus,sout,splus,soutall,depall,semiequiv,equiv)
  end

let get_query p_res = 
  let data = !(p_res.channels),!(p_res.constants),!(p_res.signature),!(p_res.reductions) in
  !(p_res.query_final),data
