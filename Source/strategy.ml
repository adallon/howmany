type strategy = Overapp | Small | All

(*
 * The overapp strategy is to just verify the overapproximating scenario
 * The small strategy is to just verify (all) the small scenarios
 * The last strategy (all) is to verify all scenarios (overapproximating and others).
 *)

