type tool_params = string * string * (string -> string) * string * string

type process_data = 
  Process.channel list * Terms.typed_term list * Terms.function_symbol list * Terms.reduction list

val create_tool_files: tool_params -> bool -> string -> string list list list -> unit
