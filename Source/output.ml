open Satequiv
open Tool_encoder
open Standard_output
(* open Deepsec *)
open Count


type output_options =
  {deepsec:bool; satequiv: bool; standard: bool; verbosity:int; run: bool ; count:bool }

let to_output_options (deepsec,sateq,std,verb,run,count) =
  {deepsec = deepsec ; satequiv = sateq || run ; standard = not(deepsec||sateq||run) || std  ; 
  verbosity = verb  ; run = run ; count = count || deepsec }

let verbosity o = o.verbosity

let has_satequiv opt = opt.satequiv
let has_run opt = opt.run
let has_count opt = opt.count

let final_output strat output_opt data filename reach_scen (dep_scen,depplus_scen, sout_scen, splus_scen,soutall_scen,depall_scen,semiequiv_scen) =
  let (chans,constants,sign,reds) = data in
  let _ =
    if output_opt.standard
    then 
      begin
        print_scenarios_reach reach_scen;
        print_scenarios_dep dep_scen;
        print_scenarios_depplus depplus_scen;
        print_scenarios_sout sout_scen;
        print_scenarios_splus splus_scen;
        print_scenarios_soutall soutall_scen;
        print_scenarios_depall depall_scen;
        print_scenarios_semiequiv semiequiv_scen;
      end
  in
  let _ =
    if output_opt.satequiv
    then 
      let encodings_list = 
        Util.map (sat_equiv_encodings strat constants) reach_scen
      in let run = output_opt.run
      in Tool_encoder.create_tool_files satequiv_params run filename encodings_list
  in let _ =
    if output_opt.count
    then 
      (* let _ = count_in_file filename (List.flatten reach_scen) in () *)
      let _ = 
        count_in_file filename semiequiv_scen ;
      in () 
  in ()
      (*
    begin
    let _ = 
      match dep_scen with
      | [] -> ()
      | l -> print_string "\nSAT-Equiv encoding of dependencies of types is disabled.\n" 
    in match sout_scen with
      | [] -> ()
      | l -> print_string "\nSAT-Equiv encoding of S_out is disabled.\n" 
    end*)


