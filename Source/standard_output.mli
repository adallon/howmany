val print_scenarios_reach:
  Process.scenarios list list -> unit

val print_scenarios_dep:
  Process.scenarios list -> unit

val print_scenarios_depplus:
  Process.scenarios list -> unit
  
val print_scenarios_sout:
  Process.scenarios list -> unit

val print_scenarios_splus:
  Process.scenarios list -> unit

val print_scenarios_soutall:
  (string * Process.scenarios ) list -> unit

val print_scenarios_depall:
  (string * Process.scenarios ) list -> unit

val print_scenarios_semiequiv:
  Process.scenarios list -> unit
