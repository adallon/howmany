open Exceptions
type sort = Atom | Bitstring ;;

let sort_to_string = function
  | Atom -> "atom"
  | Bitstring -> "bitstring"

let print_sort s = print_string (sort_to_string s)

let rec sort_list_to_string sort_list =
  let string_list = Util.map sort_to_string sort_list in
  String.concat ", " string_list 

let sort_array_to_string ar =
  let string_array = Array.map sort_to_string ar in
  Util.concat_string_array ", " string_array

let print_sort_list sl =
  print_string (sort_list_to_string sl)

type atoms = 
  (* The int field will be used for renaming.
   * Constants will not be renamed.
   *)
  Name   of string * int  (* renaming for replication *)
  |Const of string  
  | Var  of string * int * int 
  (* renaming for separate branches 
   * and renaming for replication *)

let atoms_to_string = function
  | Name (str,i) -> String.concat "x" [str;string_of_int i ]
  | Var (str,i,j)  -> 
      String.concat "x" [str;string_of_int i]
  | Const (str)  -> str

let atoms_to_string_simple = function
  | Name (str,_) -> str
  | Var (str,_,_)  -> str
  | Const (str)  -> str

let print_atoms a = print_string (atoms_to_string a)

let new_name str  = Name(str,0)
let new_const str = Const(str)
let new_var str   = Var(str,0,0)

type function_symbol = string * (sort array)

let to_function_symbol name ar_list =
  let ar_array = Array.of_list ar_list in
  (name,ar_array)

let function_symbol_def_to_string (name,ar) =
  let sorts = sort_array_to_string ar 
  in String.concat "" [name;"(";sorts;")"]

let function_name_and_arity (name,ar) = (name,Array.length ar)



let print_function_symbol (str,ar) = print_string str

let find_function_symbol signature name =
  let rec aux = function
    | [] -> None
    | (str,ar)::t when str = name -> Some(str,ar)
    | (str,ar)::t -> aux t
  in aux signature


type 'a term = 
  Function of function_symbol * ('a term array)
  | Tuple of int * 'a term array
  | Data of 'a


let term_to_string data_to_string bound t =
  let rec aux bound = function
  | Data(a) -> data_to_string bound a
  | Tuple(n,t) ->
      let len = Array.length t in
      let to_fold (bound,t0) = aux bound t0 in
      let new_bound,(t_string_ar:string array) = 
        Util.fold_map to_fold bound t 
      in
      let t_string = Util.concat_string_array ", " t_string_ar in
      let _ =
        assert(len = n)
      in (new_bound,String.concat "" [ "("; t_string ;")" ])
  | Function(fun_symb,t) ->
      let fun_name,_ = fun_symb in 
      let to_fold (bound,t0) = aux bound t0 in
      let new_bound,t_string_ar = 
        Util.fold_map to_fold bound t 
      in
      let t_string = Util.concat_string_array ", " t_string_ar in
      (new_bound,String.concat "" [ fun_name ; "("; t_string ;")" ])
  in let _,s = aux bound t in s
      

let term_to_sort = function
  | Function(_) -> Bitstring
  | Tuple (_)   -> Bitstring
  | Data(_)     -> Atom

let is_composed t = (term_to_sort t = Bitstring)
let is_tuple = function
  | Tuple(_) -> true
  | _ -> false

let to_data a = Data(a)
let to_tuple l =
  match l with
  | [] -> assert(false) (* It should not happen *)
  | [a] -> a (* A tuple of one element is not a tuple *)
  | l -> let t_ar = Array.of_list l in Tuple(List.length l,t_ar)

let composite_term signature name arg_list =
  let (str,ar0) = 
    match find_function_symbol signature name with
    | Some(a,b) ->(a,b)
    | None ->
        let message = 
          String.concat "" ["function ";name;" is not defined"]
        in raise (Term_error message )
  in let arg_array = Array.of_list arg_list 
  in let matching_sort ar args =
    if (Array.length ar = Array.length args)
    then
      begin
        let match_arity (s,a) =
          match s,a with
          | Atom,Bitstring -> false
          | _ -> true
        in 
        let sorts = Array.map term_to_sort args in
        let merged = Array.map2 (fun x -> (fun y -> (x,y))) ar sorts
        in Array.for_all match_arity merged
      end
    else
      let ar_length  = string_of_int (Array.length ar)   in
      let arg_length = string_of_int (Array.length args) in
      let message = 
        String.concat "" 
        ["function ";name;" has arity ";ar_length;
        " but is called with ";arg_length;" arguments"]  
      in raise (Term_error message)

  in 
  if matching_sort ar0 arg_array
  then Function((str,ar0),arg_array)
  else 
    let arg_sort = Util.map term_to_sort arg_list in
    let arg_string = sort_list_to_string  arg_sort in
    let message =
    String.concat "" 
    ["function ";name;" has arity ";sort_array_to_string ar0;
    " but is called with arguments of sort ";arg_string]  
  in raise (Term_error message)


type atom_type = string

let to_atom_type str = str

type type_term = atom_type term

let type_to_string = 
  let data_to_string bound a = (bound,to_atom_type a) in
  term_to_string data_to_string []

let atom_type_to_type at = at 

let get_subtypes = function
  | Function(f,args) ->  args
  | Tuple(n,args)    ->  args
  | _ -> [||]

type untyped_term = atoms term

let untyped_term_to_string = 
  let data_to_string bound a = (bound,atoms_to_string a) in
  term_to_string data_to_string []

let untyped_term_to_string_alt = 
  let data_to_string bound a = (bound,atoms_to_string_simple a) in
  term_to_string data_to_string []
let rec string_to_untyped_term is_defined = function
  | Data(s) ->
      begin
      match is_defined s with
      | None -> Data(Var(s,0,0))
      | Some(a,tau) -> Data(a) (* We forget the type ! *)
      end
  | Tuple(n,tl) ->
      Tuple(n,Array.map (string_to_untyped_term is_defined) tl)
  | Function(symb,args) ->
      Function(symb,Array.map (string_to_untyped_term is_defined) args)


let atomic_position t a =
  let rec aux atomic_pos res = function
    | Data(s) -> 
        if s = a
        then atomic_pos
        else res
    | Tuple(n,tl) ->
        Array.fold_left (aux false) res tl
    | Function(symb,args) ->
        let _,sorts = symb in
        let args_sorts =
          Array.init (Array.length sorts) (fun i -> (args.(i),sorts.(i))) 

        in let to_fold res (arg,sort) =
          if res
          then aux (sort = Atom) true arg
          else false

        in Array.fold_left to_fold res args_sorts 
  in aux false true t

let direct_subterm t1 right =
   match t1 with
   | Data(_) -> false
   | Tuple(_) -> assert(false)
   (* Rules should not act on tuples *)
   | Function(fs,args) ->
       let f arg =  (arg = right)
       in Array.exists f args

type shape = function_symbol * untyped_term 


let get_shapes_term arr =
  let rec aux res = function
    | Data(_) -> res
    | Tuple(fn,args) ->
        let is_data b = function Data(_) -> b | _ -> false in
        if Array.fold_left is_data true args
        (* The tuples should have a trivial shape
         * so they must have only data subterms *)
        then res
        else raise (Term_error "")
    | Function(fs,args) -> 
        Array.fold_left aux ((fs,Function(fs,args))::res) args
  in List.fold_left aux [] arr
  
let compatibility shape_list =
  let rec compatible = function
    | Data(_),Data(_) -> true
    | Function(fs1,args1),Function(fs2,args2) ->
        let regroup i = (args1.(i),args2.(i)) in
        let args = 
          Array.init (Array.length args1) regroup
        in let to_fold b (a1,a2) = b && compatible (a1,a2)
        in 
        (fs1 = fs2) && (Array.fold_left to_fold true args)
    | Tuple(fn1,args1),Tuple(fn2,args2) -> fn1 = fn2
    | _,_ -> false 
  in let rec check_compatibility_of_one (fs,shf) = function
    | [] -> true
    | (fs1,shf1)::t when fs1 = fs ->
        if (compatible (shf,shf1)) 
           && (check_compatibility_of_one (fs,shf) t)
        then true
        else 
          let (name,_) = fs in
          raise (Compatibility_error name)
    | _::t -> check_compatibility_of_one (fs,shf) t 
  in let rec check_compatibility = function
    | [] -> true
    | a::t -> 
        check_compatibility_of_one a t && check_compatibility t
  in if check_compatibility shape_list
  then true
  else assert(false) 
  (* should never occur as check_compatibility
   * returns either true or an exception *)

type typed_data   = (atoms * type_term)

let typed_data_to_string (at,tau) =
    String.concat ":" [atoms_to_string at; type_to_string tau] 

let to_typed_data (a,t) = (a,t)
let typed_data_to_atoms (a,b) = a

let rec string_find str = function
  |[] -> None
  |(Name(str0,i),tau)::t when str0 = str -> Some(Name(str0,i),tau) 
  |(Var(str0,i,j),tau)::t when str0 = str -> Some(Var(str0,i,j),tau) 
  |(Const(str0),tau)::t when str0 = str -> Some(Const(str0),tau)
  | _::t -> string_find str t 

let is_variable = function
  |(Var(_,_,_),_) -> true
  | _ -> false


type typed_term   = typed_data term

let typed_term_to_string = 
  let data_to_string bound a = (bound,typed_data_to_string a) in 
  term_to_string data_to_string []

let typed_term_to_string_alt bound = 
  let rec find_and_remove res a = function
    | [] -> false,List.rev res
    | b::t ->
        if a = b then true, List.rev_append res t
        else find_and_remove (b::res) a t
  in
  let typed_data_to_string_alt bound typed_data =
    let (mems,new_bound) = find_and_remove [] typed_data bound in
    if mems
    (* The variable of bound are those that are bound in our term *)
    then
      (* When they are bound in our term, they must be printed
       * with their type *)
      (new_bound,typed_data_to_string typed_data)
    else
      (* When they are not bound in our term, they must be printed
       * without their type (as in the input language)*)
      let at = typed_data_to_atoms typed_data
      in (new_bound,atoms_to_string at)
  in
  term_to_string typed_data_to_string_alt bound

let to_typed_term t = t
let is_well_shaped shape_list term =

  let rec find_shapes res fs = function
    | [] -> res
    | (f,sh)::t when f = fs -> find_shapes (sh::res) fs t
    | _::t -> find_shapes res fs t
  in

  let rec match_shape res = function
    | (Data(_),_) -> res
    | (Tuple(arS,argsS),Tuple(arT,argsT)) -> (arS = arT)
    (* Excepted data, tuples have no subterms in shapes.
     * Thus, we only need to check the arity of the tuples *)
    | (Function(fsS,argsS),Function(fsT,argsT)) ->
        if (fsS = fsT)
        then 
          let arr_len = Array.length argsS in
          let init_fun i = (argsS.(i),argsT.(i)) in
          let merged = Array.init arr_len init_fun
          in Array.fold_left match_shape res merged
        else
          false
    | _ -> false

  in let rec aux res = function
    | Data(x) -> res
    | Tuple(n,args) ->
        if res
        then Array.fold_left aux true args
        else false
    | Function(fs,args) -> 
        if res
        then
          let shapes = find_shapes [] fs shape_list in
          let to_fold b sh =
            if b 
            then
              begin
              if match_shape true (sh,Function(fs,args))
              then true
              else
                let message = String.concat "" 
                ["Subterm ";typed_term_to_string (Function(fs,args));
                " is not well-shaped,\n as it does not follow the shape ";
                untyped_term_to_string sh;".\n This is forbidden."]
                in raise (Term_error message)
              end
            else
              false  
          in List.fold_left to_fold true shapes
        else
          false

  in aux true term

let get_variables t =
  let rec aux res = function
    | Data(Var(x,y,z)) -> (Var(x,y,z)::res) 
    | Data(Name(_,_)) ->  assert(false)
    (* This function should only be used on rules
     * and thus there should be no name in them. *)
    | Data(Const(_)) -> res
    | Tuple(n,args) -> Array.fold_left aux res args
    | Function(fs,args) ->
        Array.fold_left aux res args
  in aux [] t



let rec untype = function
  | Data(a,tau) -> Data(a)
  | Tuple(n,ta) -> Tuple(n,Array.map untype ta)
  | Function(f,ta) -> Function(f,Array.map untype ta)
let define_constant name type_term = 
  let c = new_const name in Data(c,type_term)

let typed_term_to_typed_data = function
  |Data(d) -> d
  | _ -> assert(false)
  (* should only be used on data *)

let is_constant_name name c =
  match c with
  | Data(Const(str),tau) -> 
      if str = name
      then Some(Const(str),tau)
      else None
  | _ -> None

let constant_name c =
  match c with
  | Data(Const(str),_) -> str
  | _ -> assert(false) (* should not be called on non-constant *)

let replication_of_data ind = function
  | (Name(name,i),tau) -> assert(i=0); (Name(name,ind),tau)
  | (Var(x,i,j),tau) -> assert(j=0); (Var(x,i,ind),tau)
  | _ -> assert(false) 
  (* constants cannot be replicated 
   * and this function should not be called on them
   *)


type parsing_data = (string * (type_term option))
let parsing_data_to_string (a,tau_op) =
 match tau_op with
 | None -> a
 | Some(tau) -> String.concat "" [a;":";type_to_string tau]  

let parsing_data_to_name (a,tau_op) = a

let string_and_type_to_nonce (n,tau) = (n,Some(tau))
let string_to_parsing_data n = (n,None)

let parsing_data_name_to_typed_data (name,tau_op) =
  match tau_op with
  | None -> assert(false) 
  (* should not occur for a nonce declaration *)
  | Some(tau) -> (Name(name,0),tau)


type parsing_term = parsing_data term
let parsing_term_to_string = 
  let data_to_string bound a = bound, parsing_data_to_string a
  in 
  term_to_string data_to_string []
let to_parsing_tuple = to_tuple 
let composite_parsing_term = composite_term
let parsing_untyped_data s = Data(s,None)
let parsing_typed_data (s,tau) = Data(s,Some(tau))

let rec string_to_parsing_term = function
  | Data(a) -> Data(a,None)
  | Function(f,args) -> 
      Function(f,Array.map string_to_parsing_term args)
  | Tuple(n,ta) -> Tuple(n,Array.map string_to_parsing_term ta)

let rec parsing_term_to_typed_term defined index = function
  | Data(a,None) ->
      begin 
        match string_find a defined with
        | None ->
            let message = 
              String.concat "" ["identifier ";a;" has never been defined. Perhaps you forgot to type it."]
            in raise (Term_error message)
        | Some(t,tau) -> Data(t,tau),defined,[]
      end
  | Data(a,Some(tau)) ->
      begin
        match string_find a defined with
        | Some(_) -> 
            let message = 
              String.concat " " ["Identifier";a;"is used as a binding variable but has been defined before."]
            in raise (Term_error message)
        | None -> 
            let typed_data = Var(a,index,0),tau in
            let defined2 = typed_data::defined in
            Data(typed_data),defined2,[typed_data]
      end
  | Tuple(n,ta) ->
      let ta2,def2,bound = aux_arr defined index ta
      in Tuple(n,ta2),def2,bound
  | Function(f,ta) ->
      let ta2,def2,bound = aux_arr defined index ta
      in Function(f,ta2),def2,bound

and aux_arr defined index ta =
  let new_ta = 
    Array.map (fun _ -> Data (Const(""),Data(""))) ta 
  in let rec aux bound def index i =
    if (i = Array.length ta) 
    then new_ta,def,bound
    else
      let ta_i = ta.(i) in
      let new_ta_i,def2,new_bound = 
        parsing_term_to_typed_term def index ta_i
      in
      let bound2 = List.rev_append (List.rev new_bound) bound
      in
      begin
        new_ta.(i) <- new_ta_i ;
        aux bound2 def2 index (i+1)
      end
  in aux [] defined index 0


let replace_name merged t =
  let rec find name = function
    | [] -> None,[]
    | (n,t,Some(true))::l -> 
        let res,l_res = find name l in res,(n,t,Some(true))::l_res
    | (n,t,o)::l when (n = name) -> Some(t),(n,t,Some(false))::l
    | (n,t,o)::l ->
        let res,l_res = find name l in res,(n,t,o)::l_res
  in let rec aux merged = function
    | Data(a,None) ->
        begin
          match find a merged with
          | Some(t),merged2 -> t,merged2
          | None,merged2    -> Data(a,None),merged2
        end
    | Data(a,Some(tau)) ->
        begin
          match find a merged with
          | Some(_),_ -> 
              let message =
                String.concat "" 
                ["Identifier ";a;" is bound after beeing also used ";
                "as an argument of a process. This is forbidden."]
              in raise (Term_error message)
          | None,_ -> Data(a,Some(tau)),merged
        end
    | Function(f,args) ->
        let args2 = Array.copy args in
        let arr,merged2 = aux_array merged args2 0 in
        Function(f,arr),merged2
    | Tuple(n,args)    ->
        let args2 = Array.copy args in
        let arr,merged2 = aux_array merged args2 0 in
        Tuple(n,arr),merged2
  and aux_array merged arr = function
    | i when i = Array.length arr -> arr,merged
    | i -> 
        let t_i,merged_i = aux merged (arr.(i)) in
        begin
          arr.(i) <- t_i ; 
          aux_array merged_i arr (i+1)
        end
  in aux merged t

let replication_data_term names vars t =
  let rec search_in_list n = function
    | [] -> n
    | (old,renaming)::t ->
        if n = old then renaming else search_in_list n t
  in let rec aux name_list vars = function
  | Data(Name(name,i),tau) ->
      let new_name = search_in_list (Name(name,i),tau) name_list
      in Data(new_name)
  | Data(Var(name,i,j),tau) ->
      assert(j=0);
      let new_var = search_in_list (Var(name,i,j),tau) vars 
      in Data(new_var)
  | Data(a) -> Data(a)
  | Function(f,args) ->
      let args1 = Array.map (aux name_list vars) args 
      in Function(f,args1)
  | Tuple(num,args) ->
      let args1 = Array.map (aux name_list vars) args 
      in Tuple(num,args1)
  in aux names vars t

let hidden_channel t =
  match t with
  | Data(ch1,None) -> ch1
  | _  -> 
      let message = 
        "A channel is instanciated with a composed term. This is forbidden."
      in raise (Term_error message)

let rec get_type = function
  | Data(t,tau) -> tau
  | Tuple(n,st) -> Tuple(n,Array.map get_type st)
  | Function(f,args) -> Function(f,Array.map get_type args)

let verify_types_match pairs_match =
  let check_one ((x,tau),y) =
    if tau = get_type y then true
    else 
      let t1_str = typed_term_to_string (Data(x,tau)) in
      let y_str  = typed_term_to_string y in
      raise (Type_compliance_error(t1_str,y_str))
  in
  List.for_all check_one pairs_match

let rec get_encrypted_subterms res = function
  (* We assume that any function symbol is an encryption. *)
  | Data(a,tau) -> res
  | Tuple(n,args) -> Array.fold_left get_encrypted_subterms res args
  | Function(f,args) ->
      let new_res = ((Function(f,args))::res) in
      Array.fold_left get_encrypted_subterms new_res args

let check_unifiable_terms t1 t2 =
  let rec aux t1 t2 =
    match t1,t2 with
    | Data(Var(x,i,j),tau),t -> true, [(Var(x,i,j),tau),t]
    | t,Data(Var(x,i,j),tau) -> true, [(Var(x,i,j),tau),t] 
    | Data(x,_),Data(y,_)  -> (x = y), []
    | Tuple(na,ta),Tuple(nb,tb) when (na = nb) ->
       assert(Array.length ta = Array.length tb); aux_array ta tb
    | Function(fa,args_a),Function(fb,args_b) when fa = fb ->
       assert(Array.length args_a = Array.length args_b); 
       aux_array args_a args_b
    | _ -> false,[]
   and aux_array args_a args_b =
     let rec search (x,i,j,tau) = function
       | [] -> Data(Var(x,i,j),tau)
       | ((Var(x1,i1,j1),tau1),t)::_ 
           when (x1 = x)&&(i1 = i)&&(j1 = j) 
           -> assert(tau = tau1); t
       | _::q -> search (x,i,j,tau) q
     in let rec replace list_replace = function
       | Data(Var(x,i,j),tau) -> search (x,i,j,tau) list_replace
       | Data(a) -> Data(a)
       | Tuple(n,ta) ->
           Tuple(n,Array.map (replace list_replace) ta)
       | Function(f,args) ->
           Function(f,Array.map (replace list_replace) args)
     in 
     let to_fold (b,list_replace) (t1,t2) =
(* (bool,typed_data*typed_term list) -> typed_term -> (bool,typed_data*typed_term list) *)
       if b 
       then
         let t1_replaced,t2_replaced =
           replace list_replace t1,replace list_replace t2
         in let b1,l1 = aux t1_replaced t2_replaced
         in b1,List.rev_append l1 list_replace
       else 
         false,[]
     in let len = Array.length args_a  
     in let args = Array.init len (fun i -> (args_a.(i),args_b.(i)))
     in Array.fold_left to_fold (true,[]) args
  in let b,_ = aux t1 t2 in b

let check_compliance_pair t1 t2 =
  let unif = check_unifiable_terms t1 t2 in
  match unif with
  | false -> true
  | true ->
      if get_type t1 = get_type t2 then true
      else 
        let t1_str = typed_term_to_string t1 in
        let t2_str = typed_term_to_string t2 in
        raise (Type_compliance_error (t1_str,t2_str))

let check_compliance_terms l l_match =
  let b = verify_types_match l_match in
  let compare_one (t:typed_term) (l:typed_term list) =
    List.for_all (check_compliance_pair t) l
  in let rec compare_all = function
    |[] -> true
    |a::t -> (compare_one a t)&&(compare_all t)
  in let encrypted = 
    List.fold_left get_encrypted_subterms [] l
  in b && compare_all encrypted


let get_dishonest_types l =

  let rec aux res = function
    (* only names types can be honest *)
    | Data(Var(_),tau)   -> (tau::res)
    | Data(Const(_),tau) -> (tau::res)
    | Data(Name(_),tau)  -> res
    | Function(f,args) -> Array.fold_left aux res args
    | Tuple(n,args) -> Array.fold_left aux res args
 
  in List.fold_left aux [] l

let get_name_types l =
  let rec aux res = function
    | Data(Name(_),tau)  -> tau::res
    | Data(_) -> res
    | Function(f,args) ->
        Array.fold_left aux res args
    | Tuple(n,args) ->
        Array.fold_left aux res args
  in List.fold_left aux [] l

let get_subterms tau =
  let rec aux res = function
    | Data(x) -> (Data(x))::res
    | Tuple(n,args) ->
        let t = Tuple(n,args) in
        let new_res = t::res  in
        Array.fold_left aux new_res args
    | Function(f,args) ->
        let t = Function(f,args) in
        let new_res = t::res  in
        Array.fold_left aux new_res args
  in aux [] tau

let is_public_type verbose name_types tau =
  let subterms = get_subterms tau in
  let rec aux = function
    | [] -> 
        let _ =
          if verbose > 0
          then 
            begin
              print_string "\n  public type: ";
              print_string (type_to_string tau);
              if verbose > 1 then print_newline ()
            end
          else ()
        in true
    | tau0::l -> 
        if List.mem tau0 subterms
        then false
        else aux l
  in aux name_types

type reduction = string * (untyped_term list) * untyped_term

let is_built_from  constants untyped_term = 
  let (untyped_constants:untyped_term list) = 
    List.map untype constants 
  in let rec aux = function
    | Data(x) -> List.mem (Data(x)) untyped_constants
    | Tuple(fn,args) ->
        Array.fold_left (fun x -> fun y -> x && aux y) true args
    | Function(fs,args) ->
        Array.fold_left (fun x -> fun y -> x && aux y) true args
  in aux untyped_term


type rho_prep  = (function_symbol * int * untyped_term list)
(* each rho_prep corresponds to a reduction rule.
 * *function symbol* is the reduced function symbol
 * (aenc in adec(aenc(x,pub(y)),y) -> x )
 * *int* is the index of the subterm which is obtained
 * ( 0 in aenc(x,pub(y)) as x has index 0)
 * *untyped_term_list* is the list of terms of the reduction rule
 * (ie [aenc(x,pub(y)), y])
 * *)

let prepare_rho reductions =
  (* computes all the rho_prep from the reduction rules *)
  let on_one_element (des,left,right) =
    let t1 = match left with
    | a::_ -> a
    | [] -> assert(false) (* No rule should be empty *)
    in let fs,args =
      match t1 with
      | Function(fs,args) -> fs,args
      | Tuple(_,_) -> 
          let message = 
            String.concat "" 
            ["The reduction rule with destructor ";des ;
            " reduces tuples. It is forbidden." ]
          in raise (Term_error message)

      | Data(_)-> 
          let message = 
            String.concat "" 
            ["The reduction rule with destructor ";des ;
            " reduces no function symbol. It is forbidden." ]
          in raise (Term_error message)

    in let rec recognize_subterm len arg = function
      | i when len = i -> 
          let message =
            String.concat "" 
            [ "The rewriting system should be subterm-convergent.";
            " This is not the case of the rule with destructor ";des;"."]
          in raise (Term_error message)
      | i when arg.(i) = right -> i
      | i  -> recognize_subterm len arg (i+1)
    in let i0 = recognize_subterm (Array.length args) args 0 
    in (fs,i0,left)
  in List.map on_one_element reductions


let unificator (pattern:untyped_term) term =
  let rec search (x,i,j) = function
    | [] -> None
    | (Var(x1,i1,j1),t)::_ when (x1 = x)&&(i1 = i)&&(j1 = j) -> 
        Some(t)
    | _::q -> search (x,i,j) q
  in
  let rec aux res pat term =
    match pat,term with
    | Data(Var(x,i,j)),tau -> 
        begin
          match search (x,i,j) res with
          | Some(t) -> 
              if t = tau
              then true,res
              else false,[]
          | None    -> true,(Var(x,i,j),tau)::res
        end
    | Data(x),_  ->
        let message = "At left of reduction rules, atoms should be variables."
        in raise (Term_error message)
    (* The only data should be variables in rules *)
    | Tuple(na,ta),Tuple(nb,tb) when (na = nb) ->
       assert(Array.length ta = Array.length tb); aux_array res ta tb
    | Function(fa,args_a),Function(fb,args_b) when fa = fb ->
       assert(Array.length args_a = Array.length args_b); 
       aux_array res args_a args_b
    | _ -> false,[]
   and aux_array res args_a args_b =

     let to_fold (b,res) (t1,t2) =
(* (bool,res) -> pattern*type -> (bool,res) *)
       if b 
       then aux res t1 t2
       else false,[]
     in let len = Array.length args_a  
     in let args = Array.init len (fun i -> (args_a.(i),args_b.(i)))
     in Array.fold_left to_fold (true,[]) args
  in aux [] pattern term 

let replace_internal theta pat =
  let rec search (x,i,j) = function
    | [] -> None
    | (Var(x1,i1,j1),t)::_ when (x1 = x)&&(i1 = i)&&(j1 = j) -> Some(t)
    | _::q -> search (x,i,j) q
  in let rec aux = function
    | Data(Var(x,i,j))  ->
        begin
          match search (x,i,j) theta with
          | None -> assert(false)
          | Some(t) -> t
        end
    | Data(x) -> assert(false) (* it should not happen *)
    | Function(fs,args) -> Function(fs,Array.map aux args)
    | Tuple(n,args) -> Tuple(n,Array.map aux args)
  in aux pat


type position = int list

let rho_generic rho_prep t =
  (* given a term t and the list of rho_prep
   * obtained from the reduction rules,
   * this function computes the function rho (for types and terms)
   * as described in the paper
   * Recall that this function sends a list of (tau0,p)#S
   * where tau0 is a type and S is the set of type that should 
   * be deduced before deducing an element of type tau0 at position p
   * in tau.
   * e.g. 
      * rho(senc(tau1,tau2)) = {
        * (senc(tau1,tau2),root)#emptyset ;
        * (tau1, 0)#{tau2} }
      * as we need an element of type tau2 to deduce
      * the element of type tau1 from senc(tau1,tau2)
   *
   *
   * The result sent is thus a list of tuples tau0,p,S where:
     *  tau0 is a type 
     *  p is a position (int list)
     *  S is a list of types.
   * *)

  let rec find_f fs res = function
    | [] -> res
    | ((fs0,i0,left0)::t) when fs0 = fs -> 
        find_f fs ((i0,left0)::res) t
    | _::t -> find_f fs res t
  in 
  let rec aux (pos:position) s res = function
    | Data(x) -> (Data(x),pos,s)::res
    | Function(fs,args) -> 
        let rules = find_f fs [] rho_prep in
        let new_res = (Function(fs,args),pos,s)::res in
        let on_one_rule res (i0,left0) =
          let l1,l_plus =
            match left0 with
            | a::t -> a,t
            | [] -> assert(false) (* should not be empty *)
          in let b,theta = unificator l1 (Function(fs,args)) 
          in if b
          then
            let new_s = 
              List.rev_append (List.map (replace_internal theta) l_plus) s
            in aux (i0::pos) new_s res args.(i0)
          else res
        in List.fold_left on_one_rule new_res rules
    | Tuple(n,args) -> 
        let to_fold i0 res term = aux (i0::pos) s res term
        in 
        Util.fold_left_i to_fold ((Tuple(n,args),pos,s)::res) args
  in aux [] [] [t,[],[]] t 

type 'a rho_element = 'a * position * 'a list

let rho rho_prep tau = rho_generic rho_prep tau
let rho_term = rho_generic

(* Functions used for the marking computation,
 * and in particular with sigma_match
 *)

let replace sigma pat =
  let rec search (x,i,j) = function
    | [] -> None
    | (Var(x1,i1,j1),t)::_ when (x1 = x)&&(i1 = i)&&(j1 = j) -> Some(t)
    | _::q -> search (x,i,j) q
  in let rec aux = function
    | Data(Var(x,i,j),tau)  ->
        begin
          match search (x,i,j) sigma with
          | None ->  Data(Var(x,i,j),tau) 
          | Some(t) -> t
        end
    | Data(x) -> Data(x)
    | Function(fs,args) -> Function(fs,Array.map aux args)
    | Tuple(n,args) -> Tuple(n,Array.map aux args)
  in aux pat

let typed_unificator sigma (t0:typed_term) (t1:typed_term) =

  let rec aux sigma t0 t1 =
  
    match replace sigma t0,replace sigma t1 with
    | Data(Var(x,i,j),_),t -> true,(Var(x,i,j),t)::sigma
    | t,Data(Var(x,i,j),_) -> true,(Var(x,i,j),t)::sigma
    | Data(x),Data(y)  -> if x = y then true,sigma else false,[]
    | Tuple(na,ta),Tuple(nb,tb) when (na = nb) ->
       assert(Array.length ta = Array.length tb); aux_array sigma ta tb
    | Function(fa,args_a),Function(fb,args_b) when fa = fb ->
       assert(Array.length args_a = Array.length args_b); 
       aux_array sigma args_a args_b
    | _ -> false,[]

   and aux_array sigma args_a args_b =
     let to_fold (b,sigma) (t1,t2) =
(* (bool,res) -> pattern*type -> (bool,res) *)
       if b 
       then aux sigma t1 t2
       else false,[]
     in let len = Array.length args_a  
     in let args = Array.init len (fun i -> (args_a.(i),args_b.(i)))
     in Array.fold_left to_fold (true,sigma) args

  in aux sigma t0 t1

let add_to_subst sigma (var,t) = 
  let tvar = Data(var) in
  let b,sigma1 = typed_unificator sigma tvar t in
  if b
  then sigma1
  else 
    let varstr = typed_data_to_string var in
    let tstr   = typed_term_to_string t   in
    let message = ["Some sequence of match ... with ... is impossible, as the corresponding couples (matched variable, corresponding term) do not unify.\n "; "In particular, variable ";varstr;" and term ";tstr;" are not unifiable."]
    in raise (Term_error (String.concat "" message))

let types_of_tests tau reductions =
  let for_one_reduction tau (des,arg_list,res) =
    let t1 = List.hd arg_list in
    let others = List.tl arg_list in
    let b,theta = unificator t1 tau in
    if b then Some(Util.map (replace_internal theta) others)
    else None
  in let other_types = 
    Util.map (for_one_reduction tau) reductions
  in Util.simplify_option_list other_types




let constP,constQ = 
  (* Constants used to encode reachability 
   * as an equivalence property. *)
  let const_str = "HMReachConstant" in
  let tau_str = "HMReachType" in
  let cP_str = String.concat "" [const_str;"Ok"] in
  let cQ_str = String.concat "" [const_str;"Ko"] in
  let tauP_str = String.concat "" [tau_str;"Ok"] in
  let tauQ_str = String.concat "" [tau_str;"Ko"] in
  let cP = Const(cP_str) in
  let cQ = Const(cQ_str) in
  let tauP = Data (tauP_str) in
  let tauQ = Data (tauQ_str) in
  let constP = Data (cP,tauP) in
  let constQ = Data (cQ,tauQ) in
  constP,constQ

let deepsec_input_term bound = function
    | Data(Var(x,i,j),tau) 
    when List.length bound = 1 && bound = [Var(x,i,j),tau] -> 
      atoms_to_string_simple (Var(x,i,j))
    | _ -> Util.todo(); assert(false)
