val reduction_or_test: 
  (Terms.typed_term list) -> 
    Terms.reduction list -> 
      Terms.reduction list * Terms.reduction list * Terms.shape list

