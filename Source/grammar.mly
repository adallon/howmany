%{
open Terms
open Parsing_helper
open Parsing_process

let p_res = init_parsing_result () 

exception Syntax_error of string
%}
%token LET
%token FUN
%token BITSTRING ATOM
%token NEW NEWCHANNEL
%token IN OUT 
%token PHASE
%token BEGIN END 
%token BANG PAR ARROW
%token MATCH WITH
%token REACH
%token PUBLIC 
%token EQUIVALENCE REACHABILITY DEP SOUT DEPPLUS SPLUS SOUTALL DEPALL SEMIEQUIV
%token LEFTPAR RIGHTPAR LEFTBRACKET RIGHTBRACKET
%token <char>CHAR
%token <int>INT
%token EQ
%token REDUC
%token SEMICOLON
%token COLON
%token COMMA
%token CHANNEL
%token <int> EOL
%token EOF
%token DOT
%token <string> IDENT
%start main
%type <Parsing_helper.parsing_result> main
%%

/* This file goes from the more complex (at the top)
  to the less complex (at the bottom).
  The related rules are kept together.
 */

main:
  | channelDef  
  { 
    add_channels p_res $1 ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "channels ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)
  }
  | publicDef   
  { 
  let const_list = $1 in 
  add_constants p_res const_list ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "constants ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)
  }
  | functionDef 
  { 
  let fs = $1 in 
  add_fun_symbol p_res fs ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "function ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)

(*  print_int (List.length (get_signature p_res)) ; *)
  }
  | reducDef    {
  let red = $1 in
  add_reduction p_res red ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "reduction ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)
  }
  | processDef   {
  let new_def = $1 in
  add_process_def p_res new_def ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "process ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)
  }
  | equivalence  { 
  let (p,q) = $1 in
  add_equivalence p_res p q ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "equivalence ";
    print_newline ();
    print_string res;
    print_newline ();
    *)
    p_res
(*
  end
*)
  }
  | reachability {
    add_reachability p_res $1 ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "reachability ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)
  }
  | dependency {
    add_dep p_res $1;
    p_res

  }
  | sout {
    add_sout p_res $1;
    p_res
  }
  | depplus {
    add_depplus p_res $1;
    p_res
  }
  | splus {
    add_splus p_res $1;
    p_res
  }
  | soutall {
    add_soutall p_res $1;
    p_res
  }
  | depall {
    add_depall p_res $1;
    p_res
  }
  | semiequiv {
    add_semiequiv p_res $1;
    p_res
  }
  | error 
  {
    let start_pos = Parsing.symbol_start_pos () in
    let end_pos   = Parsing.symbol_end_pos () in
    syntax_error start_pos end_pos
  }
  | EOF 
  { 
    stop p_res ;
(*
  let res = parsing_result_to_string p_res in
  begin
    print_string "EOF ";
    print_newline ();
    print_string res;
    print_newline ();
*)
    p_res
(*
  end
*)
  }


/*  Main cases  */

dependency: DEP LEFTPAR process COMMA term_type RIGHTPAR DOT 
{ ($3,atom_type_to_type $5) }

depplus: DEPPLUS LEFTPAR process COMMA term_type RIGHTPAR DOT 
{ ($3, atom_type_to_type $5) }

sout: SOUT LEFTPAR process COMMA term_type RIGHTPAR DOT 
{ ($3, atom_type_to_type $5) }

splus: SPLUS LEFTPAR process COMMA term_type RIGHTPAR DOT 
{ ($3, atom_type_to_type $5) }

soutall: SOUTALL LEFTPAR process RIGHTPAR DOT 
{ $3 }

depall: DEPALL LEFTPAR process RIGHTPAR DOT 
{ $3 }

semiequiv: SEMIEQUIV LEFTPAR process RIGHTPAR DOT 
{ $3 }

reachability: REACHABILITY LEFTPAR process RIGHTPAR DOT { $3 }

equivalence: EQUIVALENCE LEFTPAR process COMMA process RIGHTPAR DOT 
    { ($3,$5) }
processDef:  
  | LET IDENT EQ process {($2,[],$4)}
  | LET IDENT LEFTPAR list_ident RIGHTPAR EQ process {($2,$4,$7)}

reducDef:    
  REDUC IDENT LEFTPAR list_untyped_term RIGHTPAR ARROW untyped_term DOT  { ($2,$4,$7) }
functionDef: FUN IDENT LEFTPAR list_sort RIGHTPAR DOT 
            { to_function_symbol $2 $4 }
channelDef:  CHANNEL list_ident DOT 
{ 
  List.map new_channel $2 
}

publicDef:   PUBLIC  list_typed_const DOT { $2 }

/*  Processes  */

process:
  | terminal_instruction { $1 }
  | sequential_instruction SEMICOLON process 
  { append_to_sequential $1 $3 }
  | sequential_instruction DOT { $1 }

/* Intruction are split between those
that can be followed by another instruction,
like input and outputs, but also bang or phases, 
and those that terminate the process.  */

terminal_instruction:
  | process_call { $1 }
  | parallel_process { parallel $1 }
  | matcher { let (var,cases) = $1 in new_match var cases }
  | BANG process { bang $2 }
  | REACH { reach }
  | INT DOT 
  {
    match $1 with 
    |0 -> nul_process
    |k -> 
        let start_pos  = Parsing.symbol_start_pos ()
        in let end_pos = Parsing.symbol_end_pos ()
        in error_number k start_pos end_pos
  }

 /* Sequential instructions can be described by one rule */

sequential_instruction:
  | NEW IDENT COLON term_type 
  { new_nonce $2 (atom_type_to_type $4) }
  | PHASE INT { phase_nul $2}
  | NEWCHANNEL IDENT SEMICOLON OUT LEFTPAR IDENT COMMA IDENT RIGHTPAR 
 { create_channel $6 $2 $8 }
  | IN LEFTPAR IDENT COMMA typed_term RIGHTPAR 
  { new_input  $3 $5 }
  | OUT LEFTPAR IDENT COMMA untyped_term RIGHTPAR 
  { new_output $3 (string_to_parsing_term $5) }

/* description of terminal instructions */

    /* Terminal instructions 1 : pattern matching */

matcher:
  | MATCH IDENT WITH cases { ($2,$4)}

cases:
  | one_case { [$1] }
  | LEFTPAR case_list RIGHTPAR { $2 }
  | LEFTBRACKET case_list RIGHTBRACKET { $2 }
  | BEGIN case_list END { $2 }
 
case_list:
  | one_case PAR case_list  { $1::$3 }
  | one_case { [$1] }

one_case: typed_term ARROW process { ($1,$3) }

    /* Terminal instruction 2 : parallel composition */
parallel_process: 
  | LEFTPAR parallel_process_list RIGHTPAR  { $2 }
  | LEFTBRACKET parallel_process_list RIGHTBRACKET { $2 }

parallel_process_list:
  | process PAR parallel_process_list { $1 :: $3 }
  | process  { [$1] }

  /* Terminal instruction 3 : process calls */
process_call : 
  | IDENT     { call p_res ($1,[]) }
  | IDENT DOT { call p_res ($1,[]) }
  | IDENT LEFTPAR list_untyped_term RIGHTPAR 
  { call p_res ($1,List.map string_to_parsing_term $3) }

/* Terms  */

  /* Terms : typed terms */ 
  
  /* we also need to know which variables are bound.
  Those are variables with a type
   */ 
typed_term:
  | IDENT LEFTPAR list_typed_term RIGHTPAR 
  {
let sign = get_signature p_res in
composite_parsing_term sign $1 $3
  }
  | LEFTPAR list_typed_term RIGHTPAR { to_parsing_tuple $2  }
  | IDENT COLON term_type 
  { 
    parsing_typed_data ($1,atom_type_to_type $3) 
  }
   | IDENT { parsing_untyped_data $1 }

list_typed_term:
  | typed_term COMMA list_typed_term { $1 :: $3 }
  | typed_term { [$1]}

  /* Terms : typed const */

list_typed_const:
  | typed_const COMMA list_typed_const { $1::$3 }
  | typed_const { [$1] }

typed_const:
  | IDENT COLON term_type 
  {
  define_constant $1 ( atom_type_to_type  $3)
  }
  | LEFTPAR IDENT COLON term_type RIGHTPAR 
  {
  define_constant $2 ( atom_type_to_type  $4)
  }

  /* Terms : untyped terms 
  (same syntax as types, but used as terms in rule definition ) */

list_untyped_term:
  | untyped_term COMMA list_untyped_term {$1 :: $3}
  | untyped_term {[$1]}

untyped_term:
  | IDENT LEFTPAR list_untyped_term RIGHTPAR 
  {
    let sign = get_signature p_res in
    composite_term sign $1 $3
  }
  | LEFTPAR list_untyped_term RIGHTPAR {to_tuple $2 }
  | IDENT {to_data $1}

  /* Types : terms built on strings */

list_term_types:
  | term_type COMMA list_term_types { $1 :: $3 }
  | term_type {[$1] }

term_type:
  | IDENT LEFTPAR list_term_types RIGHTPAR 
  {
  let sign = get_signature p_res in
  composite_term sign $1 $3 
  }
  | LEFTPAR list_term_types RIGHTPAR {to_tuple $2 }
  | IDENT {to_data (to_atom_type $1) }


/* Sorts */

list_sort:
  | sort COMMA list_sort {$1 :: $3 }
  | sort { [$1] }

sort:
  | ATOM { Atom }
  | BITSTRING { Bitstring }

/* List of identifiers */

list_ident:
  | IDENT COMMA list_ident { $1 :: $3 }
  | IDENT {[$1] }


