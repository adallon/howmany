val satequiv_params: Tool_encoder.tool_params

val sat_equiv_encodings: Strategy.strategy -> Terms.typed_term list -> Process.scenarios list -> string list list

val sat_equiv_equivalence: 
  Terms.typed_term list -> (Process.process list *( Terms.typed_data list * Process.process list ) * int) -> string
