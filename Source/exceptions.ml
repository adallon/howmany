
exception OptionError
(* Raised in main.ml: error in the options or in the files *)

exception Help
(* Raised in main.ml: user required help. Print help message and stop. *)

exception Term_error of string 
(* There has been an error in terms. 
 * Mainly raised in terms.ml
 * *)

exception Compatibility_error of string
(* There has been an error in compatibility of shapes. 
 * See terms.ml and rule_checker.ml
 *)

exception Type_compliance_error of string * string
(* Processes are not type compliant for the given type system.
 * Returns two strings describing two unifiable terms with different types.
 * Raised by terms.ml
 *)

exception Process_error of string 
(* Error in the processes.
 * Mainly raised by process.ml
 *)

exception Parsing_error of string
(* Error during parsing.
 * Raised by parsing_helper.ml
 *)

exception SAT_Equiv_error of string
(* Error in the SAT-Equiv encoding.
 * Raised by satequiv.ml
 *)

exception Deepsec_error of string
(* Error in the Deepsec encoding.
 * Raised by deepsec.ml
 *)

