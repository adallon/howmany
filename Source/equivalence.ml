open Conclude_dep
open Count
open Terms
open Process
open Scenario
open Output
open Satequiv
open Strategy

let scenario_to_string s0 =
  let (s,nb_act) = s0 in
  let rec process_list_to_string = function
    | [] -> ""
    | [p] -> 
        let str = process_to_string p in
        String.concat "" [str;"\n  )\n"]
    | p::q ->
        let str = process_to_string p in
        String.concat "\n  |" [str; process_list_to_string q]

  in let insert_in_list l a =
    if List.mem a l then l else a::l

  in let merge_lists l1 l2 =
    List.fold_left insert_in_list l1 l2

  in let (s_no_rep,names),j = 
    let rec applied_no_rep res i = function
      | [] -> List.rev res , i
      | x::t -> let a,b,j = no_replication i x in applied_no_rep ((a,b)::res) j t
    in let to_split,i = applied_no_rep [] 1 s 
    in List.split to_split , i

  in let name_list = 
    List.fold_left merge_lists [] names

  in let name_to_string d =
    let data = typed_data_to_string d
    in String.concat "" ["new ";data;"; "]

  in let name_str = 
    String.concat "" (List.map name_to_string name_list)
  in 
  match s_no_rep with
  | [] -> (Some(0),nb_act,"0.\n"),j (* Empty scenario *)
  | [p] ->
    (Some(1),nb_act,String.concat "" [name_str;process_to_string p]),j
  | pl ->
    (Some(List.length pl),nb_act,
    String.concat "" [name_str;"\n  (";process_list_to_string pl]),j

let scenario_to_stringQ j (names,nb_act,sQ) =
  let rec process_list_to_string = function
    | [] -> ""
    | [p] -> 
        let str = process_to_string p in
        String.concat "" [str;"\n  )\n"]
    | p::q ->
        let str = process_to_string p in
        String.concat "\n  |" [str; process_list_to_string q]

  in let s_no_rep,_ = 
    let rec applied_no_rep res i = function
      | [] -> List.rev res
      | x::t -> let a,b,j = no_replication i x in applied_no_rep ((a,b)::res) j t
    in List.split (applied_no_rep [] j sQ)

  in let name_to_string d =
    let data = typed_data_to_string d
    in String.concat "" ["new ";data;"; "]

  in let name_str = 
    String.concat "" (List.map name_to_string names)
  in 
  match s_no_rep with
  | [] -> Some(0),nb_act,"0.\n" (* Empty scenario *)
  | [p] ->
    Some(1),nb_act,String.concat "" [name_str;process_to_string p]
  | pl ->
    Some(List.length pl),nb_act,
    String.concat "" [name_str;"\n  (";process_list_to_string pl]


let scenario_list_to_string (s,opt) =
  let to_map (scP,(names,scQ),n) =
    let a,j = scenario_to_string (scP,n) in let b = scenario_to_stringQ j (names,n,scQ) in a,b
  in (Util.map to_map s , to_map opt )



let scenario_list_to_string_list scenarios =
  let str_l = Util.map scenario_list_to_string scenarios
  in let max m value =
    match m,value with
    | None,_|_,None -> None
    | Some(m),Some(v) -> if m < v then Some(v) else Some(m) 

  in let optimal_scenario opt =
    let separator = "\n --- Overapproximating scenario ---\n\n - Protocol P -\n" in
    let separatorPQ = "\n - protocol Q - \n" in
    match opt with
    | (None,_,_),_ -> None
    | _,(None,_,_) -> assert(false) (* Q cannot be bottom if P is bottom *)
    | (Some(n),nb_inst,sP),(Some(nQ),nb_instQ,sQ) ->
        assert(n=nQ && nb_inst = nb_instQ); Some(n,nb_inst,String.concat "" [separator;sP;separatorPQ;sQ ])

  in let rec next_scenario max_num max_inst res i (scLeft,opt) =
    match scLeft with
      | [] -> max_num,max_inst,List.rev res,optimal_scenario opt
      | ((int_op,inst,sc),(int_opQ,instQ,scQ))::sc_l ->
          assert(int_op = int_opQ && inst = instQ);
          let istr = string_of_int i in 
          let sc_with_number = String.concat "" ["\n --- Scenario ";istr;" ---\n\n - Protocol P -\n";sc;"\n - Protocol Q - \n";scQ ] in 
          let new_max = max max_num int_op in 
          let new_max_inst = max max_inst (Some inst)
          in next_scenario new_max new_max_inst (sc_with_number::res) (i+1) (sc_l,opt)
  in let str_l1 = Util.map (next_scenario (Some 0) (Some 0) [] 1) str_l
  in Util.map (fun (m,max_i,l,opt) -> m,max_i,String.concat "" l,opt) str_l1 


let print_scenarios_equivalence scenarios =
  let (str_list:(int option * int option * string * (int * int * string) option) list ) = scenario_list_to_string_list scenarios
  in 
  let rec print_with_new_line i = function
    | [] -> ()
    | (max_num,max_inst,str,opt)::l ->
      begin
        print_newline();
        print_string "-- equivalence request ";
        print_int i ;
        print_string " --";
        print_newline();
        print_string str;
        let _ =
          match opt with
          | None -> ()
          | Some(n,nb_inst,s) ->
              let _ =
              print_newline ();
              print_string s;
              print_string "\nParallel processes in the overapproximating scenario: ";
              print_int n;
              print_string "\nInstructions (labels) in the overapproximating scenario: ";
              print_int nb_inst;
              print_newline ()
              in ()
        in
        let _ =
          match max_num with
          | None -> ()
          | Some(m) ->
              let _ =
                print_newline();
                print_string "Maximal number of parallel processes in the simple scenarios: ";
                print_int m;
              in ()
        in
        let _ =
          match max_inst with
          | None -> ()
          | Some(m) ->
              let _ =
                print_newline();
                print_string "Maximal number of instructions (labels) in the simple scenarios: ";
                print_int m;
              in ()
        in

        print_newline ();
        print_with_new_line (i+1) l
      end
  in 
  begin 
    match str_list with
    | [] -> ()
    | l  ->
        let _ =
          print_newline ();
          print_newline ();
          print_with_new_line 1 str_list;
          print_newline();

        in ()
  end

let equivalence_handler verbosity strat output_opt data filename_output (dest,test) equivalence =
  let equiv_scenarios_P =
    (* List.flatten (Util.map (fun (x,y) -> [semi_equivalence verbosity (dest,test) x , y ;semi_equivalence verbosity (dest,test) y, x]) equivalence) *)
    Util.map (fun (x,y) -> (semi_equivalence verbosity (dest,test) x , y)) equivalence
  in let all_scenarios =
    let to_map (x,y) =
      let z_list,z_opt = get_count x
      in let x_list,x_opt = 
        match x with
      | None -> assert(false) (* has been checked earlier *)
      | Some(sc_l,(sc_opt,n)) -> 
          let opt = ((sc_opt,n),z_opt) in (List.combine sc_l z_list,opt)
      in let compute_scen ((sc,n),ct) =
        (sc,generate_scenario_Q verbosity y ct,n)
      in Util.map compute_scen x_list, compute_scen x_opt

    in Util.map to_map equiv_scenarios_P
  in 
      let _ =
        if (has_count output_opt)
        then
          let scen = Util.map (fun (x,_) -> x) equiv_scenarios_P in
          count_in_file filename_output scen
      in

  if (has_satequiv output_opt)
  then let _ =
      let (_,const,_,_) = data in
      let to_map x =
        let x_list,x_opt = x in
        let y_list =  Util.map (sat_equiv_equivalence const) x_list in
        let y_opt  =  sat_equiv_equivalence const x_opt in
        match strat with
        | Overapp -> [y_opt]
        | Small   -> y_list
        | All     -> y_opt::y_list
      in let encodings = Util.map to_map all_scenarios
      in let run = has_run output_opt 
      in Tool_encoder.create_tool_files satequiv_params run filename_output [encodings]
    in ()
  else print_scenarios_equivalence all_scenarios
