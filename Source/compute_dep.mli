val print_level : int -> unit
val print_v_level : 'a -> 'a -> int -> unit
val print_v_string : 'a -> 'a -> string -> unit
val print_v_newline : 'a -> 'a -> unit -> unit
val set_empty :
  int -> Dep_graph.dependency_graph -> Dep_graph.dependency_graph
val dependencies :
  int ->
  Dep_graph.dependency_graph ->
  (Label.label -> Label.result) * (Label.type_label -> Label.result) *
  (Label.type_label -> Label.result) * (Label.type_label -> Label.result) *
  (Label.type_label -> Label.result)
