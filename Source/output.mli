type output_options

val to_output_options: 
  bool* bool * bool * int * bool * bool -> output_options

val verbosity: output_options -> int

val has_satequiv: output_options -> bool
val has_run     : output_options -> bool
val has_count   : output_options -> bool


val final_output: Strategy.strategy -> output_options ->  
  Tool_encoder.process_data -> string ->  
      Process.scenarios list list ->
      Process.scenarios list * 
      Process.scenarios list *
      Process.scenarios list * 
      Process.scenarios list * 
      (string * Process.scenarios) list *
      (string * Process.scenarios) list *
      Process.scenarios list -> unit

