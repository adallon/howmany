#!/bin/python3

import json

#
#
# Partie dépendante du protocole
#
#

FILENAME = "bac"
#Correspond au fichier bac.hm


#Constantes:
constants = "free getC, cR, cP, ok.\n"
# Ne pas mettre les canaux

#Signature:
signature = "fun senc/2.\nfun mac/2.\n\nreduc sdec(senc(x,y),y) -> x.\n\n"

#Processus:
def PR(ch,ke,km):
    proc = [("out(",",getC)"),("in(",",xP)"),("new nR; new kR; out(",",(senc((cR,nR,xP,kR),"+ke+"),mac(senc((cR,nR,xP,kR),"+ke+"),"+km+")))"),("in(",",xr)"),("let (xsenc,xmac) = xr in let (=cP,xP1,xnR,yP) = sdec(xsenc,"+ke+") in if xP1 = xP then if xnR = nR then if mac(xsenc,"+km+") = xmac then out(",",ok)")]
    res  = (ch,"("+ke+","+km+")", proc)
    return res

def PP(ch,ke,km):
    proc = [("in(",",ygetC)"),("if ygetC = getC then new nP; new kP; out(",",nP)"),("in(",",yp)"),(" let (ysenc,ymac) = yp in let (=cR,xR,ynP,yR1) = sdec(ysenc,"+ke+") in if ynP = nP then if mac(ysenc,"+km+") = ymac then out(",",(senc((cP,nP,xR,kP),"+ke+"), mac(senc((cP,nP,xR,kP),"+ke+"),"+km+")))")]
    res  = (ch,"("+ke+","+km+")", proc)
    return res

PR1 = PR("c1","ke1","km1")
PR2 = PR("c3","ke2","km2")

PP1 = PP("c2","ke1","km1")
PP2 = PP("c4","ke2","km2")

#Il manque la phase
PRProp1 = PR("c5","ke1","km1")
PPProp1 = PP("c6","ke1","km1")

PRProp2 = PR("c5","ke2","km2")
PPProp2 = PP("c6","ke2","km2")

nonces = "new ke1; new km1; new ke2; new km2;" 

completP = (nonces,[PR1,PR2,PP1,PP2,PRProp1,PPProp1])
completQ = (nonces,[PR1,PR2,PP1,PP2,PRProp2,PPProp2])

#
#
# Partie indépendante du protocole
#
#

JSON_NAME = "_howmany_encoding_"+FILENAME+".json"

def dps_name(file_name,str0):
  dps_name = "_deepsec_encoding_"+file_name+"_"+str0+".dps"
  return dps_name

f = open(JSON_NAME,'r')
scenarios = json.load(f)
f.close()

def canaux(scen):
  str0 = "free "
  for i in range(len(scen)-1):
    ch,n = scen[i]
    str0 = str0 + ch+"_"+str(i) + ","
  ch,p = scen[len(scen)-1]
  str0 = str0 + ch +"_" +str(len(scen)-1)+ ".\n"
  return str0

def get_process(P_name,proc,ch,n):
  res = "let "+P_name+" = "
  k = n #Nombre restant d'étapes
  i = 0 #Nombre d'étapes réalisées

  while (k > 0 and i < len(proc)):
    (a,b) = proc[i]
    if (k == 1 or i == len(proc)-1):
      sep = ".\n\n"
    else:
      sep="; "
    if b == "" :
      res += a+sep
    else:
      res += a+ch+b+sep
    k -= 1
    i += 1

  return res

P_init,P_process = completP
Q_init,Q_process = completQ

def all_processes(sc,completP,name):
  #name = P ou Q
  P_init,P_process = completP
  j=0
  process_strP = ""
  final_aux_list  = []
  for ch,n in sc:
    for c,var,proc in P_process:
      if ch == c:
        process_strP = process_strP + get_process(name+str(j)+var,proc,ch+"_"+str(j),n)
        final_aux_list.append(name+str(j)+var)
    j+=1
  j=0
  process_strP = process_strP + "let "+name+" = " +  P_init + "("
  for x in range(len(final_aux_list)-1):
    process_strP += final_aux_list[x]+"| "
    j+=1
  if (len(final_aux_list)>0):
    process_strP += final_aux_list[-1]+").\n\n"
  else:
    process_strP += ").\n\n"
  return process_strP

#
# La partie req est inutile car on traitera un exemple à la fois,
# mais pour le moment c'est nécessaire pour le formatage.
#


i_req = 0
#for req in scenarios:
#  for reach_inst in req:
for reach_inst in scenarios:
  opt = True
  i = 0
  for sc in reach_inst:
    if opt:
      dps_name_str = dps_name(FILENAME,"opt_"+str(i_req))
      opt = False
    else:
      dps_name_str = dps_name(FILENAME,str(i_req)+"_"+str(i))
      i += 1
    chans = canaux(sc)

    #Process P
    process_strP = all_processes(sc,completP,"P")

    #Process Q
    process_strQ = all_processes(sc,completQ,"Q")

    final = "\n\nquery trace_equiv(P,Q).\n"
    encoding = chans + constants + signature + process_strP + process_strQ + final
    f = open(dps_name_str,'w')
    f.write(encoding)
    f.close()
  i_req += 1

print("Terminé")
