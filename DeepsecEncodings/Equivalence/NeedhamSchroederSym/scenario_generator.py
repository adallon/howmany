#!/bin/python3

import json

#
#
# Partie dépendante du protocole
#
#

FILENAME = "needham-schroeder-sym-sc"
#Correspond au fichier needham-schroeder-sym-sc.hm


#Constantes:
constants = "free a, b ,c, kcs, req, rep.\nfree m1,m2,ok.\n"
# Ne pas mettre les canaux

#Signature:
signature = "fun senc/2.\nreduc sdec(senc(x,y),y) -> x.\n\n"

#Processus:
pAB = ("c0","(kas)",[("new na; out(",",(a,b,na))"),("in(",",xsenc)"),("let (xna,=b,xkab,xfwd) = sdec(xsenc,kas) in if xna = na then out(",",xfwd)"),("in(",",xsenc1)"),("let (=req,xnb) = sdec(xsenc1,xkab) in out(",", senc((rep,xnb),xkab))") ])

pAC = ("c1","(kas)",[("new na; out(",",(a,c,na))"),("in(",",xsenc)"),("let (xna,=c,xkab,xfwd) = sdec(xsenc,kas) in if xna = na then out(",",xfwd)"),("in(",",xsenc1)"),("let (=req,xnb) = sdec(xsenc1,xkab) in out(",", senc((rep,xnb),xkab))") ])

pBA0 = ("c7","(kbs)",[("in(",",yb)"), ("let (=a,ykab) = sdec(yb,kbs) in new nb;  out(",",senc((req,nb),ykab))"),("in(",",ysenc)"),("let (=rep,ynb) = sdec(ysenc,ykab) in if ynb = nb then out(",",senc(m1,ykab))") ])
pBA1 = ("c7","(kbs)",[("in(",",yb)"), ("let (=a,ykab) = sdec(yb,kbs) in new nb;  out(",",senc((req,nb),ykab))"),("in(",",ysenc)"),("let (=rep,ynb) = sdec(ysenc,ykab) in if ynb = nb then new kf; out(",",senc(m2,kf))") ])
pBA = ("c2","(kbs)",[("in(",",yb)"), ("let (=a,ykab) = sdec(yb,kbs) in new nb;  out(",",senc((req,nb),ykab))"),("in(",",ysenc)"),("let (=rep,ynb) = sdec(ysenc,ykab) in if ynb = nb then out(",",ok)") ])

#Dans pBC je voudrais ajouter un output pour que le test =sdec... soit visible.
pBC = ("c3","(kbs)",[("in(",",yb)"), ("let (=c,ykab) = sdec(yb,kbs) in new nb;  out(",",senc((req,nb),ykab))"),("in(",",ysenc)")]) #,("let (=rep,ynb) = sdec(ysenc,ykab) in if ynb = nb then out(",",ok)")] 

pSAB = ("c4","(kas,kbs)",[("in(",",zs)"), ("let (=a,=b,zna) = zs in new kab; out(",",senc((zna,b,kab,senc((a,kab),kbs)),kas))")])
pSAC = ("c5","(kas,kcs)",[("in(",",zs)"), ("let (=a,=c,zna) = zs in new kab; out(",",senc((zna,c,kab,senc((a,kab),kcs)),kas))")])
pSCB = ("c6","(kcs,kbs)",[("in(",",zs)"), ("let (=c,=b,zna) = zs in new kab; out(",",senc((zna,b,kab,senc((c,kab),kbs)),kcs))")])

completP = ("new kas; new kbs; ",[pAB,pAC,pBA,pBA0,pBC,pSAB,pSCB,pSAC])
completQ = ("new kas; new kbs; ",[pAB,pAC,pBA,pBA1,pBC,pSAB,pSCB,pSAC])

#
#
# Partie indépendante du protocole
#
#

JSON_NAME = "_howmany_encoding_"+FILENAME+".json"

def dps_name(file_name,str0):
  dps_name = "_deepsec_encoding_"+file_name+"_"+str0+".dps"
  return dps_name

f = open(JSON_NAME,'r')
scenarios = json.load(f)
f.close()

def canaux(scen):
  str0 = "free "
  for i in range(len(scen)-1):
    ch,n = scen[i]
    str0 = str0 + ch+"_"+str(i) + ","
  ch,p = scen[len(scen)-1]
  str0 = str0 + ch +"_" +str(len(scen)-1)+ ".\n"
  return str0

def get_process(P_name,proc,ch,n):
  res = "let "+P_name+" = "
  k = n #Nombre restant d'étapes
  i = 0 #Nombre d'étapes réalisées

  while (k > 0 and i < len(proc)):
    (a,b) = proc[i]
    if (k == 1 or i == len(proc)-1):
      sep = ".\n\n"
    else:
      sep="; "
    if b == "" :
      res += a+sep
    else:
      res += a+ch+b+sep
    k -= 1
    i += 1

  return res

P_init,P_process = completP
Q_init,Q_process = completQ

def all_processes(sc,completP,name):
  #name = P ou Q
  P_init,P_process = completP
  j=0
  process_strP = ""
  final_aux_list  = []
  for ch,n in sc:
    for c,var,proc in P_process:
      if ch == c:
        process_strP = process_strP + get_process(name+str(j)+var,proc,ch+"_"+str(j),n)
        final_aux_list.append(name+str(j)+var)
    j+=1
  j=0
  process_strP = process_strP + "let "+name+" = " +  P_init + "("
  for x in range(len(final_aux_list)-1):
    process_strP += final_aux_list[x]+"| "
    j+=1
  if (len(final_aux_list)>0):
    process_strP += final_aux_list[-1]+").\n\n"
  return process_strP

#
# La partie req est inutile car on traitera un exemple à la fois,
# mais pour le moment c'est nécessaire pour le formatage.
#


i_req = 0
#for req in scenarios:
#  for reach_inst in req:
for reach_inst in scenarios:
  opt = True
  i = 0
  for sc in reach_inst:
    if opt:
      dps_name_str = dps_name(FILENAME,"opt_"+str(i_req))
      opt = False
    else:
      dps_name_str = dps_name(FILENAME,str(i_req)+"_"+str(i))
      i += 1
    chans = canaux(sc)

    #Process P
    process_strP = all_processes(sc,completP,"P")

    #Process Q
    process_strQ = all_processes(sc,completQ,"Q")

    final = "\n\nquery trace_equiv(P,Q).\n"
    encoding = chans + constants + signature + process_strP + process_strQ + final
    f = open(dps_name_str,'w')
    f.write(encoding)
    f.close()
  i_req += 1

print("Terminé")
