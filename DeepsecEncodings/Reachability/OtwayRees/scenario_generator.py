#!/bin/python3

import json

#
#
# Partie dépendante du protocole
#
#

FILENAME = "otway-rees-sc"
#Correspond au fichier otway-rees-sc.hm


#Constantes:
constants = "free a, b ,c, kcs.\nfree ok, ko.\n"
# Ne pas mettre les canaux

#Signature:
signature = "fun senc/2.\nreduc sdec(senc(x,y),y) -> x.\n\n"

#Processus:
#On pourrait ajouter un output pour valider le format du dernier input
pAB = ("c0","(kas)",[("new m; new na; out(",",(m,a,b,senc((na,m,a,b),kas)))"),("in(",",x)")])

pAC = ("c1","(kas)",[("new m; new na; out(",",(m,a,c,senc((na,m,a,c),kas)))"),("in(",",x)")])

pBA0 = ("c2","(kbs)",[("in(",",yb)"), ("let (ym,=a,=b,yenc1) = yb in new nb;  out(",",(ym,a,b,yenc1,senc((nb,ym,a,b),kbs)))"),("in(",",yb2)"),("let (ym1,yenc3,ysenc) = yb2 in if ym1 = ym then let (ynb,yab) = sdec(ysenc,kbs) in if ynb = nb then out(",",(ym,yenc3))"),("in(",",yab1)"),("if yab1 = yab then out(",",ok)") ])

pBA1 = ("c2","(kbs)",[("in(",",yb)"), ("let (ym,=a,=b,yenc1) = yb in new nb;  out(",",(ym,a,b,yenc1,senc((nb,ym,a,b),kbs)))"),("in(",",yb2)"),("let (ym1,yenc3,ysenc) = yb2 in if ym1 = ym then let (ynb,yab) = sdec(ysenc,kbs) in if ynb = nb then out(",",(ym,yenc3))"),("in(",",yab1)"),("if yab1 = yab then out(",",ko)") ])

pBC = ("c3","(kbs)",[("in(",",yb)"), ("let (ym,=c,=b,yenc1) = yb in new nb;  out(",",(ym,c,b,yenc1,senc((nb,ym,c,b),kbs)))"),("in(",",yb2)"),("let (ym1,yenc3,ysenc) = yb2 in if ym1 = ym then let (ynb,yab) = sdec(ysenc,kbs) in if ynb = nb then out(",",(ym,yenc3))") ])
 
pSAB = ("c4","(kas,kbs)",[("in(",",zs)"), ("let (zm,=a,=b,zsenc1,zsenc2) = zs in let (zna,zm1,=a,=b) = sdec(zsenc1,kas) in if zm1 = zm then let (znb,zm2,=a,=b) = sdec(zsenc2,kbs) in if zm2 = zm then new kab; out(",", (zm,senc((zna,kab),kas),senc((znb,kab),kbs)))")])
pSAC = ("c5","(kas,kcs)",[("in(",",zs)"), ("let (zm,=a,=c,zsenc1,zsenc2) = zs in let (zna,zm1,=a,=c) = sdec(zsenc1,kas) in if zm1 = zm then let (znb,zm2,=a,=c) = sdec(zsenc2,kcs) in if zm2 = zm then new kab; out(",", (zm,senc((zna,kab),kas),senc((znb,kab),kcs)))")])
pSCB = ("c6","(kcs,kbs)",[("in(",",zs)"), ("let (zm,=c,=b,zsenc1,zsenc2) = zs in let (zna,zm1,=c,=b) = sdec(zsenc1,kcs) in if zm1 = zm then let (znb,zm2,=c,=b) = sdec(zsenc2,kbs) in if zm2 = zm then new kab; out(",", (zm,senc((zna,kab),kcs),senc((znb,kab),kbs)))")])

completP = ("new kas; new kbs; ",[pAB,pAC,pBA0,pBC,pSAB,pSCB,pSAC])
completQ = ("new kas; new kbs; ",[pAB,pAC,pBA1,pBC,pSAB,pSCB,pSAC])

#
#
# Partie indépendante du protocole
#
#

JSON_NAME = "_howmany_encoding_"+FILENAME+".json"

def dps_name(file_name,str0):
  dps_name = "_deepsec_encoding_"+file_name+"_"+str0+".dps"
  return dps_name

f = open(JSON_NAME,'r')
scenarios = json.load(f)
f.close()

def canaux(scen):
  str0 = "free "
  for i in range(len(scen)-1):
    ch,n = scen[i]
    str0 = str0 + ch+"_"+str(i) + ","
  ch,p = scen[len(scen)-1]
  str0 = str0 + ch +"_" +str(len(scen)-1)+ ".\n"
  return str0

def get_process(P_name,proc,ch,n):
  res = "let "+P_name+" = "
  k = n #Nombre restant d'étapes
  i = 0 #Nombre d'étapes réalisées

  while (k > 0 and i < len(proc)):
    (a,b) = proc[i]
    if (k == 1 or i == len(proc)-1):
      sep = ".\n\n"
    else:
      sep="; "
    if b == "" :
      res += a+sep
    else:
      res += a+ch+b+sep
    k -= 1
    i += 1

  return res

P_init,P_process = completP
Q_init,Q_process = completQ

def all_processes(sc,completP,name):
  #name = P ou Q
  P_init,P_process = completP
  j=0
  process_strP = ""
  final_aux_list  = []
  for ch,n in sc:
    for c,var,proc in P_process:
      if ch == c:
        process_strP = process_strP + get_process(name+str(j)+var,proc,ch+"_"+str(j),n)
        final_aux_list.append(name+str(j)+var)
    j+=1
  j=0
  process_strP = process_strP + "let "+name+" = " +  P_init + "("
  for x in range(len(final_aux_list)-1):
    process_strP += final_aux_list[x]+"| "
    j+=1
  if (len(final_aux_list)>0):
    process_strP += final_aux_list[-1]+").\n\n"
  return process_strP

#
# La partie req est inutile car on traitera un exemple à la fois,
# mais pour le moment c'est nécessaire pour le formatage.
#


i_req = 0
#for req in scenarios:
#  for reach_inst in req:
for reach_inst in scenarios:
  opt = True
  i = 0
  for sc in reach_inst:
    if opt:
      dps_name_str = dps_name(FILENAME,"opt_"+str(i_req))
      opt = False
    else:
      dps_name_str = dps_name(FILENAME,str(i_req)+"_"+str(i))
      i += 1
    chans = canaux(sc)

    #Process P
    process_strP = all_processes(sc,completP,"P")

    #Process Q
    process_strQ = all_processes(sc,completQ,"Q")

    final = "\n\nquery trace_equiv(P,Q).\n"
    encoding = chans + constants + signature + process_strP + process_strQ + final
    f = open(dps_name_str,'w')
    f.write(encoding)
    f.close()
  i_req += 1

print("Terminé")
