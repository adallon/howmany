#!/bin/python3

import json

#
#
# Partie dépendante du protocole
#
#

FILENAME = "nsl-sc"
#Correspond au fichier nsl-sc.hm


#Constantes:
constants = "free a, b ,c, skc, one, two, three.\nfree ok, ko.\n"
# Ne pas mettre les canaux

#Signature:
signature = "fun aenc/2.\nfun pub/1.\n\nreduc adec(aenc(x,pub(y)),y) -> x.\n\n"

#Processus:
pAB = ("c1","(ska,skb)",[("new nab; out(",",aenc((one,a,nab),pub(skb)))"),("in(",",xaenc)"),("let (=two,xnab,xnb,=b) = adec(xaenc,ska) in if xnab = nab then out(",",aenc((three,xnb),pub(skb)))")])
pAC = ("c2","(ska,skc)",[("new nab; out(",",aenc((one,a,nab),pub(skc)))"),("in(",",xaenc)"),("let (=two,xnab,xnb,=c) = adec(xaenc,ska) in if xnab = nab then out(",",aenc((three,xnb),pub(skc)))")])

pBA0 = ("c3","(skb,ska)",[("in(",",yaenc)"), ("let (=one,=a,yna) = adec(yaenc,skb) in new nba; out(",",aenc((two,yna,nba,b),pub(ska)))"),("in(",",yaenc2)"),("let (=three,ynba) = adec(yaenc2,skb) in if ynba = nba then in(",",ynba2)"),("if ynba2 = nba then out(",",ok)") ])
pBA1 = ("c3","(skb,ska)",[("in(",",yaenc)"), ("let (=one,=a,yna) = adec(yaenc,skb) in new nba; out(",",aenc((two,yna,nba,b),pub(ska)))"),("in(",",yaenc2)"),("let (=three,ynba) = adec(yaenc2,skb) in if ynba = nba then in(",",ynba2)"),("if ynba2 = nba then out(",",ko)") ])


#Dans pBC je voudrais ajouter un output pour que le test =adec... soit visible.
pBC = ("c4","(skb,skc)",[("in(",",yaenc)"), ("let (=one,=c,yna) = adec(yaenc,skb) in new nba; out(",",aenc((two,yna,nba,b),pub(skc)))"),("in(",",yaenc2)")]) #,("let (=three,ynba) = adec(yaenc2,skb) in if ynba = nba then in(",",ynba2)") ])

pKey = ("c0","(ska,skb)",[("out(",",pub(ska))"),("out(",",pub(skb))")])

completP = ("new ska; new skb; ",[pAB,pAC,pBA0,pBC,pKey])
completQ = ("new ska; new skb; ",[pAB,pAC,pBA1,pBC,pKey])

#
#
# Partie indépendante du protocole
#
#

JSON_NAME = "_howmany_encoding_"+FILENAME+".json"

def dps_name(file_name,str0):
  dps_name = "_deepsec_encoding_"+file_name+"_"+str0+".dps"
  return dps_name

f = open(JSON_NAME,'r')
scenarios = json.load(f)
f.close()

def canaux(scen):
  str0 = "free "
  for i in range(len(scen)-1):
    ch,n = scen[i]
    str0 = str0 + ch+"_"+str(i) + ","
  ch,p = scen[len(scen)-1]
  str0 = str0 + ch +"_" +str(len(scen)-1)+ ".\n"
  return str0

def get_process(P_name,proc,ch,n):
  res = "let "+P_name+" = "
  k = n #Nombre restant d'étapes
  i = 0 #Nombre d'étapes réalisées

  while (k > 0 and i < len(proc)):
    (a,b) = proc[i]
    if (k == 1 or i == len(proc)-1):
      sep = ".\n\n"
    else:
      sep="; "
    if b == "" :
      res += a+sep
    else:
      res += a+ch+b+sep
    k -= 1
    i += 1

  return res

P_init,P_process = completP
Q_init,Q_process = completQ

def all_processes(sc,completP,name):
  #name = P ou Q
  P_init,P_process = completP
  j=0
  process_strP = ""
  final_aux_list  = []
  for ch,n in sc:
    for c,var,proc in P_process:
      if ch == c:
        process_strP = process_strP + get_process(name+str(j)+var,proc,ch+"_"+str(j),n)
        final_aux_list.append(name+str(j)+var)
    j+=1
  j=0
  process_strP = process_strP + "let "+name+" = " +  P_init + "("
  for x in range(len(final_aux_list)-1):
    process_strP += final_aux_list[x]+"| "
    j+=1
  if (len(final_aux_list)>0):
    process_strP += final_aux_list[-1]+").\n\n"
  return process_strP

#
# La partie req est inutile car on traitera un exemple à la fois,
# mais pour le moment c'est nécessaire pour le formatage.
#


i_req = 0
#for req in scenarios:
#  for reach_inst in req:
for reach_inst in scenarios:
  opt = True
  i = 0
  for sc in reach_inst:
    if opt:
      dps_name_str = dps_name(FILENAME,"opt_"+str(i_req))
      opt = False
    else:
      dps_name_str = dps_name(FILENAME,str(i_req)+"_"+str(i))
      i += 1
    chans = canaux(sc)

    #Process P
    process_strP = all_processes(sc,completP,"P")

    #Process Q
    process_strQ = all_processes(sc,completQ,"Q")

    final = "\n\nquery trace_equiv(P,Q).\n"
    encoding = chans + constants + signature + process_strP + process_strQ + final
    f = open(dps_name_str,'w')
    f.write(encoding)
    f.close()
  i_req += 1

print("Terminé")
